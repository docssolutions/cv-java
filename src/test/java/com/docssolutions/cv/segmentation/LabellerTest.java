/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.segmentation;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import csv.*;
import csv.impl.*;

import java.io.File;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.segmentation.Labeller;

public class LabellerTest
{
    @Ignore
    public void inputWithLabellerShouldBeCorrectEx1()
    {
        float[][] mx = CVUtils.getMatrixFromCSV("Ex1/inputBwlabel_BW2.csv");
        //CVUtils.printMatrix(mx);

        Labeller lr = new Labeller();
        float[][] new_mx = lr.getRawLabels(mx);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("Ex1/outputBwlabel_L.csv");
        //CVUtils.printMatrix(expected_mx);

        //float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);
        float[][] diff_mx = CVUtils.markLabelDifferences(expected_mx, new_mx);
        //CVUtils.printPresenceInMatrix(diff_mx);
    }

    @Ignore
    public void inputWithLabellerShouldBeCorrectMini2()
    {
        float[][] mx = CVUtils.getMatrixFromCSV("mini2/inputBwlabel_BW2.csv");
        //CVUtils.printMatrix(mx);

        Labeller lr = new Labeller();
        float[][] new_mx = lr.getRawLabels(mx);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("mini2/outputBwlabel_L.csv");
        //CVUtils.printMatrix(expected_mx);

        //float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);
        float[][] diff_mx = CVUtils.markLabelDifferences(expected_mx, new_mx);
        //CVUtils.printPresenceInMatrix(diff_mx);
    }
}
