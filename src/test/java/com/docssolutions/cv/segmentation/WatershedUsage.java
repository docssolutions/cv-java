/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.segmentation;

import com.docssolutions.cv.segmentation.Watershed;
import com.docssolutions.cv.transformation.EuclidianDistanceTransformer;
import com.docssolutions.cv.filter.ImgPlusPrinter;
import com.docssolutions.cv.CVUtils;

import csv.*;
import csv.impl.*;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Scanner;
import java.util.HashMap;
import java.awt.image.*;

public class WatershedUsage {


    public static void main( String[] args ) 
    {

        Watershed myWatershed;
        EuclidianDistanceTransformer edt = new EuclidianDistanceTransformer();
        
/*
        // °J° String  _filename      = "0Example1C.jpg";
        String  _filename      = "forWatershedInv.jpg";
        // °J° String  _filename      = "e3.jpg";
        im = new ImagePlus(_filename);
        // °J° e.show();

        BinaryProcessor ip_bin = new BinaryProcessor((ByteProcessor)im.getProcessor()); 
        ip_float = ip_bin.convertToFloatProcessor(); 
        // °J° ip_float = ip_color.convertToFloatProcessor();
        ImgPlusPrinter.print_img_pixels_ip(ip_float, ip_float.getWidth(), ip_float.getHeight());
        // °J° ImgPlusPrinter.print_img_pixels_ip(ip_float, 8,8);
        float[][] dists = CVUtils.transformMatrixFrom1To2D((float[])ip_float.getPixels(), im.getWidth(), im.getHeight());
        

        System.out.println(":: To apply raw distance transformation...");
        float[][] new_input_mat = edt.applyRawDistanceTransformation(dists);
        // °J° CVUtils.printMatrix(new_input_mat);
        ip_float = new FloatProcessor(new_input_mat);
        im.setProcessor(ip_float);
        im.show();
        // °J° */
        
        // °J° System.out.println(":: input_mat");
        // °J° CVUtils.printMatrix(ex_MAT);
        // °J° ip_float = new FloatProcessor(ex_MAT);
        // °J° ip_float = (FloatProcessor)ip_float.resize(80,80);
        // °J° im = new ImagePlus("dist.jpg", ip_float);
        // °J° im.show();
        // °J° BufferedImage bf = CVUtils.createBImageF(ex_MAT);
        // °J° CVUtils.saveImage("dist", bf);

        // °J° ERASE DEBUG_IMG FOLDER BEFORE EXECUTING
        try {
            // °J° FileCVUtils.deleteQuietly(FileCVUtils.getFile("out.mp4"));
            FileUtils.cleanDirectory(FileUtils.getFile("debug_img"));
        } catch(Exception e) {
            System.out.println("There is not debug image folder: " +e.getMessage());
        }
        
        try {
            // °J° File f = new File("WsdistInfWatershed.csv");
            File f = new File("wB.csv");
            // °J° File f = new File("w2.csv");
            
            System.out.println("XXX");
            CSVReader in = new CSVReader(f);
            in.setColumnDelimiter("\n");
            in.setColumnSeparator(',');

            int w = 0;
            int h = 0;
            while(in.hasNext()) {
                h++;
                Object[] columns = in.next();
                if (w < columns.length)
                    w = columns.length;
            }
            in.close();
            System.out.println("read");
            // °J° //System.out.println(":: Image read of "+w+"x"+h);

            float[][] input_mat = new float[h][w];
            int[][] input_lut = new int[h][w];

            in = new CSVReader(f);
            Object[] columns;
            in.setColumnDelimiter("\n");
            in.setColumnSeparator(',');
            int i=0, j=0;

            while(in.hasNext()) {
                columns = in.next();
                for(j=0;j<w;j++) {
                    String cj= (String) columns[j];
                    input_mat[i][j] = (cj.equals("-Inf"))? Float.NEGATIVE_INFINITY : Float.parseFloat(cj);
                }
                i++;
            }
            in.close();
// °J° 
            // °J° System.out.println(":: input_mat");
            // °J° CVUtils.printMatrix(input_mat);
            // °J° ip_float = new FloatProcessor(input_mat);
            // °J° ip_float = (FloatProcessor)ip_float.resize(800,800);
            // °J° im = new ImagePlus("dist.jpg", ip_float);
            // °J° im.show();
            
            // °J° BufferedImage inq_bim = CVUtils.createBImageDst(input_mat);
            // °J° CVUtils.saveImage("wBD.png", inq_bim);
            
            System.out.println("watersed");
            myWatershed = new Watershed(input_mat);
            // °J° myWatershed.getMinSrch();
            // °J° myWatershed.getMinSrch2(input_lut);
            // °J° myWatershed.getMinSrch3();
            int[][] lut = myWatershed.applyWatershed();
            // °J° CVUtils.createBImageI(CVUtils.colorImage(lut, foundMinimums),4);
            BufferedImage lbl_bim = CVUtils.createBImageI(lut,4);
            CVUtils.saveImage(String.format("lut_END.png"), lbl_bim);
            
            // °J° System.out.println("done");
            final float usedMem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/(1024*1024);
            System.out.println("Memory used: "+usedMem+" MB");
            // °J° int minsFound = myWatershed.getMins();
            // °J° int[][] colr_labels = new int[h*4][w*4];
            // °J° float fc = 180/minsFound;
            // °J° for ( int r = 0; r < h; r++){
                // °J° for (int c = 0; c < w; c++){
                    // °J° for (int mm = 0; mm < 4; mm++){
                        // °J° for (int nn = 0; nn < 4; nn++){
                            // °J° colr_labels[c*4+mm][r*4+nn] = (lut[r][c] < 25)? (int)(lut[r][c]*fc): 255;
                        // °J° }
                    // °J° }
                // °J° }
            // °J° }
            // °J° 
            // °J° CVUtils.showMatrix(colr_labels);
            
        
        } catch(Exception e) {
            System.out.println(":: DAFUQ IO: " +e.getMessage());
            e.printStackTrace();
        }
    }
}
