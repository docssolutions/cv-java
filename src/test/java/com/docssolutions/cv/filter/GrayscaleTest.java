/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import com.docssolutions.cv.filter.Grayscale;
import com.docssolutions.cv.CVUtils;

public class GrayscaleTest
{
    @Ignore
    public void inputWithGrayscaleShouldBeCorrect()
    {
		BufferedImage inputImage = null;

		try {
			String path = "data/imgTest/";
			inputImage = ImageIO.read(new File(path + "Example1.jpg"));

			Grayscale grayScaleTransformer = new Grayscale(inputImage);
			float[][] grayImage = grayScaleTransformer.applyRgb2Gray();

			BufferedImage bImgOut = CVUtils.createBImageF(grayImage);

			float[][] expected_mx = CVUtils.getMatrixFromCSV("Ex1/inputAnisodiff2D_grayImage.csv");
			float[][] diff_mat = CVUtils.markSquareDifferences(grayImage, expected_mx);

            int n = grayImage.length;
            int m = grayImage[0].length;

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    assertEquals(expected_mx[i][j], grayImage[i][j], 0.0f);
		}
		catch(Exception e) {
            e.printStackTrace();
            fail();
		}
    }
}
