/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import com.docssolutions.cv.filter.FilterUtils;

import java.util.*;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.filter.FilterUtils;

public class FilterUtilsTest 
{
    private static double[][] kernel1 = {{-1, 0, 1},
                                        {-2, 0, 2},
                                        {-1, 0, 1}};

    private static double[][] kernel2 = {{1, 2, 3},
                                        {4, 5, 6},
                                        {7, 8, 9}};
        
    private static double[][] gaussianKernel = {{0.011344, 0.083820, 0.011344},
                                               {0.083820, 0.619347, 0.083820},
                                               {0.011344, 0.083820, 0.011344}};

    @Ignore
    public void kernelExample1ShouldBeSeparable()
    {
        assertEquals("kernel1 should be separable", true, FilterUtils.isKernelSeparable(kernel1));
    }

    @Ignore
    public void kernelExample2ShouldNotBeSeparable()
    {
        assertEquals("kernel2 should not be separable", false, FilterUtils.isKernelSeparable(kernel2));
    }

    @Ignore
    public void gaussianKernelShouldBeSeparable()
    {
        FilterUtils.getKernelComponents(gaussianKernel);
        //assertEquals("gaussianKernel should be separable", true, FilterUtils.isKernelSeparable(gaussianKernel));
    }
}
