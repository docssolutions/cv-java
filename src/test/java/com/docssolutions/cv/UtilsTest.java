/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;

public class UtilsTest
{
    private float[] vs;

    @Before
    public void initVs() {
        float[] vs_1 = {0.6844323f, -0.2612878f, 0.0085295f,
                        -0.1449806f, 0.4193766f, -0.7038889f,
                        0.4718234f, -0.6742370f, -1.3942013f,
                        -0.5551019f, 1.8918553f, 0.2381500f,
                        -1.8515779f, -0.0562009f, 0.2782228f,
                        -0.7523585f};
        vs = vs_1;
    }
    
    @Ignore
    public void kurtosisShouldBeCorrect() {
        float mean = CVUtils.meanMtrx1D(vs);
        float stdv = CVUtils.std1D(vs, mean);
        System.out.println(":: Kurtosis: "+CVUtils.getKurtosis(vs, mean, stdv));
        assertEquals(3.5379, CVUtils.getKurtosis(vs, mean, stdv), 0.01f);
    }
    @Ignore
    public void entropyShouldBeCorrect() {
        float[][] mx = CVUtils.getMatrixFromCSV("outputImcrop_I.csv");
        float[] mx0 = mx[0];
        System.out.println(":: Entropy: "+CVUtils.getEntropy(mx0));
//        assertEquals(4.6122f, CVUtils.getEntropy(mx), 0.0001f);
    }
    
    @Ignore
	public void covarianceMatrixExample() {
		float[][] nMtrx = { {0.8147f,    0.1576f,    0.9723f},
						    {Float.NaN,  Float.NaN,  Float.NaN},
						    {0.1270f,    0.9572f,    1.0842f},
						    {0.9134f,    Float.NaN,  Float.NaN},
						    {0.6324f,    Float.NaN,  Float.NaN},
						    {0.0975f,    0.1419f,    0.2394f},
						    {0.2785f,    0.4218f,    0.7003f},
						    {0.5469f,    0.9157f,    1.4626f},
						    {0.9575f,    0.7922f,    1.7497f},
						    {0.9649f,    Float.NaN,  Float.NaN}};
    
		float[][] mtrx = { {-1.0f, 1.0f, 2.0f},
						   {-2.0f, 3.0f, 1.0f},
						   {4.0f, 0.0f, 3.0f} };

		float[][] cov = CVUtils.covarianceMatrix(mtrx);

		CVUtils.printMatrix(cov);

		float[][] nCov = CVUtils.covarianceNanMatrix(nMtrx);
		CVUtils.printMatrix(nCov);
	}

    @Ignore
    public void avgFilterTest() {
		double[][] input = { {4.0, 2.0, 6.0, 8.0},
							 {3.0, 9.0, 6.0, 43.0},
							 {1.0, 79.0, 88.0, 23.0} };

		double[][] out = CVUtils.averageFilter(input, 10, 10);
		CVUtils.printMatrix(out);
	}

    @Ignore
    public void padarrayTest() {
		double[][] input = { {4.0, 2.0, 6.0, 8.0},
							 {3.0, 9.0, 6.0, 43.0},
							 {1.0, 79.0, 88.0, 23.0} };

		double[][] out1 = CVUtils.padarray(input, 5, 5, false);
		double[][] out2 = CVUtils.padarray(out1, 4, 4, true);
		CVUtils.printMatrix(out2);

		double[][] s = CVUtils.cumsum(out2);
		CVUtils.printMatrix(s);
	}
}
