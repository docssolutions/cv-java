/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.transformation.Cropper;

public class CropperTest
{
    @Ignore
    public void printCropImageInputAndOutputGrayscale() {
        float[][] mx = CVUtils.getMatrixFromCSV("Ex1/inputImcrop_grayImage.csv");
        //CVUtils.printMatrix(mx);
        Cropper cr = new Cropper();
        float[][] new_mx = cr.applyRawCropping(mx, 2196, 880, 288, 288);
        float[][] expected_mx = CVUtils.getMatrixFromCSV("Ex1/outputImcrop_I.csv");

        int n = new_mx.length;
        int m = new_mx[0].length;

        //CVUtils.printMatrix(new_mx);

        //CVUtils.println(":: length of new_mx: "+m+","+n);
        //CVUtils.println(":: length of expected_mx: "+expected_mx[0].length+","+expected_mx.length);

        assert new_mx.length == expected_mx.length;
        assert new_mx[0].length == expected_mx[0].length;
        //CVUtils.printMatrix(expected_mx);

        float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], 0.001f);
    }

    @Ignore
    public void printCropImageInputAndOutputBinary() {
        float[][] mx = CVUtils.getMatrixFromCSV("inputImcrop_L.csv");
        //CVUtils.printMatrix(mx);
        Cropper cr = new Cropper();
        float[][] new_mx = cr.applyRawCropping(mx, 609, 169, 288, 288);
        float[][] expected_mx = CVUtils.getMatrixFromCSV("outputImcrop_I2.csv");

        int n = new_mx.length;
        int m = new_mx[0].length;

        //CVUtils.printMatrix(new_mx);

        //CVUtils.println(":: length of new_mx: "+m+","+n);
        //CVUtils.println(":: length of expected_mx: "+expected_mx[0].length+","+expected_mx.length);

        assert new_mx.length == expected_mx.length;
        assert new_mx[0].length == expected_mx[0].length;
        //CVUtils.printMatrix(expected_mx);

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], 0.001f);
    }
}
