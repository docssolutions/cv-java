/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.transformation.Filler;

public class FillerTest
{
    @Ignore
    public void inputWithFillerShouldBeCorrectEx1()
    {
        float[][] mx = CVUtils.getMatrixFromCSV("Ex1/inputImfill_BW2c.csv"); 		// Disk-5
        //float[][] mx = CVUtils.getMatrixFromCSV("Ex1/inputImfill_I2n.csv"); 	// Disk-2
        //CVUtils.printMatrix(mx);
        
        Filler filler = new Filler();
        float[][] new_mx = filler.applyRawHolesFilling(mx);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("Ex1/outputImfill_BW2f.csv"); 		// Disk-2
        //float[][] expected_mx = CVUtils.getMatrixFromCSV("Ex1/outputImfill_I3.csv"); 		// Disk-2
        //CVUtils.printMatrix(expected_mx);

        float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);
        //CVUtils.printPresenceInMatrix(diff_mx);

        int n = new_mx.length;
        int m = new_mx[0].length;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], 1.0f); // Very weak threshold
    }

    @Ignore
    public void inputWithFillerShouldBeCorrectMini2()
    {
        float tolerance = 0.01f; // Weak threshold
        float[][] mx = CVUtils.getMatrixFromCSV("mini2/inputImfill_BW2c.csv"); 		// Disk-5
        //float[][] mx = CVUtils.getMatrixFromCSV("mini2/inputImfill_I2n.csv"); 		// Disk-2
        //CVUtils.printMatrix(mx);
        
        Filler filler = new Filler();
        float[][] new_mx = filler.applyRawHolesFilling(mx);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("mini2/outputImfill_BW2f.csv"); 		// Disk-5
        //float[][] expected_mx = CVUtils.getMatrixFromCSV("mini2/outputImfill_I3.csv"); 		// Disk-2
        //CVUtils.printMatrix(expected_mx);

        float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx, tolerance);
        //CVUtils.printPresenceInMatrix(diff_mx);

        int n = new_mx.length;
        int m = new_mx[0].length;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], tolerance); // Very weak threshold
    }
}
