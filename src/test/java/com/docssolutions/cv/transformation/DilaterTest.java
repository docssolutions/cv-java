/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.transformation.Dilater;

public class DilaterTest
{
    @Ignore
    public void inputWithDilaterShouldBeCorrectEx1()
    {
        //float tolerance = 0.015f; // Weak threshold
        float[][] mx = CVUtils.getMatrixFromCSV("Ex1/inputImdilate_I2.csv");
        //CVUtils.printMatrix(mx);
        
        Dilater dl = new Dilater();
        float[][] new_mx = dl.applyRawDiskDilation(mx, 2.0f);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("Ex1/outputImdilate_I2n.csv");
        //CVUtils.printMatrix(expected_mx);

        //float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx, tolerance);
        float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);
        //CVUtils.printPresenceInMatrix(diff_mx);

        int n = new_mx.length;
        int m = new_mx[0].length;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], 0.0f);
    }

    @Ignore
    public void inputWithDilaterShouldBeCorrectMini2()
    {
        //float tolerance = 0.015f; // Weak threshold
        float[][] mx = CVUtils.getMatrixFromCSV("mini2/inputImdilate_I2.csv");
        //CVUtils.printMatrix(mx);
        
        Dilater dl = new Dilater();
        float[][] new_mx = dl.applyRawDiskDilation(mx, 2.0f);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("mini2/outputImdilate_I2n.csv");
        //CVUtils.printMatrix(expected_mx);

        //float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx, tolerance);
        float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);
        //CVUtils.printPresenceInMatrix(diff_mx);

        int n = new_mx.length;
        int m = new_mx[0].length;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], 0.0f);
    }
}
