/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.transformation.Eroder;

public class EroderTest
{
    @Ignore
    public void inputWithDilaterShouldBeCorrectEx1()
    {
        float[][] mx = CVUtils.getMatrixFromCSV("Ex1/inputImerode_dlt.csv");
        //CVUtils.printMatrix(mx);
        
        Eroder er = new Eroder();
        float[][] new_mx = er.applyRawDiskErosion(mx, 5.0f);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("Ex1/outputImerode_B.csv");
        //CVUtils.printMatrix(expected_mx);

        float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);
        //CVUtils.printPresenceInMatrix(diff_mx);

        int n = new_mx.length;
        int m = new_mx[0].length;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], 0.0f);
    }

    @Ignore
    public void inputWithDilaterShouldBeCorrectMini2()
    {
        float[][] mx = CVUtils.getMatrixFromCSV("mini2/inputImerode_dlt.csv");
        //CVUtils.printMatrix(mx);
        
        Eroder er = new Eroder();
        float[][] new_mx = er.applyRawDiskErosion(mx, 5.0f);
        //CVUtils.printMatrix(new_mx);

        float[][] expected_mx = CVUtils.getMatrixFromCSV("mini2/outputImerode_B.csv");
        //CVUtils.printMatrix(expected_mx);

        float[][] diff_mx = CVUtils.markSquareDifferences(expected_mx, new_mx);
        //CVUtils.printPresenceInMatrix(diff_mx);

        int n = new_mx.length;
        int m = new_mx[0].length;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(expected_mx[i][j], new_mx[i][j], 0.0f);
    }
}
