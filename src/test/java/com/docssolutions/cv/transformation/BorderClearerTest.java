/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.transformation.Cropper;

public class BorderClearerTest
{
    @Ignore
    public void printClearImageBorderInputAndOutput() {
        float[][] mx = CVUtils.getMatrixFromCSV("inputImclearborder_BW2.csv");
        CVUtils.printMatrix(mx);
        float[][] expected_mx = CVUtils.getMatrixFromCSV("outputImclearborder_BW2.csv");
        CVUtils.printMatrix(expected_mx);
    }
}
