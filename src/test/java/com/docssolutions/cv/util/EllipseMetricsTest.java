/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.util.EllipseMetrics;

public class EllipseMetricsTest
{
    private static LinkedList<Point> mx_1;
    private static LinkedList<Point> mx_1a;
    private static LinkedList<Point> mx_2;

    private EllipseMetrics ems;

    static {
        mx_1 = new LinkedList<Point>();
        mx_1.add(new Point(1,1)); mx_1.add(new Point(1,2));
        mx_1.add(new Point(2,1)); mx_1.add(new Point(2,2));

        mx_1a = new LinkedList<Point>();
        mx_1a.add(new Point(1,1)); mx_1a.add(new Point(1,2)); mx_1a.add(new Point(1,3));
        mx_1a.add(new Point(2,1)); mx_1a.add(new Point(2,2)); mx_1a.add(new Point(2,3));
        //mx_1a.add(new Point(3,1)); mx_1a.add(new Point(3,2)); mx_1a.add(new Point(3,3));

        mx_2 = new LinkedList<Point>();
        mx_2.add(new Point(0,2)); 
        mx_2.add(new Point(1,0)); mx_2.add(new Point(1,2)); mx_2.add(new Point(1,3)); 
        mx_2.add(new Point(2,0)); mx_2.add(new Point(2,1)); mx_2.add(new Point(2,2)); mx_2.add(new Point(2,3)); mx_2.add(new Point(2,4)); 
        mx_2.add(new Point(3,0)); mx_2.add(new Point(3,3));
    }

    @Ignore
    public void ellipseMetricsShouldPrintEig() {
        ems = new EllipseMetrics(mx_2);
        System.out.println(":: mx_1 - major axis length: "+ems.getMajorAxisLength());
        System.out.println(":: mx_1 - minor axis length: "+ems.getMinorAxisLength());
        System.out.println(":: mx_1 - eccentricity: "+ems.getEccentricity());
    }
}
