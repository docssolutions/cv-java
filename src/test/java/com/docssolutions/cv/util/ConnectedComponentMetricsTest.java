/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.util.ConnectedComponentMetrics;

public class ConnectedComponentMetricsTest
{
    private static final boolean[][] mx1 = {{false, false, false, false},
                                            {false, true,  true,  false},
                                            {false, true,  true,  false},
                                            {false, false, false, false}};

    private static final boolean[][] mx1a = {{false, false, false, false, false},
                                             {false, true,  true,  true, false},
                                             {false, true,  true,  true, false},
                                             {false, true,  true,  true, false},
                                             {false, false, false, false, false}};
    
    private static final boolean[][] mx2 = {{false, false, true,  false, false},
                                            {true,  false, true,  true,  false},
                                            {true,  true,  true,  true,  true },
                                            {true,  false, false, true,  false}};
    private LinkedList<Point> pixels_list;

    private LinkedList<Point> populatePixelsList(boolean[][] mx, LinkedList<Point> pixels_list)
    {
        pixels_list = new LinkedList<Point>();
        int i,j;
        for(i=0;i<mx.length;i++)
            for(j=0;j<mx[0].length;j++)
                if (mx[i][j])
                    pixels_list.add(new Point(j,i));
        return pixels_list;
    }
    
    @Ignore
    public void perimeterShouldPrintBorderPixels()
    {
        pixels_list = populatePixelsList(mx1a, pixels_list);
        ConnectedComponentMetrics ccm = new ConnectedComponentMetrics(0, pixels_list);
        // "component should have perimeter of 8"
        System.out.println(":: Perimeter: "+ccm.getPerimeter());
        assertEquals(7.476f, ccm.getPerimeter(), 0.01f);
    }

    @Ignore
    public void perimeterShouldPrintBorderPixelsEx1()
    {
        pixels_list = populatePixelsList(mx1, pixels_list);
        ConnectedComponentMetrics ccm = new ConnectedComponentMetrics(0, pixels_list);
        // "component should have perimeter of 8"
        System.out.println(":: Perimeter: "+ccm.getPerimeter());
        assertEquals(8.0f, ccm.getPerimeter(), 0.01f);
    }

    @Ignore
    public void boundingBoxShouldBeCalculated()
    {
        pixels_list = populatePixelsList(mx1, pixels_list);
        ConnectedComponentMetrics ccm = new ConnectedComponentMetrics(0, pixels_list);
        Point[] bb = ccm.getBoundingBox();
        assertEquals("top-left bounding box pixel must be (1,1)", new Point(1,1), bb[0]);
        assertEquals("bottom-right bounding box pixel must be (2,2)", new Point(2,2), bb[1]);
    }
}
