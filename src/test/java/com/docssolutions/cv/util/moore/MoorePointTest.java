/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.moore;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.util.moore.MoorePoint;

public class MoorePointTest
{
    @Ignore
    public void shouldVisitAllNeighborsFromSouthernPixel()
    {
        MoorePoint mp = new MoorePoint(new Point(2,10), new Point(2,11));
        // System.out.println(":: Initial point: "+mp);
        assertEquals("entry_pixel from mp should be (2,11)", new Point(2,11), mp.entryPoint());
        // System.out.println(":: After only entry_point checked: "+mp);
        assertEquals("next clockwise point should be (1,11)", new Point(1,11), (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
        assertEquals("next clockwise point should be (1,10)", new Point(1,10), (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
        assertEquals("next clockwise point should be (1,9)", new Point(1,9), (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
        assertEquals("next clockwise point should be (2,9)", new Point(2,9), (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
        assertEquals("next clockwise point should be (3,9)", new Point(3,9), (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
        assertEquals("next clockwise point should be (3,10)", new Point(3,10), (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
        assertEquals("next clockwise point should be (3,11)", new Point(3,11), (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
        assertEquals("next clockwise point should be null", null, (Point) mp.nextClockwisePoint());
        // System.out.println(":: After next clockwise point requested: "+mp);
    }

    @Ignore
    public void shouldVisitCorrectlyClockwisePixels()
    {
        MoorePoint mp = new MoorePoint(new Point(2,10), new Point(2,11));
        assertEquals("entry_pixel from mp should be (2,11)", new Point(2,11), mp.entryPoint());
        assertEquals("next clockwise point should be (2,9)", new Point(2,9), (Point) mp.nextClockwisePoint(new Point(1,9)));
        assertEquals("next clockwise point should be (1,10)", new Point(3,10), (Point) mp.nextClockwisePoint(new Point(3,9)));
        assertEquals("next clockwise point should be null", null, (Point) mp.nextClockwisePoint(new Point(3,11)));
    }

    @Ignore
    public void shouldCompareCorrectlyMoorePoints()
    {
        Point p1 = new Point(1,1);
        Point p2 = new Point(1,1);
        assertTrue("bla ", p1.equals(p2) );
        MoorePoint mp = new MoorePoint(new Point(2,10), new Point(2,11));
        MoorePoint mp2 = new MoorePoint(new Point(2,10), new Point(2,11));
        assertTrue("two identical moore point should be the same", mp.equals(mp2));
        MoorePoint mp3 = new MoorePoint(new Point(2,11), new Point(2,12));
        assertFalse("to moore point with different centers must be different", mp.equals(mp3));
        MoorePoint mp4 = new MoorePoint(new Point(2,10), new Point(2,9));
        assertFalse("to moore point with the same center but different entry points must be different", mp.equals(mp4));
    }
}
