/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.segmentation.Labeller;
import com.docssolutions.cv.util.RegionProperties;

public class RegionPropertiesTest
{
	private static TreeSet<String> ts;

    private static String in_base = "regionprops/inputRegionprops-mini2_L.csv";
    private static String in_ex1 = "regionprops/inputRegionprops-Ex1_L.csv";
    private static String in_ex2 = "regionprops/inputRegionprops-Ex2_L.csv";
    private static String in_ex3 = "regionprops/inputRegionprops-Ex3_L.csv";
    private static String in_ex4 = "regionprops/inputRegionprops-Ex4_L.csv";
    private static String in_ex5 = "regionprops/inputRegionprops-Ex5_L.csv";
    
    private static String out_base = "regionprops/outputRegionprops-mini2_RP.csv";
    private static String out_ex1 = "regionprops/outputRegionprops-Ex1_RP.csv";
    private static String out_ex2 = "regionprops/outputRegionprops-Ex2_RP.csv";
    private static String out_ex3 = "regionprops/outputRegionprops-Ex3_RP.csv";
    private static String out_ex4 = "regionprops/outputRegionprops-Ex4_RP.csv";
    private static String out_ex5 = "regionprops/outputRegionprops-Ex5_RP.csv";
    
    private LinkedList<ConnectedComponentMetrics> comps_base;
    private LinkedList<ConnectedComponentMetrics> comps_ex1;
    private LinkedList<ConnectedComponentMetrics> comps_ex2;
    private LinkedList<ConnectedComponentMetrics> comps_ex3;
    private LinkedList<ConnectedComponentMetrics> comps_ex4;
    private LinkedList<ConnectedComponentMetrics> comps_ex5;

    private float[][] props_base;
    private float[][] props_ex1;
    private float[][] props_ex2;
    private float[][] props_ex3;
    private float[][] props_ex4;
    private float[][] props_ex5;

    @Before
    public void fillDataFromCSV() {
		ts = new TreeSet<String>();
		ts.add("Centroid");
		ts.add("Area");
		ts.add("Perimeter");
		ts.add("MajorAxisLength");
		ts.add("MinorAxisLength");
		ts.add("Eccentricity");
		ts.add("EquivDiameter");

        comps_base = getRegionPropsFromCSV(in_base, ts);
        comps_ex1 = getRegionPropsFromCSV(in_ex1, ts);
        comps_ex2 = getRegionPropsFromCSV(in_ex2, ts);
        comps_ex3 = getRegionPropsFromCSV(in_ex3, ts);
        comps_ex4 = getRegionPropsFromCSV(in_ex4, ts);
        comps_ex5 = getRegionPropsFromCSV(in_ex5, ts);

        props_base = CVUtils.getMatrixFromCSV(out_base);
        props_ex1 = CVUtils.getMatrixFromCSV(out_ex1);
        props_ex2 = CVUtils.getMatrixFromCSV(out_ex2);
        props_ex3 = CVUtils.getMatrixFromCSV(out_ex3);
        props_ex4 = CVUtils.getMatrixFromCSV(out_ex4);
        props_ex5 = CVUtils.getMatrixFromCSV(out_ex5);
    }

    @Ignore
    public void inputWithRegionPropertiesShouldBeCorrectEx1()
    {
        float[][] new_mx = getPropertiesFloatMtrx(comps_ex1);
        //CVUtils.printMatrix(new_mx);

        float[][] diff_mx = CVUtils.markSquareDifferences(new_mx, props_ex1);
        //CVUtils.printPresenceInMatrix(diff_mx);

        int n = new_mx.length;
        int m = new_mx[0].length;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                assertEquals(new_mx[i][j], props_ex1[i][j], 0.0f); // Very weak threshold
    }

    private float[][] getPropertiesFloatMtrx(LinkedList<ConnectedComponentMetrics> components_metrics) {
		int size = components_metrics.size();
		int numProperties = 8;

		float[][] result = new float[size][numProperties];
		int pp = 0;

		for(ConnectedComponentMetrics m : components_metrics) {
			result[pp][0] = m.getCentroid().x;
			result[pp][1] = m.getCentroid().y;
			result[pp][2] = m.getArea();
			result[pp][3] = m.getPerimeter();
			result[pp][4] = m.getMajorAxisLength();
			result[pp][5] = m.getMinorAxisLength();
			result[pp][6] = m.getEccentricity();
			result[pp][7] = m.getEquivDiameter();

			pp++;
		}

		return result;
	}

    @Ignore
    public void baseInputRegionPropsShouldBeAsExpected() {
        printConnectedComponents(":: Printing base props with "+comps_base.size()+" components...", comps_base);
    }

    @Ignore
    public void example1InputRegionPropsShouldBeAsExpected() {
        printConnectedComponents(":: Printing example 1 props with "+comps_ex1.size()+" components...", comps_ex1);
    }

    @Ignore
    public void example2InputRegionPropsShouldBeAsExpected() {
        printConnectedComponents(":: Printing example 2 props with "+comps_ex2.size()+" components...", comps_ex2);
    }

    @Ignore
    public void example3InputRegionPropsShouldBeAsExpected() {
        printConnectedComponents(":: Printing example 3 props with "+comps_ex3.size()+" components...", comps_ex3);
    }

    @Ignore
    public void example4InputRegionPropsShouldBeAsExpected() {
        printConnectedComponents(":: Printing example 4 props with "+comps_ex4.size()+" components...", comps_ex4);
    }

    @Ignore
    public void example5InputRegionPropsShouldBeAsExpected() {
        printConnectedComponents(":: Printing example 5 props with "+comps_ex5.size()+" components...", comps_ex5);
    }

    private LinkedList<ConnectedComponentMetrics> getRegionPropsFromCSV(String filename, Set<String> ps)
    {
        float[][] mx = CVUtils.getMatrixFromCSV(filename);
        RegionProperties rp = new RegionProperties();
        LinkedList<ConnectedComponentMetrics> cm = rp.getRawRegionProperties(mx);
        rp.calculatePropertiesAPriori(ps);
        return cm;
    }

    private void printConnectedComponents(String msg, LinkedList<ConnectedComponentMetrics> components_metrics) {
        CVUtils.println(msg);
        for(ConnectedComponentMetrics m : components_metrics) {
            System.out.printf("C-X: %12.6f, "+
                              "C-Y: %12.6f, "+
                              "A: %6d, "+
                              "P: %10.6f, "+
                              "Major A L: %10.6f, "+
                              "Minor A L: %10.6f, "+
                              "Ecc: %10.6f, "+
                              "Equiv Diam: %10.6f",
                              m.getCentroid().x,
                              m.getCentroid().y,
                              m.getArea(),
                              m.getPerimeter(),
                              m.getMajorAxisLength(),
                              m.getMinorAxisLength(),
                              m.getEccentricity(),
                              m.getEquivDiameter());
            System.out.println();
        }
    }
}
