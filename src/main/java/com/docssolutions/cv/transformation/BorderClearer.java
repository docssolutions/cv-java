/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import java.awt.*;
import java.util.*;

import com.docssolutions.cv.CVUtils;
    
public class BorderClearer
{
    // Enumerators indicating 4 or 8 neighborhood connectivity.
    public static final int E_4CONN = 0;
    public static final int E_8CONN = 1;
    
    public BorderClearer() {}

    public float[][] applyRawBorderClearing(float[][] mx)
    {
        assert CVUtils.isBinaryMatrix(mx);

        int m = mx[0].length;
        int n = mx.length;
        int i, j;
        float[][] new_mx = new float[n][m];

        for (i = 0; i < n; i++)
            for (j = 0; j < m; j++)
                new_mx[i][j] = mx[i][j];

        for (j=0; j<m; j++) {
            if(new_mx[0][j] == 1.0f) wipe8NeighboorConnectedComponent(new_mx, new Point(j,0));
            if(new_mx[n-1][j] == 1.0f) wipe8NeighboorConnectedComponent(new_mx, new Point(j,n-1));
        }
        
        for (i=1; i<n-1; i++) {
            if(new_mx[i][0] == 1.0f) wipe8NeighboorConnectedComponent(new_mx, new Point(0,i));
            if(new_mx[i][m-1] == 1.0f) wipe8NeighboorConnectedComponent(new_mx, new Point(m-1,i));
        }

        return new_mx;
    }

    private void wipe4NeighboorConnectedComponent(float[][] mx, Point p) {wipeConnectedComponent(mx, p, E_4CONN);}
    private void wipe8NeighboorConnectedComponent(float[][] mx, Point p) {wipeConnectedComponent(mx, p, E_8CONN);}

    /**
     * @brief Wipe connected component at point p from matrix mx.
     *
     * @param mx must be a bidimensional float matrix.
     * @param p must be a given Point in the matrix.
     * @param conn indicates whether processing must be done with 4 or 8 neighbor connectivity.
     */

    private void wipeConnectedComponent(float[][] mx, Point p, int conn)
    {
        if ( mx[p.y][p.x] != 2.0f ) return;

        int m = mx[0].length;
        int n = mx.length;

        Queue<Point> queue = new LinkedList<Point>();
        queue.add(p);

        // Start consuming queue until empty. All 2.0f pixels will be turned off to 0.0f
        while (!queue.isEmpty()) {

            p = queue.remove();
            
            if (mx[p.y][p.x] == 2.0f) {
                //System.out.println(":: Point processed at ("+p.x+","+p.y+"), "+queue.size()+" remaining.");

                mx[p.y][p.x] = 0.0f;

                // 4 Neighbors
                if (0 <= p.y-1 && mx[p.y-1][p.x] == 2.0f) {queue.add(new Point(p.x, p.y-1));} // N
                if (p.y+1 < n  && mx[p.y+1][p.x] == 2.0f) {queue.add(new Point(p.x, p.y+1));} // S
                if (0 <= p.x-1 && mx[p.y][p.x-1] == 2.0f) {queue.add(new Point(p.x-1, p.y));} // W
                if (p.x+1 < m  && mx[p.y][p.x+1] == 2.0f) {queue.add(new Point(p.x+1, p.y));} // E

                if (conn == E_8CONN) {
                    // Last 4 neighbors
                    if (0 <= p.x-1 && 0 <= p.y-1 && mx[p.y-1][p.x-1] == 2.0f) {queue.add(new Point(p.x-1, p.y-1));} // NW
                    if (0 <= p.x-1 && p.y+1 < n  && mx[p.y+1][p.x-1] == 2.0f) {queue.add(new Point(p.x-1, p.y+1));} // SW
                    if (p.x+1 < m  && 0 <= p.y-1 && mx[p.y-1][p.x+1] == 2.0f) {queue.add(new Point(p.x+1, p.y-1));} // NE
                    if (p.x+1 < m  && p.y+1 < n  && mx[p.y+1][p.x+1] == 2.0f) {queue.add(new Point(p.x+1, p.y+1));} // SE
                }
            }
        }
    }
}
