/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import com.docssolutions.cv.CVUtils;

public class Eroder
{
    private float threshold = 140.0f;
    
    public Eroder() {}

    public float getThreshold() { return threshold; }
    public void setThreshold(float threshold) { this.threshold = threshold; }

    private static boolean[][] kernel_radius_5 = { {false, false, true, true, true, true, true, false, false},
                                                   {false, true, true, true, true, true, true, true, false},
                                                   {true, true, true, true, true, true, true, true, true},
                                                   {true, true, true, true, true, true, true, true, true},
                                                   {true, true, true, true, true, true, true, true, true},
                                                   {true, true, true, true, true, true, true, true, true},
                                                   {true, true, true, true, true, true, true, true, true},
                                                   {false, true, true, true, true, true, true, true, false},
                                                   {false, false, true, true, true, true, true, false, false} };

    public float[][] applyRawDiskErosion(float[][] mx, float radius)
    {
        int m = mx[0].length;
        int n = mx.length;
        float[][] new_mx = new float[n][m];

        int kernel_limit = (int) Math.floor(radius);
        int c = kernel_limit;
        int kn = 1 + 2 * kernel_limit;
        boolean[][] kernel = new boolean[kn][kn];
        int i, j;

        // Create disk shape values in square kernel.
        for(i=0;i<kn;i++)
            for(j=0;j<kn;j++)
                kernel[i][j] = Math.sqrt(Math.pow(c-i,2) + Math.pow(c-j, 2)) <= radius;

        //CVUtils.printMatrix(kernel);

        //CVUtils.printMatrix(mx);

        // Apply bitwise operations with kernel upon matrix.
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                if (radius == 5.0f) {
                    applyLocalKernelConvolution(mx, new_mx, kernel_radius_5, i, j);
                }else{
                    applyLocalKernelConvolution(mx, new_mx, kernel, i, j);
                }

        //CVUtils.printPresenceInMatrix(mx);

        //CVUtils.printPresenceInMatrix(new_mx);

        return new_mx;
    }
    
    private float[][] applyLocalKernelConvolution(float[][] mx, float[][] new_mx, boolean[][] kernel, int x, int y)
    {
        // We need to take care of the outer matrix boundaries.
        int m = mx[0].length;
        int n = mx.length;

        // We suppose the kernel is a square matrix
        int kn = kernel.length;

        // Actually (kn - 1) / 2 is more correct. We can get rid of substraction because integer casting truncation.
        int kernel_limit = kn / 2;
        int c = kernel_limit;

        int i, j;

        // Processed variable tests whether the pixel is already turned off.
        boolean processed = false;
        
        //System.out.println(":: applyLocallyBitwiseBla at ("+x+","+y+") =>");

        for(i=0;i<kn && !processed;i++) {
            for(j=0;j<kn && !processed;j++) {
                if (0<=(x+i-c) && (x+i-c)<n &&
                    0<=(y+j-c) && (y+j-c)<m &&
                    kernel[i][j] && 
                    mx[x+i-c][y+j-c] == 0.0f) {
                    //System.out.print(mx[x+i-c][y+j-c]+" ");
                    new_mx[x][y] = 0.0f;
                    processed = true;
                }
            }
            //System.out.println();
        }

        if (!processed) new_mx[x][y] = 1.0f;

        return mx;
    }
}
