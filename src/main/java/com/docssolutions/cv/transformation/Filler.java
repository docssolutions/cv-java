/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;

public class Filler
{
    public Filler() {}

    /**
     * @brief Apply raw holes filling algorith to bidimensional float matrix. It expect to have only
     *       0.0f and 1.0f values.
     * References: 
     * {@url http://www.mathworks.com/help/images/ref/imfill.html}
     * {@url http://en.wikipedia.org/wiki/Flood_fill}
     *
     * @param mx must be a bidimensional float matrix.
     *
     * @return An bidimensional float matrix instance with the result.
     * 
     * @see applyFloodFillAlgorithmFromPosition
     */
    
    public float[][] applyRawHolesFilling(float[][] mx)
    {
        //System.out.println(":: In applyRawHolesFilling");
        int m = mx[0].length;
        int n = mx.length;
        float[][] new_mx = new float[n][m];
        int i, j;

        //System.out.println(":: Print original matrix");
        //CVUtils.printPresenceInMatrix(mx);

        // Make copy of mx
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                new_mx[i][j] = mx[i][j];
        
        // From boundary, apply flood fill algorithm for background pixels.
        for(j=0;j<m;j++) {
            new_mx = applyFloodFillAlgorithmFromPosition(new_mx, new Point(j, 0));
            new_mx = applyFloodFillAlgorithmFromPosition(new_mx, new Point(j, n-1));
        }
        for(i=1;i<n-1;i++) {
            new_mx = applyFloodFillAlgorithmFromPosition(new_mx, new Point(0, i));
            new_mx = applyFloodFillAlgorithmFromPosition(new_mx, new Point(m-1, i));
        }

        // Take the special value of 2.0f to background pixel (0.0f) and everything else turn it to 1.0f.
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                new_mx[i][j] = new_mx[i][j] == 2.0f ? 0.0f : 1.0f;

        //System.out.println(":: Print filled matrix");
        //CVUtils.printPresenceInMatrix(new_mx);

        return new_mx;
    }

    /**
     * @brief Apply flood fill algorithm from a given position. It's based on scanline fill algorithm described 
     * in {@url http://en.wikipedia.org/wiki/Flood_fill}.
     * Further discussion can be tracked here:
     * {@url http://stackoverflow.com/questions/23907474/efficient-implementation-of-matlab-function-imfillbw-holes-in-c-not-usi}
     * {@url http://stackoverflow.com/questions/2783204/flood-fill-using-a-stack}
     *
     * @param mx must be a bidimensional float matrix.
     * @param p must be a given Point in the matrix.
     *
     * @return An bidimensional float matrix instance with the result.
     */

    private float[][] applyFloodFillAlgorithmFromPosition(float[][] mx, Point p)
    {
        if ( mx[p.y][p.x] != 0.0f ) return mx;

        int m = mx[0].length;
        int n = mx.length;
        int j;

        Queue<Point> queue = new LinkedList<Point>();
        int w, e;
        queue.add(p);
        
        // Start consuming queue until empty. Scan horizontally all background pixels and check each northern's and southern's
        // pixels if are also background pixels. Add them to queue if necessary.
        while (!queue.isEmpty()) {
            p = queue.remove();
            //System.out.println(":: Point processed at ("+p.x+","+p.y+"), "+queue.size()+" remaining.");

            w = p.x;
            e = p.x;

            while(0 <= w && mx[p.y][w] == 0.0f) { w--; } w++;
            while(e < m && mx[p.y][e] == 0.0f) { e++; } 

            //System.out.println(":: Line boundary set at columns "+w+" and "+e+".");

            for(j=w;j<e;j++) {
                mx[p.y][j] = 2.0f;
                if (0 <= p.y-1 && mx[p.y-1][j] == 0.0f) {
                    queue.add(new Point(j, p.y-1));
                }
                if (p.y+1 < n && mx[p.y+1][j] == 0.0f) {
                    queue.add(new Point(j, p.y+1));
                }
            }
        }

        return mx;
    }
}
