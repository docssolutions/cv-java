/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.transformation.Dilater;
import com.docssolutions.cv.transformation.Eroder;

public class MorphologicalCloser
{
    private float threshold = 140.0f;
    
    public MorphologicalCloser() {}

    public float getThreshold() { return threshold; }
    public void setThreshold(float threshold) { this.threshold = threshold; }

    public float[][] applyMorphologicalRawDiskClosing(float[][] mx, float radius)
    {
        float[][] new_mx;
        new_mx = new Dilater().applyRawDiskDilation(mx, radius);
        new_mx = new Eroder().applyRawDiskErosion(new_mx, radius);
        return new_mx;
    }
}
