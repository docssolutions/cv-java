/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import com.docssolutions.cv.CVUtils;

public class Dilater
{
    private float threshold = 140.0f;
    
    public Dilater() {}

    public float getThreshold() { return threshold; }
    public void setThreshold(float threshold) { this.threshold = threshold; }

    private static float[][] kernel_radius_5 = { {0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f},
                                                   {0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f},
                                                   {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
                                                   {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
                                                   {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
                                                   {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
                                                   {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f},
                                                   {0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f},
                                                   {0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f} };

    public float[][] applyRawDiskDilation(float[][] mx, float radius)
    {
        int m = mx[0].length;
        int n = mx.length;
        float[][] new_mx = new float[n][m];

        int kernel_limit = (int) Math.floor(radius);
        int c = kernel_limit;
        int kn = 1 + 2 * kernel_limit;
        float[][] kernel = new float[kn][kn];
        int i, j;

        // Create disk shape values in square kernel.
        for(i=0;i<kn;i++)
            for(j=0;j<kn;j++)
                kernel[i][j] = Math.sqrt(Math.pow(c-i,2) + Math.pow(c-j, 2)) <= radius ? 1.0f : 0.0f;

        //CVUtils.printMatrix(kernel);

        //CVUtils.printMatrix(mx);

        // Apply bitwise operations with kernel upon matrix.
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                if (radius == 5.0f) {
                    applyLocalBitwiseKernelConvolution(mx, new_mx, kernel_radius_5, i, j);
                }else{
                    applyLocalBitwiseKernelConvolution(mx, new_mx, kernel, i, j);
                }

        //CVUtils.printPresenceInMatrix(mx);

        //CVUtils.printPresenceInMatrix(new_mx);

        return new_mx;
    }
    
    private float[][] applyLocalBitwiseKernelConvolution(float[][] mx, float[][] new_mx, float[][] kernel, int x, int y)
    {
        // We need to take care of the outer matrix boundaries.
        int m = mx[0].length;
        int n = mx.length;

        // We suppose the kernel is a square matrix
        int kn = kernel.length;

        // Actually (kn - 1) / 2 is more correct. We can get rid of substraction because integer casting truncation.
        int kernel_limit = kn / 2;
        int c = kernel_limit;

        int i, j;

        // Presence variable is sensitive to non-zero neighborhood values. If any of the locally marked values in the 
        // kernel overlapped with the main matrix at (x,y) is a non-zero, presence will be non-zero.
        float presence = 0.0f;
        
        //System.out.println(":: applyLocallyBitwiseBla at ("+x+","+y+") =>");

        for(i=0;i<kn;i++) {
            for(j=0;j<kn;j++) {
                if (0<=(x+i-c) && (x+i-c)<n &&
                    0<=(y+j-c) && (y+j-c)<m) {
                    //System.out.print(mx[x+i-c][y+j-c]+" ");
                    presence += mx[x+i-c][y+j-c] * kernel[i][j];
                }
            }
            //System.out.println();
        }

        // If presence value is non-zero, we'll set it to one, otherwise zero.
        new_mx[x][y] = presence != 0.0f ? 1.0f : 0.0f;

        //System.out.println(":: presence value is: "+presence);
        return mx;
    }
}
