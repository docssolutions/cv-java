/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.util.meijster.DistanceTransform;
import com.docssolutions.cv.util.meijster.matrix.*;

/**
 * @brief This transformation takes a binary image and calculates its euclidian distance transformation, namely, the distance
 *       from each pixel to its closest non-background neighboor.
 * References
 * - {@url http://stackoverflow.com/questions/7426136/fastest-available-algorithm-for-distance-transform}
 * - {@url http://liu.diva-portal.org/smash/get/diva2:23335/FULLTEXT01}
 * - {@url https://github.com/vinniefalco/LayerEffects}
 * - {@url http://en.wikipedia.org/wiki/Fast_marching_method}
 * - {@url http://www.math.uci.edu/~zhao/publication/mypapers/pdf/fastsweep1.pdf}
 * - {@url http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.102.1717}
 * - {@url http://cs.brown.edu/~pff/dt/}
 * Base algorithm taken from this paper:
 * - {@url http://fab.cba.mit.edu/classes/S62.12/docs/Meijster_distance.pdf}
 * Base Java implementation from:
 * - {@url http://palea.cgrb.oregonstate.edu/svn/jaiswallab/Annotation/src/ie/dcu/image/dt/DistanceTransform.java}
 *
 */

public class EuclidianDistanceTransformer
{
    public EuclidianDistanceTransformer() {}

    /**
     * Apply Meijster distance transformation algorithm to matrix.
     *
     * References:
     * - {@url http://palea.cgrb.oregonstate.edu/svn/jaiswallab/Annotation/src/ie/dcu/image/dt/DistanceTransform.java}
     *
     * @param mx must be a bidimensional float matrix.
     *
     * @return An bidimensional float matrix instance with the result.
     * 
     * @see 
     */
    
    public float[][] applyRawDistanceTransformation(float[][] mx)
    {
        assert CVUtils.isBinaryMatrix(mx);

        //System.out.println(":: In applyRawDistanceTransformation");
        int m = mx[0].length;
        int n = mx.length;

        // Optimized Meijster algorithm for EDT. O(n*m)
		DistanceTransform dt = new DistanceTransform();
		dt.init(new FloatMatrix(mx), 0);
		//FloatMatrix new_mx = dt.computeTransformFloat();
		FloatMatrix new_mx = dt.computeMultithreadedTransformFloat();

        return CVUtils.transformMatrixFrom1To2D(new_mx.values(), m, n);

        //// Naive approach for EDT. O(n^2*m^2)
        //int i, j, ii, jj;
        //float[][] new_mx = new float[n][m];
        //for(i=0;i<n;i++)
        //    for(j=0;j<m;j++)
        //        new_mx[i][j] = Float.MAX_VALUE;

        //for(i=0;i<n;i++)
        //    for(j=0;j<m;j++)
        //        if (mx[i][j] != 0.0f)
        //            for(ii=0;ii<n;ii++)
        //                for(jj=0;jj<m;jj++)
        //                    if (new_mx[ii][jj] > edt(i,j,ii,jj))
        //                        new_mx[ii][jj] = edt(i,j,ii,jj);

        //return new_mx;
    }

    private float edt(int i, int j, int ii, int jj)
    {
        return (float) Math.sqrt((ii-i)*(ii-i) + (jj-j)*(jj-j));
    }

    ///**
    // * @brief Auxiliary class holding values for component management.
    // */
    private float[][] markForegroundPixelsWithInfinity(float[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i, j;

        float inf = Float.MAX_VALUE - (float)(n * n - m * m);

        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                mx[i][j] = mx[i][j] == 1.0f ? inf : 0.0f;
        return mx;
    }

    /**
     * Performs the column transform
     */
    private float[][] transformCols(float[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i, j, b;
                
        for (j = 0; j < m; j++) {

            // Forward
            for (i = 1, b = 1; i < n; i++) {
                System.out.println(":: At ("+i+","+j+"): "+mx[i][j]+" with val in i-1 of "+mx[i-1][j]);
                if (mx[i][j] > mx[i-1][j]) {
                    System.out.println(":: posterior greater..., changing current to "+(mx[i-1][j] + (float) b));
                    mx[i][j] = mx[i-1][j] + (float) b;
                    b += 2;
                } else {
                    b = 1;
                }
            }
            
            // Backward
            for (i = n - 2, b = 1; i >= 0; i--) {
                System.out.println(":: At ("+i+","+j+"): "+mx[i][j]+" with val in i+1 of "+(mx[i+1][j]+ (float) b));
                if (mx[i][j] > mx[i+1][j] + (float) b) {
                    System.out.println(":: prior greater..., changing current to "+(mx[i+1][j] + (float) b));
                    mx[i][j] = mx[i+1][j] + (float) b;
                    b += 2;
                } else {
                    b = 1;
                }
            }

            CVUtils.printMatrix(mx);
        }
        return mx;
    }
    
    /**
     * Meijsters f function
     */
    private static float f(int x, int i, float pixel) {
        return ((float)((x - i)*(x - i))) + pixel;
    }

    /**
     * Performs the row transform
     */
    private float[][] transformRows(float[][] mx)
    {
        // Meijster row transform
        int m = mx[0].length;
        int n = mx.length;
        int i, j, q, u, w;
        float im_r_u;
        
        int[] s = new int[m];
        int[] t = new int[m];
        float[] r = new float[m];
        
        for (i = 0; i < n; i++) {
            q = s[0] = t[0] = 0;
            
            for (u = 1; u < m; ++u) {
                im_r_u = mx[i][u];
                
                while (q != -1 && f(t[q], s[q], mx[i][s[q]]) > f(t[q], u, im_r_u)) {
                    --q;
                }
                
                if (q == -1) {
                    q = 0;
                    s[0] = u;
                } else {
                    w = 1 + ((u * u - (s[q] * s[q]) + (int)(mx[i][u] - mx[i][s[q]])) / (2 * (u - s[q]))); // [WARNING] Truncating to int!!
                    if (w < m) {
                        ++q;
                        s[q] = u;
                        t[q] = w;
                    }
                }
            }
            
            for (j=0;j<m;j++)
                r[j] = mx[i][j];

            for (u = m - 1; u != -1; --u) {
                mx[i][u] = f(u, s[q], r[s[q]]);

                if (u == t[q]) {
                    --q;
                }
            }
        }
        return mx;
    }
}
