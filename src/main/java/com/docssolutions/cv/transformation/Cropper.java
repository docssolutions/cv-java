/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;
    
public class Cropper
{
    // Enumerators indicating 4 or 8 neighborhood connectivity.
    public static final int E_4CONN = 0;
    public static final int E_8CONN = 1;
    
    public Cropper() {}

    public float[][] applyRawCropping(float[][] mx, int x, int y, int nw, int nh)
    {
        //assert CVUtils.isBinaryMatrix(mx);

        int m = mx[0].length;
        int n = mx.length;

        assert x + nw + 1 < m;
        assert y + nh + 1 < n;

        int i, j;
        float[][] new_mx = new float[nh + 1][nw + 1];

        for (i=0; i<=nh; i++)
            for (j=0; j<=nw; j++)
                if ( 0 <= y+i-1 && y+i-1 < n && 0 <= x+j-1 && x+j-1 < m )
                    new_mx[i][j] = mx[y+i-1][x+j-1];
        
        return new_mx;
    }
}
