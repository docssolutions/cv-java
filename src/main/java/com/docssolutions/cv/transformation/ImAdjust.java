/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.transformation;

import java.io.*;
import java.util.*;
import java.awt.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.util.MatrixUtils;
import com.docssolutions.cv.filter.ImgPlusPrinter;
import com.docssolutions.cv.filter.Convolver;

public class ImAdjust
{
	// the following are the input parameters, with default values assigned to them
    static int kw            = 5;
    static int kh            = 5;
    static double sigma      = 0.5;
    static double sigma2     = Math.pow(sigma,2);
	static boolean sstats    = false; // display xdt value in each iteration
	static boolean tstats    = false; // measure needed runtime
		
    Convolver myConv = new Convolver();
	   
    public ImAdjust() {}

    private double sum_of(float[][] _kernel, int _kw, int _kh) 
    {
        int i, j = 0;
        float sum = (float) 0.0;
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                sum += _kernel[i][j];
        return (double) sum;
    }

    private double sum_of(double[][] _kernel, int _kw, int _kh) 
    {
        int i, j = 0;
        double sum = (double) 0.0;
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                sum += _kernel[i][j];
        return (double) sum;
    }

    private double max_of(float[][] _kernel, int _kw, int _kh) 
    {
        int i, j = 0;
        float max = _kernel[0][0];
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                if(max < _kernel[i][j])
                    max = _kernel[i][j];
        return (double) max;
    }

    private double max_of(double[][] _kernel, int _kw, int _kh) 
    {
        int i, j = 0;
        double max = _kernel[0][0];
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                if(max < _kernel[i][j])
                    max = _kernel[i][j];
        return max;
    }

    private void threshold(float[][] _kernel, int _kw, int _kh, float _th)
    {
        int i, j = 0;
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                if(_kernel[i][j]<_th)
                    _kernel[i][j] = (float) 0.0;
    }

    private void threshold(double[][] _kernel, int _kw, int _kh, double _th)
    {
        int i, j = 0;
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                if(_kernel[i][j]<_th)
                    _kernel[i][j] = (double) 0.0;
    }

    private float[] convert_kernel_to_1d(float[][] _kernel, int _kw, int _kh)
    {
        float[] _kernel_1d = new float[_kw*_kh];
        int i, j = 0;
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                _kernel_1d[i*_kw+j] = _kernel[i][j];
        return _kernel_1d;
    }
    
    private double[] convert_kernel_to_1d(double[][] _kernel, int _kw, int _kh)
    {
        double[] _kernel_1d = new double[_kw*_kh];
        int i, j = 0;
        for(i=0;i<_kh;i++)
            for(j=0;j<_kw;j++)
                _kernel_1d[i*_kw+j] = _kernel[i][j];
        return _kernel_1d;
    }

    public int[] getHistogram(int[] img) {
        int[] histogram = new int[256];
        for (int i = 0; i < img.length; i++) {
            int v = img[i];
            histogram[v]++;
        }
        // °J° ImgPlusPrinter.print_img_pixels_I(histogram,256,1);
        return histogram;
    }
    
    public double[] cumSum(int[] img) {
        double[] cumulative = new double[256];
        double acc = 0;
        for (int i = 0; i < img.length; i++) {
            acc += (double) img[i];
            cumulative[i] = acc;
        }
        // °J° ImgPlusPrinter.print_img_pixels_I(cumulative,256,1);
        return cumulative;
    }
    
    // °J° public float[] cumSum(int[] img) {
        // °J° float[] cumulative = new float[256];
        // °J° float acc = 0;
        // °J° for (int i = 0; i < img.length; i++) {
            // °J° acc += (float) img[i];
            // °J° cumulative[i] = acc;
        // °J° }
        // °J° return cumulative;
    // °J° }
    
    public int sum (int[] arr){
        int s = 0;
        for (int e : arr) s += e;
        // °J° System.out.println(s);
        return s;
    }
    
    public int find_gt(double[] arr, double thr, int pos){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > thr) 
                if ( --pos == 0)
                    return i;   
        }
        return -1;
    }
    
    public int find_gt(float[] arr, float thr, int pos){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > thr) 
                if ( --pos == 0)
                    return i;   
        }
        return -1;
    }
    
    public int find_ge(double[] arr, double thr, int pos){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= thr) 
                if ( --pos == 0)
                    return i;   
        }
        return -1;
    }
    
    public int find_ge(float[] arr, float thr, int pos){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= thr) 
                if ( --pos == 0)
                    return i;   
        }
        return -1;
    }
    
    public double[] stretchlim(int[] img){
        double[] lowhigh = new double[2];
        double[] ilowhigh = new double[2];
        int nbins = 256;
        double tol_low = 0.01;
        double tol_high = 0.99;
        int[] N = getHistogram(img);
        double[] cdf = CVUtils.divEW(cumSum(N),(double)sum(N)); 
        //double[] cdf = MatrixUtils.divEW(cumSum(N), (double) sum(N)); 
        // °J° ImgPlusPrinter.print_img_pixels_D(cdf,1,nbins);
        int ilow = find_gt(cdf, tol_low, 1);
        // °J° System.out.println("ilow: "+ilow);
        int ihigh = find_ge(cdf, tol_high, 1);
        // °J° System.out.println("ihigh: "+ihigh);
        if (ilow == ihigh)   {
            ilowhigh[0] = 0;
            ilowhigh[1] = nbins-1;
        } else {
            ilowhigh[0] = ilow;
            ilowhigh[1] = ihigh;
        }
        lowhigh[0] = (ilowhigh[0])/(nbins-1); 
        lowhigh[1] = (ilowhigh[1])/(nbins-1); 
        return lowhigh;
    }

	public float[][] applyImAdjustArray(float[][] input_mat) {
		int[] mtrx = CVUtils.convertFromFloatToInt(CVUtils.transformMatrixFrom2To1D(input_mat));
		float[] pixs = CVUtils.convertFromIntToFloat(this.applyImAdjustArray(mtrx));

		return CVUtils.transformMatrixFrom1To2D(pixs, input_mat[0].length, input_mat.length);
	}

	public int[] applyImAdjustArray(int[] pixs) {
        double low_in  = 0;
        double high_in  = 1;
        double low_out  = 0;
        double high_out = 1;
        int gamma = 1;
        // °J° float

		double[] lowhigh_in = stretchlim(pixs);
        low_in = lowhigh_in[0];
        high_in = lowhigh_in[1];
        // °J° System.out.println(String.format("(%f, %f)", low_in, high_in));
        int expansionFactor = 1;
        double[] npixs = CVUtils.im2double(pixs);
        // °J° ImgPlusPrinter.print_img_pixels_F(npixs, width, height,8);
        // °J° img(:) =  max(lIn(d,:), min(hIn(d,:),img));
        npixs = CVUtils.minMat(npixs, high_in);
        npixs = CVUtils.maxMat(npixs, low_in);
        // °J° out = ( (img - lIn(d,:)) ./ (hIn(d,:) - lIn(d,:)) ) .^ (g(d,:));
        npixs = CVUtils.addEW(npixs, -low_in);
        npixs = CVUtils.divEW(npixs, high_in - low_in);
        // °J° As g is 1, there is no need to apply the power operation
        // °J° npixs = CVUtils.divEW(npixs, high_in - low_in);
        
        // °J° As hOut is 1, and lOut = 0, there is no need to do this either
        // °J° out(:) = out .* (hOut(d,:) - lOut(d,:)) + lOut(d,:);
        
        pixs = CVUtils.im2int(npixs);

        return pixs;
	}

}
