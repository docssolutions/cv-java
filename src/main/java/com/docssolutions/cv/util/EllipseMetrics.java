/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.*;

import com.docssolutions.cv.CVUtils;

public class EllipseMetrics
{
    //private float xbar;
    //private float ybar;
    private double xbar;
    private double ybar;
    private int size;
    //private float eccentricity;
    //private float orientation;
    //private float major_axis_length;
    //private float minor_axis_length;
    private double eccentricity;
    private double orientation;
    private double major_axis_length;
    private double minor_axis_length;

    //private LinkedList<Point2D.Float> pixels_list;
    private LinkedList<Point2D.Double> pixels_list;
    private float[][] cov;
    private float[][] eigen_vectors;
    private float[] eigen_values;
        
    public EllipseMetrics()
    {
        this(null);
    }

    public EllipseMetrics(LinkedList<Point> pixels_list)
    {
        this.eccentricity = -1.0f;
        this.orientation = -1.0f;
        this.major_axis_length = -1.0f;
        this.minor_axis_length = -1.0f;
        setPixelsList(pixels_list);
    }

    public boolean hasExistingPixelsList() {
        return pixels_list != null;
    }

    public void setPixelsList(LinkedList<Point> pixels_list) {
        if(pixels_list != null) {
            size = pixels_list.size();
            //Point2D.Float c = ConnectedComponentUtils.getCentroid(pixels_list);
            Point2D.Double c = ConnectedComponentUtils.getDoubleCentroid(pixels_list);
            //CVUtils.println("centroid at ("+c.x+","+c.y+")");
            xbar = c.x;
            ybar = c.y;
            //this.pixels_list = new LinkedList<Point2D.Float>();
            this.pixels_list = new LinkedList<Point2D.Double>();
            Iterator i = pixels_list.iterator();
            Point p;
            int j = 0;
            while(i.hasNext()) {
                p = (Point) i.next();
                //if(j++ < 20)
                //    CVUtils.println("example p at ("+p.x+","+p.y+")");
                //this.pixels_list.add(new Point2D.Float((float) p.x - xbar,-((float) p.y - ybar)));
                this.pixels_list.add(new Point2D.Double((double) p.x - xbar,-((double) p.y - ybar)));
            }
        }else{
            this.pixels_list = null;
        }
    }

    private void calculateAllMetrics()
    {
        //float uxx = 0.0f;
        //float uyy = 0.0f;
        //float uxy = 0.0f;
        double uxx = 0.0;
        double uyy = 0.0;
        double uxy = 0.0;

        Iterator i = pixels_list.iterator();
        //Point2D.Float p;
        Point2D.Double p;
        int j = 0;
        //CVUtils.println(":: centered points");
        while(i.hasNext()) {
            //p = (Point2D.Float) i.next();
            p = (Point2D.Double) i.next();
            //if(j++ < 20)
            //CVUtils.println("example p at ("+p.x+","+p.y+")");
            //uxx += (float) Math.pow(p.x, 2);
            //uyy += (float) Math.pow(p.y, 2);
            //uxy += p.x * p.y;
            uxx += Math.pow(p.x, 2);
            uyy += Math.pow(p.y, 2);
            uxy += (double) (p.x * p.y);
        }

        //uxx = uxx / (float) size + (1.0f/12.0f);
        //uyy = uyy / (float) size + (1.0f/12.0f);
        //uxy /=  (float) size;
        uxx = uxx / (double) size + (1.0/12.0);
        uyy = uyy / (double) size + (1.0/12.0);
        uxy /=  (double) size;

        //CVUtils.println(":: n="+size);
        //CVUtils.println(":: uxx="+uxx);
        //CVUtils.println(":: uyy="+uyy);
        //CVUtils.println(":: uxy="+uxy);

        //float common = (float) (Math.sqrt(Math.pow(uxx - uyy, 2) + 4 * Math.pow(uxy, 2)));
        double common = Math.sqrt(Math.pow(uxx - uyy, 2) + 4 * Math.pow(uxy, 2));

        //CVUtils.println(":: common="+common);

        major_axis_length = (float) (2 * Math.sqrt(2) * Math.sqrt(uxx + uyy + common));
        minor_axis_length = (float) (2 * Math.sqrt(2) * Math.sqrt(uxx + uyy - common));
        eccentricity = (float) (2 * Math.sqrt(Math.pow(major_axis_length / 2.0, 2) -
                                              Math.pow(minor_axis_length / 2.0, 2)) / major_axis_length);
        //float num, den;
        double num, den;
        if (uyy > uxx) {
            //num = uyy - uxx + (float) Math.sqrt(Math.pow(uyy - uxx, 2) + 4 * Math.pow(uxy, 2));
            //den = 2 * uxy;
            num = uyy - uxx + Math.sqrt(Math.pow(uyy - uxx, 2) + 4 * Math.pow(uxy, 2));
            den = 2 * uxy;
        }else{
            //num = 2 * uxy;
            //den = uxx - uyy + (float) Math.sqrt(Math.pow(uxx - uyy, 2) + 4 * Math.pow(uxy, 2));
            num = 2 * uxy;
            den = uxx - uyy + Math.sqrt(Math.pow(uxx - uyy, 2) + 4 * Math.pow(uxy, 2));
        }
        if (num == 0.0f || den == 0.0f) {
            orientation = 0.0f;
        }else{
            orientation = (float) (180.0 / Math.PI * Math.atan(num / den));
        }
    }

    public float getEccentricity() 
    {
        if(eccentricity != -1.0f) {
            return (float) eccentricity;
        }


        calculateAllMetrics();

        return (float) eccentricity;
    }
    
    public float getOrientation()
    {
        if(orientation != -1.0f) {
            return (float) orientation;
        }

        calculateAllMetrics();

        return (float) orientation;
    }

    /**
     * References
     * - {@url http://stackoverflow.com/questions/1532168/what-are-the-second-moments-of-a-region}
     */
    
    public float getMajorAxisLength()
    {
        if(major_axis_length != -1.0f) {
            return (float) major_axis_length;
        }

        //float[][] px_mx = CVUtils.pixelsToMatrix(pixels_list);
        ////CVUtils.printMatrix(px_mx);
        //cov = CVUtils.covarianceMatrix(px_mx);

        //eigen_vectors = CVUtils.eigenVectors(cov);
        //eigen_values = CVUtils.eigenValues(cov);
        //
        //major_axis_length = (float) Math.max(eigen_values[0],eigen_values[1]);
        //minor_axis_length = (float) Math.min(eigen_values[0],eigen_values[1]);
        
        calculateAllMetrics();

        return (float) major_axis_length;
    }
    
    public float getMinorAxisLength()
    {
        if(minor_axis_length != -1.0f) {
            return (float) minor_axis_length;
        }

        //float[][] px_mx = CVUtils.pixelsToMatrix(pixels_list);
        //cov = CVUtils.covarianceMatrix(px_mx);

        //eigen_vectors = CVUtils.eigenVectors(cov); // Where do I calculate this?
        //eigen_values = CVUtils.eigenValues(cov); // Where do I calculate this?
        //
        //major_axis_length = eigen_values[0]; // Major?
        //minor_axis_length = eigen_values[1]; // Minor?

        calculateAllMetrics();

        return (float) minor_axis_length;
    }
    
}
