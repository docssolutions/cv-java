/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import java.awt.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;
import javax.imageio.*;

import csv.*;
import csv.impl.*;

import Jama.Matrix;
import Jama.EigenvalueDecomposition;

import com.docssolutions.cv.util.moore.MoorePoint;

public class MatrixUtils
{
    // ================================================================================
    // Matrix Operations - Element-wise

    // Division element-wise

    /**
     * Element wise division function. Performs a division operation over every element 
     * (creates a new float array)
     * @param arr a float bidimentional array which is going to be modified
     * @param dividend a float that divide arr element wise
     */
    public static float[] divEW(float[] arr, float dividend){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        return divEW(arr, result, dividend);
    }

    /**
     * Element wise division function. Performs a division operation over every element 
     * (updates the result onto a receiving matrix)
     * @param arr a float bidimentional array which is going to be modified
     * @param wk_arr a float bidimentional array that will carry out the result
     * @param dividend a float that divide arr element wise
     */
    public static float[] divEW(float[] arr, float[] wk_arr, float dividend){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        for (int i = 0; i< arr.length; i++){
            wk_arr[i] = (float)(arr[i]/dividend);
        }
        return wk_arr;
    }

    // Inverted Division element-wise

    /**
     * Element wise inverted division function. Performs a division operation over every element 
     * (creates a new float array)
     * @param arr a float bidimentional array which is going to be modified
     * @param divisor a float that divide arr element wise
     */
    public static float[] divInvEW(float[] arr, float divisor){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        return divInvEW(arr, result, divisor);
    }
    
    /**
     * Element wise inverted division function. Performs a division operation over every element 
     * (updates the result onto a receiving matrix)
     * @param arr a float bidimentional array which is going to be modified
     * @param wk_arr a float bidimentional array that will carry out the result
     * @param divisor a float that divide arr element wise
     */
    public static float[] divInvEW(float[] arr, float[] wk_arr, float divisor){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        for (int i = 0; i< arr.length; i++){
            wk_arr[i] = (float) (divisor/arr[i]);
        }
        return wk_arr;
    }
    
    // Exponential element-wise

    /**
     * Element wise exp function. Performs an exponential operation over every element
     * (creates a new float array)
     * @param arr a float bidimentional array which is going to be modified
     */
    public static float[] expEW(float[] arr){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        return expEW(arr, result);
    }

    /**
     * Element wise exp function. Performs an exponential operation over every element
     * (updates the result onto a receiving matrix)
     * @param arr a float bidimentional array which is going to be modified
     * @param wk_arr a float bidimentional array that will carry out the result
     */
    public static float[] expEW(float[] arr, float[] wk_arr){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        for (int i = 0; i< arr.length; i++){
            wk_arr[i] = (float) (Math.exp(arr[i]));
        }
        return wk_arr;
    }

    // Power element-wise

    /**
     * Element wise pow function. Performs a pow operation over every element
     * (creates a new float array)
     * name: powEW
     * @param arr a float bidimentional array which is going to be modified
     * @param exponent a float that power arr element wise
     */
    public static float[] powEW(float[] arr, int exponent){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        return powEW(arr, result, exponent);
    }

    /**
     * Element wise pow function. Performs a pow operation over every element
     * (updates the result onto a receiving matrix)
     * @param arr a float bidimentional array which is going to be modified
     * @param wk_arr a float bidimentional array that will carry out the result
     * @param exponent a float that power arr element wise
     */
    public static float[] powEW(float[] arr, float[] wk_arr, int exponent){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        for (int i = 0; i< arr.length; i++){
            wk_arr[i] = (float) Math.pow(arr[i], exponent);
        }
        return wk_arr;
    }

    // Addition element wise

    /**
     * Element wise add function. Performs a addition operation over every element
     * (creates a new float array)
     * @param arr a float bidimentional array which is going to be modified
     * @param sum a float that is added to each element of arr
     */
    public static float[] addEW(float[] arr, float sum){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        return addEW(arr, result, sum);
    }

    /**
     * Element wise add function. Performs a addition operation over every element
     * (updates the result onto a receiving matrix)
     * @param arr a float bidimentional array which is going to be modified
     * @param sum a float that is added to each element of arr
     */
    public static float[] addEW(float[] arr, float[] wk_arr, float sum){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)(arr[i]+sum);
        }
        return result;
    }

    // Multiplication element wise

    /**
     * Element wise multiplication function. Performs a multiplication operation over every element
     * (creates a new float array)
     * @param arr a float bidimentional array which is going to be modified
     * @param factor is the factor with which each element is multiplicated.
     * 
     */
    public static float[] timesScalarEW(float[] arr, float factor){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        return timesScalarEW(arr, result, factor);
    }

    public static float[] timesScalarEW(float[] arr, float[] wk_arr, float factor){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        for (int i = 0; i< arr.length; i++){
            wk_arr[i] = (float)(arr[i]*factor);
        }
        return wk_arr;
    }
    
    // END of Matrix Operations - Element-wise
    // ................................................................................

    // ================================================================================
    // Operations over Matrices

    // Multiplication of two matrices
    
    /**
     * Element wise matrix multiplication function. Performs a multiplication operation over every element of each matrix.
     * @param A a float bidimentional array which is going to be modified
     * @param B a float bidimentional array which is going to be multiplied
     */
    public static float[] timesMatEW(float[] A, float[] B) throws IllegalArgumentException{
        if ((A.length == 0) || (B.length == 0)){
            // °J° System.out.println("array is empty");
            throw new IllegalArgumentException("array is empty");
        }
        if (A.length != B.length){
            // °J° System.out.println("arrays should be the same size");
            throw new IllegalArgumentException("arrays should be the same size");
        }
            
        float[] result =  new float[A.length];
        return timesMatEW(A, result, B);
    }

    // TODO javadoc

    public static float[] timesMatEW(float[] A, float[] wk_arr, float[] B) throws IllegalArgumentException{
        if ((A.length == 0) || (B.length == 0)){
            // °J° System.out.println("array is empty");
            throw new IllegalArgumentException("array is empty");
        }
        if (A.length != B.length){
            // °J° System.out.println("arrays should be the same size");
            throw new IllegalArgumentException("arrays should be the same size");
        }
        for (int i = 0; i< A.length; i++){
            wk_arr[i] = (float)(A[i] * B[i]);
        }
        return wk_arr;
    }

    // Addition of two matrices

    /**
     * Element wise matrix addition function. Performs an addition operation over every element of each matrix
     * @param A a float bidimentional array which is going to be modified
     * @param B a float bidimentional array which is going to be multiplied
     * 
     */
    public static float[] addMatEW(float[] A, float[] B) throws IllegalArgumentException{
        if ((A.length == 0) || (B.length == 0)){
            // °J° System.out.println("array is empty");
            throw new IllegalArgumentException("array is empty");
        }
        if (A.length != B.length){
            // °J° System.out.println("arrays should be the same size");
            throw new IllegalArgumentException("arrays should be the same size");
        }
            
        float[] result =  new float[A.length];
        return addMatEW(A, result, B);
    }

    public static float[] addMatEW(float[] A, float[] wk_arr, float[] B) throws IllegalArgumentException{
        if ((A.length == 0) || (B.length == 0)){
            // °J° System.out.println("array is empty");
            throw new IllegalArgumentException("array is empty");
        }
        if (A.length != B.length){
            // °J° System.out.println("arrays should be the same size");
            throw new IllegalArgumentException("arrays should be the same size");
        }
        for (int i = 0; i< A.length; i++){
            wk_arr[i] = (float)(A[i] + B[i]);
        }
        return wk_arr;
    }
    
    
    // END of Matrix Operations
    // ................................................................................

    // ================================================================================
    // Casting equivalents for Matrix Operations...

    public static float[] addEW(float[] arr, int sum){
        return addEW(arr, (float) sum);
    }

    // END of equivalent casting.
    // ................................................................................

    // ================================================================================
    // General utilities

    public static float[] clearMatrix(float[] arr){
        int n = arr.length;
        for (int i = 0; i < n; i++)
            arr[i] = 0.0f;
        return arr;
    }

    public static float[][] clearMatrix(float[][] mx){
        int n = mx.length;
        int m = mx[0].length;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                mx[i][j] = 0.0f;
        return mx;
    }

    // END of general utilities
    // ................................................................................

}
