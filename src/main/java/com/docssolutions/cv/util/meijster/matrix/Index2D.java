/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.matrix;

import java.io.Serializable;

/**
 * An immutable class encapsulating a two dimensional index.
 * 
 * @author Kevin McGuinness
 */
public final class Index2D implements Serializable {
	
	/**
	 * Serialization UID.
	 */
	private static final long serialVersionUID = 2722263604670361463L;
	
	/**
	 * The row index.
	 */
	public final int i; 
	
	/**
	 * The column index.
	 */
	public final int j;
	
	/**
	 * Create an two dimensional index for the given co-ordinates.
	 * 
	 * @param i
	 *        The row index.
	 * @param j
	 *        The column index.
	 */
	public Index2D(int i, int j) {
		this.i = i;
		this.j = j;
	}
	
	/**
	 * Returns true if the passed object is an Index2D and both objects are
	 * have equal values.
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof Index2D) {
			Index2D index = (Index2D) o;
			return i == index.i && j == index.j;
		}
		return false;
	}
	
	/**
	 * Returns the hash code.
	 */
	@Override
	public int hashCode() {
		return (i ^ j);
	}
	
	/**
	 * Returns a string of the form i,j
	 */
	public String toString() {
		return String.format("%d,%d", i, j);
	}
}
