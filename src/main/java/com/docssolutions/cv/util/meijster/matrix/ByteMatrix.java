/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.matrix;

import com.docssolutions.cv.util.meijster.array.Arrays;

/**
 * Two dimensional matrix of byte values.
 *
 * @author Kevin McGuinness
 */
public class ByteMatrix extends Matrix {
    
    /**
     * Serialization UID.
     */
	private static final long serialVersionUID = 6250820249861612603L;
	
	/**
	 * Shared empty matrix 
	 */
	private static final ByteMatrix EMPTY_MATRIX = new ByteMatrix(0,0);
	
	/**
	 * The byte values of the matrix in row-major order.
	 */
	public final byte[] values;

    /**
     * Construct an uninitialized matrix of the given size.
     *
     * @param rows
     *        The number of rows
     * @param cols
     *        The number of columns
     */
	public ByteMatrix(int rows, int cols) {
		super(Type.Byte, rows, cols);
		values = new byte[size];
	}
	
	/**
	 * Construct a matrix of the given size with the given values.
	 * 
	 * @param rows
     *        The number of rows
     * @param cols
     *        The number of columns
	 * @param values
	 *        The values to the matrix will have (not copied).
	 */
	public ByteMatrix(int rows, int cols, byte ... values) {		
		super(Type.Byte, rows, cols, values.length);
		this.values = values;
	}
	
	/**
	 * Construct a matrix of the given size by copying the values in the 
	 * given two dimensional array.
	 * 
	 * The passed two dimensional array must consist of arrays that have
	 * the same dimension.
	 * 
	 * @param matrix
	 *        A two dimensional array of byte values.
	 * @throws IllegalArgumentException
	 *        If the matrix does not contain arrays of the same dimension.       
	 */
	public ByteMatrix(byte[][] matrix) {
		this(rowsIn(matrix), colsIn(matrix));
		flatten(matrix, values);
	}
	
	/**
	 * Returns the byte value at the given row and column index.
	 *
	 * @param row
	 *        The row index.
	 * @param col
	 *        The column index.
	 * @return 
	 *        The byte value at (row, col).
	 */
	public final byte byteAt(int row, int col) {
		return values[offsetOf(row, col)];
	}
	
    /**
	 * Returns the byte value at the index.
	 *
	 * @param index
	 *        The index
	 * @return 
	 *        The byte value
	 */
	public final byte byteAt(Index2D index) {
		return values[offsetOf(index)];
	}

    /**
	 * Set the byte value at the given row and column index.
	 *
	 * @param row
	 *        The row index.
	 * @param col
	 *        The column index.
	 * @param value 
	 *        The byte value to set.
	 */
	public final void setByteAt(int row, int col, byte value) {
		values[offsetOf(row, col)] = value;
	}
	
	/**
	 * Set the byte value at the given index.
	 *
	 * @param index
	 *        The index.
	 * @param value 
	 *        The byte value to set.
	 */
	public final void setByteAt(Index2D index, byte value) {
		values[offsetOf(index)] = value;
	}
	
	/**
	 * Returns the byte value at the given offset in the matrix.
	 *
	 * @param offset
	 *        The absolute offset in the matrix.
	 * @return 
	 *        The byte value at the given offset.
	 */
	public final byte byteAt(int offset) {
		return values[offset];
	}

    /**
	 * Set the byte value at the given offset in the matrix.
	 *
	 * @param offset
	 *        The matrix offset
	 * @param value 
	 *        The byte value to set.
	 */
	public final void setByteAt(int offset, byte value) {
		values[offset] = value;
	}
	
	/**
	 * Returns the smallest byte value in the matrix.
	 *
	 * @return
	 *        The smallest byte value, or <code>null</code> if the 
	 *        matrix is empty.
	 */
	public final Byte minValue() {
	    return Arrays.min(values);
	}
	
	/**
	 * Returns the largest byte value in the matrix.
	 *
	 * @return
	 *        The largest byte value, or <code>null</code> if the 
	 *        matrix is empty.
	 */
	public final Byte maxValue() {
		return Arrays.max(values);
	}
	
	/**
	 * Returns a transposed version of the matrix.
	 */
	public final ByteMatrix transpose() {
		ByteMatrix m = new ByteMatrix(cols, rows);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				m.values[offsetOf(j,i)] = values[offsetOf(i, j)];
			}
		}
		return m;
	}

    /**
     * Returns a deep copy of the matrix.
     */
	@Override
	public ByteMatrix clone() {
		return new ByteMatrix(rows, cols, values.clone());
	}

    /**
     * Returns true if both matrices are equal in value.
     */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ByteMatrix) {
			ByteMatrix m = (ByteMatrix) obj;
			if (sizeEquals(m)) {
				return java.util.Arrays.equals(values, m.values);
			}
			return false;
		}
		return super.equals(obj);		
	}

    /**
     * Fills the matrix with the %{primative} value of the given number.
     *
     * @param number
     *        The number to fill the matrix with (will be truncated if 
     *        necessary).
     * @return 
     *        This matrix.
     */
	@Override
	public final ByteMatrix fill(Number number) {
		java.util.Arrays.fill(values, number.byteValue());
		return this;
	}

    /**
     * Returns the Byte value at the given row and column index.
     *
     * @param row
 	 *        The row index.
 	 * @param col
 	 *        The column index.
 	 * @return 
 	 *        The Byte value at (row, col).
     */
	@Override
	public final Byte valueAt(int row, int col) {
		return values[offsetOf(row, col)];
	}
	
	/**
   	 * Set the Number value at the given row and column index.
   	 *
   	 * @param row
   	 *        The row index.
   	 * @param col
   	 *        The column index.
   	 * @param value 
   	 *        The Number value to set (will be truncated if necessary).
   	 */
	@Override
	public final void setValueAt(int row, int col, Number value) {
		values[offsetOf(row, col)] = value.byteValue();
	}
	
	/**
     * Returns the Byte value at the given matrix offset.
     *
     * @param offset
     *        The absolute offset in the matrix.
 	 * @return 
 	 *        The Byte value at the given offset.
     */
	@Override
	public final Byte valueAt(int offset) {
		return values[offset];
	}
	
	/**
   	 * Set the Number value at the given matrix offset.
   	 *
   	 * @param offset
   	 *        The absolute offset in the matrix.
   	 * @param value 
   	 *        The Number value to set (will be truncated if necessary).
   	 */
	@Override
	public final void setValueAt(int offset, Number value) {
		values[offset] = value.byteValue();
	}
	
	/**
	 * Returns the matrix values (not copied).
	 */
	@Override
	public final byte[] values() {
		return values;
	}
	
	/**
	 * Returns a shared empty matrix instance.
	 */
	public static ByteMatrix empty() {
		return EMPTY_MATRIX;
	}
	
	/**
	 * Construct and return an identity matrix with the given number of rows.
	 *
	 * @param rows
	 *        The number of rows/columns for the matrix to have.
	 * @return
	 *        A new identity matrix.
	 */
	public static ByteMatrix eye(int rows) {
		ByteMatrix m = new ByteMatrix(rows, rows);
		m.fill(0);
		for (int i = 0; i < rows; i++) {
			m.setByteAt(i, i, (byte) 1);
		}
		return m;
	}
	
	/**
	 * Construct and return an zero matrix with the given number of rows
	 * and columns.
	 *
	 * @param rows
	 *        The number of rows for the matrix to have.
	 * @param cols
 	 *        The number of cols for the matrix to have.
	 * @return
	 *        A new zero matrix.
	 */
	public static ByteMatrix zeros(int rows, int cols) {
		ByteMatrix m = new ByteMatrix(rows, rows);
		m.fill(0);
		return m;
	}
	
	/**
	 * Construct and return a matrix of ones with the given number of rows
	 * and columns.
	 *
	 * @param rows
	 *        The number of rows for the matrix to have.
	 * @param cols
 	 *        The number of cols for the matrix to have.
	 * @return
	 *        A new matrix of ones.
	 */
	public static ByteMatrix ones(int rows, int cols) {
		ByteMatrix m = new ByteMatrix(rows, rows);
		m.fill(1);
		return m;
	}
	
	/**
     * Copy the given two dimensional array of byte values into the given 
	 * array. If the passed array does not contain at enough room for the
	 * passed matrix, a new array is allocated and returned.
	 * 
	 * The passed two dimensional array must consist of arrays that have
	 * the same dimension.
	 *
	 * The copy is performed such that the flattened array is in row major
	 * order. 
	 * 
	 * @param matrix
	 *        The 2D array of byte values to flatten
	 * @param array
	 *        The array to copy to (can be <code>null</code>).
	 * @return
	 *        A flattened version of the matrix.
	 * @throws IllegalArgumentException
	 *        If the matrix does not contain arrays of the same dimension.
	 */
	public static final byte[] flatten(byte[][] matrix, byte[] array) {
		int rows = matrix.length;
		int cols = matrix.length > 0 ? matrix[0].length : 0;
		int size = rows * cols;

		if (array == null || array.length < size) { 
			array = new byte[size];
		}

		for (int i = 0; i < rows; i++) {

			if (cols != matrix[i].length) {
				throw new IllegalArgumentException();
			}

			System.arraycopy(matrix[i], 0, array, cols * i, cols);
		}

		return array;
	}
	
    /**
     * Returns the number of rows in the given two dimensional array.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The number of rows.
	 */
	public static final int rowsIn(byte[][] matrix) {
		return matrix.length;
	}

	/**
	 * Returns the number of columns in the given two dimensional array.
	 * 
	 * This method assumes that the arrays in the given matrix all have
	 * equal lengths.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The number of columns.
	 */
	public static final int colsIn(byte[][] matrix) {
		return matrix.length > 0 ? matrix[0].length : 0;
	}

	/**
	 * Returns the total number of elements in the given two dimensional 
	 * array.
	 * 
	 * This method assumes that the arrays in the given matrix all have
	 * equal lengths.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The total number of elements.
	 */
	public static final int sizeOf(byte[][] matrix) {
		return rowsIn(matrix) * colsIn(matrix);
	}
}
