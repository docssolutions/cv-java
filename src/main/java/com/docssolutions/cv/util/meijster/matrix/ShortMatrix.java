/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.matrix;

import com.docssolutions.cv.util.meijster.array.Arrays;

/**
 * Two dimensional matrix of short values.
 *
 * @author Kevin McGuinness
 */
public class ShortMatrix extends Matrix {
    
    /**
     * Serialization UID.
     */
	private static final long serialVersionUID = 515302276679708530L;
	
	/**
	 * Shared empty matrix 
	 */
	private static final ShortMatrix EMPTY_MATRIX = new ShortMatrix(0,0);
	
	/**
	 * The short values of the matrix in row-major order.
	 */
	public final short[] values;

    /**
     * Construct an uninitialized matrix of the given size.
     *
     * @param rows
     *        The number of rows
     * @param cols
     *        The number of columns
     */
	public ShortMatrix(int rows, int cols) {
		super(Type.Short, rows, cols);
		values = new short[size];
	}
	
	/**
	 * Construct a matrix of the given size with the given values.
	 * 
	 * @param rows
     *        The number of rows
     * @param cols
     *        The number of columns
	 * @param values
	 *        The values to the matrix will have (not copied).
	 */
	public ShortMatrix(int rows, int cols, short ... values) {		
		super(Type.Short, rows, cols, values.length);
		this.values = values;
	}
	
	/**
	 * Construct a matrix of the given size by copying the values in the 
	 * given two dimensional array.
	 * 
	 * The passed two dimensional array must consist of arrays that have
	 * the same dimension.
	 * 
	 * @param matrix
	 *        A two dimensional array of short values.
	 * @throws IllegalArgumentException
	 *        If the matrix does not contain arrays of the same dimension.       
	 */
	public ShortMatrix(short[][] matrix) {
		this(rowsIn(matrix), colsIn(matrix));
		flatten(matrix, values);
	}
	
	/**
	 * Returns the short value at the given row and column index.
	 *
	 * @param row
	 *        The row index.
	 * @param col
	 *        The column index.
	 * @return 
	 *        The short value at (row, col).
	 */
	public final short shortAt(int row, int col) {
		return values[offsetOf(row, col)];
	}
	
    /**
	 * Returns the short value at the index.
	 *
	 * @param index
	 *        The index
	 * @return 
	 *        The short value
	 */
	public final short shortAt(Index2D index) {
		return values[offsetOf(index)];
	}

    /**
	 * Set the short value at the given row and column index.
	 *
	 * @param row
	 *        The row index.
	 * @param col
	 *        The column index.
	 * @param value 
	 *        The short value to set.
	 */
	public final void setShortAt(int row, int col, short value) {
		values[offsetOf(row, col)] = value;
	}
	
	/**
	 * Set the short value at the given index.
	 *
	 * @param index
	 *        The index.
	 * @param value 
	 *        The short value to set.
	 */
	public final void setShortAt(Index2D index, short value) {
		values[offsetOf(index)] = value;
	}
	
	/**
	 * Returns the short value at the given offset in the matrix.
	 *
	 * @param offset
	 *        The absolute offset in the matrix.
	 * @return 
	 *        The short value at the given offset.
	 */
	public final short shortAt(int offset) {
		return values[offset];
	}

    /**
	 * Set the short value at the given offset in the matrix.
	 *
	 * @param offset
	 *        The matrix offset
	 * @param value 
	 *        The short value to set.
	 */
	public final void setShortAt(int offset, short value) {
		values[offset] = value;
	}
	
	/**
	 * Returns the smallest short value in the matrix.
	 *
	 * @return
	 *        The smallest short value, or <code>null</code> if the 
	 *        matrix is empty.
	 */
	public final Short minValue() {
	    return Arrays.min(values);
	}
	
	/**
	 * Returns the largest short value in the matrix.
	 *
	 * @return
	 *        The largest short value, or <code>null</code> if the 
	 *        matrix is empty.
	 */
	public final Short maxValue() {
		return Arrays.max(values);
	}
	
	/**
	 * Returns a transposed version of the matrix.
	 */
	public final ShortMatrix transpose() {
		ShortMatrix m = new ShortMatrix(cols, rows);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				m.values[offsetOf(j,i)] = values[offsetOf(i, j)];
			}
		}
		return m;
	}

    /**
     * Returns a deep copy of the matrix.
     */
	@Override
	public ShortMatrix clone() {
		return new ShortMatrix(rows, cols, values.clone());
	}

    /**
     * Returns true if both matrices are equal in value.
     */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ShortMatrix) {
			ShortMatrix m = (ShortMatrix) obj;
			if (sizeEquals(m)) {
				return java.util.Arrays.equals(values, m.values);
			}
			return false;
		}
		return super.equals(obj);		
	}

    /**
     * Fills the matrix with the %{primative} value of the given number.
     *
     * @param number
     *        The number to fill the matrix with (will be truncated if 
     *        necessary).
     * @return 
     *        This matrix.
     */
	@Override
	public final ShortMatrix fill(Number number) {
		java.util.Arrays.fill(values, number.shortValue());
		return this;
	}

    /**
     * Returns the Short value at the given row and column index.
     *
     * @param row
 	 *        The row index.
 	 * @param col
 	 *        The column index.
 	 * @return 
 	 *        The Short value at (row, col).
     */
	@Override
	public final Short valueAt(int row, int col) {
		return values[offsetOf(row, col)];
	}
	
	/**
   	 * Set the Number value at the given row and column index.
   	 *
   	 * @param row
   	 *        The row index.
   	 * @param col
   	 *        The column index.
   	 * @param value 
   	 *        The Number value to set (will be truncated if necessary).
   	 */
	@Override
	public final void setValueAt(int row, int col, Number value) {
		values[offsetOf(row, col)] = value.shortValue();
	}
	
	/**
     * Returns the Short value at the given matrix offset.
     *
     * @param offset
     *        The absolute offset in the matrix.
 	 * @return 
 	 *        The Short value at the given offset.
     */
	@Override
	public final Short valueAt(int offset) {
		return values[offset];
	}
	
	/**
   	 * Set the Number value at the given matrix offset.
   	 *
   	 * @param offset
   	 *        The absolute offset in the matrix.
   	 * @param value 
   	 *        The Number value to set (will be truncated if necessary).
   	 */
	@Override
	public final void setValueAt(int offset, Number value) {
		values[offset] = value.shortValue();
	}
	
	/**
	 * Returns the matrix values (not copied).
	 */
	@Override
	public final short[] values() {
		return values;
	}
	
	/**
	 * Returns a shared empty matrix instance.
	 */
	public static ShortMatrix empty() {
		return EMPTY_MATRIX;
	}
	
	/**
	 * Construct and return an identity matrix with the given number of rows.
	 *
	 * @param rows
	 *        The number of rows/columns for the matrix to have.
	 * @return
	 *        A new identity matrix.
	 */
	public static ShortMatrix eye(int rows) {
		ShortMatrix m = new ShortMatrix(rows, rows);
		m.fill(0);
		for (int i = 0; i < rows; i++) {
			m.setShortAt(i, i, (short) 1);
		}
		return m;
	}
	
	/**
	 * Construct and return an zero matrix with the given number of rows
	 * and columns.
	 *
	 * @param rows
	 *        The number of rows for the matrix to have.
	 * @param cols
 	 *        The number of cols for the matrix to have.
	 * @return
	 *        A new zero matrix.
	 */
	public static ShortMatrix zeros(int rows, int cols) {
		ShortMatrix m = new ShortMatrix(rows, rows);
		m.fill(0);
		return m;
	}
	
	/**
	 * Construct and return a matrix of ones with the given number of rows
	 * and columns.
	 *
	 * @param rows
	 *        The number of rows for the matrix to have.
	 * @param cols
 	 *        The number of cols for the matrix to have.
	 * @return
	 *        A new matrix of ones.
	 */
	public static ShortMatrix ones(int rows, int cols) {
		ShortMatrix m = new ShortMatrix(rows, rows);
		m.fill(1);
		return m;
	}
	
	/**
     * Copy the given two dimensional array of short values into the given 
	 * array. If the passed array does not contain at enough room for the
	 * passed matrix, a new array is allocated and returned.
	 * 
	 * The passed two dimensional array must consist of arrays that have
	 * the same dimension.
	 *
	 * The copy is performed such that the flattened array is in row major
	 * order. 
	 * 
	 * @param matrix
	 *        The 2D array of short values to flatten
	 * @param array
	 *        The array to copy to (can be <code>null</code>).
	 * @return
	 *        A flattened version of the matrix.
	 * @throws IllegalArgumentException
	 *        If the matrix does not contain arrays of the same dimension.
	 */
	public static final short[] flatten(short[][] matrix, short[] array) {
		int rows = matrix.length;
		int cols = matrix.length > 0 ? matrix[0].length : 0;
		int size = rows * cols;

		if (array == null || array.length < size) { 
			array = new short[size];
		}

		for (int i = 0; i < rows; i++) {

			if (cols != matrix[i].length) {
				throw new IllegalArgumentException();
			}

			System.arraycopy(matrix[i], 0, array, cols * i, cols);
		}

		return array;
	}
	
    /**
     * Returns the number of rows in the given two dimensional array.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The number of rows.
	 */
	public static final int rowsIn(short[][] matrix) {
		return matrix.length;
	}

	/**
	 * Returns the number of columns in the given two dimensional array.
	 * 
	 * This method assumes that the arrays in the given matrix all have
	 * equal lengths.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The number of columns.
	 */
	public static final int colsIn(short[][] matrix) {
		return matrix.length > 0 ? matrix[0].length : 0;
	}

	/**
	 * Returns the total number of elements in the given two dimensional 
	 * array.
	 * 
	 * This method assumes that the arrays in the given matrix all have
	 * equal lengths.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The total number of elements.
	 */
	public static final int sizeOf(short[][] matrix) {
		return rowsIn(matrix) * colsIn(matrix);
	}
}
