/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.array;

import java.lang.reflect.Array;

/**
 * Collection of static methods on arrays.
 * 
 * @author Kevin McGuinness
 */
public class Arrays  {
	
	/**
	 * Clamp the values in the given byte matrix to the given range.
	 * 
	 * The values array is modified (if necessary).
	 * 
	 * The passed array must be one of the following types: 
	 * <ul>
	 *   <li><code>byte[]</code></li>
	 *   <li><code>short[]</code></li>
	 *   <li><code>int[]</code></li>
	 *   <li><code>long[]</code></li>
	 *   <li><code>float[]</code></li>
	 *   <li><code>double[]</code></li>
	 * </ul>
	 * 
	 * @param array
	 *        An array.
	 * @param min
	 *            The minimum value.
	 * @param max
	 *            The maximum value.
	 * @return 
	 *        The modified input matrix.
	 * @throws UnsupportedOperationException
	 *        If the passed array is not one of the supported types.
	 */
	public static Object clamp(Object array, Number min, Number max) 
		throws UnsupportedOperationException 
	{	
		if (array instanceof byte[]) {
			return clamp((byte[]) array, min.byteValue(), max.byteValue());
		} else if (array instanceof short[]) {
			return clamp((short[]) array, min.shortValue(), max.shortValue());
		} else if (array instanceof int[]) {
			return clamp((int[]) array, min.intValue(), max.intValue());
		} else if (array instanceof long[]) {
			return clamp((long[]) array, min.longValue(), max.longValue());
		} else if (array instanceof float[]) {
			return clamp((float[]) array, min.longValue(), max.longValue());
		} else if (array instanceof double[]) {
			return clamp((double[]) array, min.longValue(), max.longValue());
		} else {
			throw new UnsupportedOperationException();
		}
	}	

	/**
	 * Clamp the values in the given byte matrix to the given range.
	 * 
	 * The values array is modified (if necessary).
	 *
	 * @param values
	 *            A matrix of byte values.
	 * @param min
	 *            The minimum value.
	 * @param max
	 *            The maximum value.
	 * @return The modified input matrix.
	 */
	public static byte[] clamp(byte[] values, byte min, byte max) {
		if (min > max) {
			throw new IllegalArgumentException("min > max");
		}
		
		for (int i = 0; i < values.length; i++) {
			if (values[i] < min) {
				values[i] = min;
			}
			
			if (values[i] > max) {
				values[i] = max;
			}
		}
		
		return values;
	}

	/**
	 * Clamp the values in the given short matrix to the given range.
	 * 
	 * The values array is modified (if necessary).
	 *
	 * @param values
	 *            A matrix of short values.
	 * @param min
	 *            The minimum value.
	 * @param max
	 *            The maximum value.
	 * @return The modified input matrix.
	 */
	public static short[] clamp(short[] values, short min, short max) {
		if (min > max) {
			throw new IllegalArgumentException("min > max");
		}
		
		for (int i = 0; i < values.length; i++) {
			if (values[i] < min) {
				values[i] = min;
			}
			
			if (values[i] > max) {
				values[i] = max;
			}
		}
		
		return values;
	}

	/**
	 * Clamp the values in the given int matrix to the given range.
	 * 
	 * The values array is modified (if necessary).
	 *
	 * @param values
	 *            A matrix of int values.
	 * @param min
	 *            The minimum value.
	 * @param max
	 *            The maximum value.
	 * @return The modified input matrix.
	 */
	public static int[] clamp(int[] values, int min, int max) {
		if (min > max) {
			throw new IllegalArgumentException("min > max");
		}
		
		for (int i = 0; i < values.length; i++) {
			if (values[i] < min) {
				values[i] = min;
			}
			
			if (values[i] > max) {
				values[i] = max;
			}
		}
		
		return values;
	}

	/**
	 * Clamp the values in the given long matrix to the given range.
	 * 
	 * The values array is modified (if necessary).
	 *
	 * @param values
	 *            A matrix of long values.
	 * @param min
	 *            The minimum value.
	 * @param max
	 *            The maximum value.
	 * @return The modified input matrix.
	 */
	public static long[] clamp(long[] values, long min, long max) {
		if (min > max) {
			throw new IllegalArgumentException("min > max");
		}
		
		for (int i = 0; i < values.length; i++) {
			if (values[i] < min) {
				values[i] = min;
			}
			
			if (values[i] > max) {
				values[i] = max;
			}
		}
		
		return values;
	}

	/**
	 * Clamp the values in the given float matrix to the given range.
	 * 
	 * The values array is modified (if necessary).
	 *
	 * @param values
	 *            A matrix of float values.
	 * @param min
	 *            The minimum value.
	 * @param max
	 *            The maximum value.
	 * @return The modified input matrix.
	 */
	public static float[] clamp(float[] values, float min, float max) {
		if (min > max) {
			throw new IllegalArgumentException("min > max");
		}
		
		for (int i = 0; i < values.length; i++) {
			if (values[i] < min) {
				values[i] = min;
			}
			
			if (values[i] > max) {
				values[i] = max;
			}
		}
		
		return values;
	}

	/**
	 * Clamp the values in the given double matrix to the given range.
	 * 
	 * The values array is modified (if necessary).
	 *
	 * @param values
	 *            A matrix of double values.
	 * @param min
	 *            The minimum value.
	 * @param max
	 *            The maximum value.
	 * @return The modified input matrix.
	 */
	public static double[] clamp(double[] values, double min, double max) {
		if (min > max) {
			throw new IllegalArgumentException("min > max");
		}
		
		for (int i = 0; i < values.length; i++) {
			if (values[i] < min) {
				values[i] = min;
			}
			
			if (values[i] > max) {
				values[i] = max;
			}
		}
		
		return values;
	}
	
	/**
	 * Filter the values in the given array.
	 * 
	 * @param array
	 *        An array.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] filter(T[] array, Filter filter) {
		T[] buffer = (T[]) Array.newInstance(
				array.getClass().getComponentType(), array.length);
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}
	

	/**
	 * Filter the values in the given byte array.
	 *
	 * This method is potentially more efficient than the generic Filter 
	 * version, since it must box each value in the array.
	 * 
	 * @param array
	 *        An array of bytes.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static byte[] filter(byte[] array, Filter.Byte filter) {
		byte[] buffer = new byte[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given byte array.
	 * 
	 * @param array
	 *        An array of bytes.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static byte[] filter(byte[] array, Filter filter) {
		byte[] buffer = new byte[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given short array.
	 *
	 * This method is potentially more efficient than the generic Filter 
	 * version, since it must box each value in the array.
	 * 
	 * @param array
	 *        An array of shorts.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static short[] filter(short[] array, Filter.Short filter) {
		short[] buffer = new short[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given short array.
	 * 
	 * @param array
	 *        An array of shorts.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static short[] filter(short[] array, Filter filter) {
		short[] buffer = new short[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given int array.
	 *
	 * This method is potentially more efficient than the generic Filter 
	 * version, since it must box each value in the array.
	 * 
	 * @param array
	 *        An array of ints.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static int[] filter(int[] array, Filter.Integer filter) {
		int[] buffer = new int[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given int array.
	 * 
	 * @param array
	 *        An array of ints.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static int[] filter(int[] array, Filter filter) {
		int[] buffer = new int[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given long array.
	 *
	 * This method is potentially more efficient than the generic Filter 
	 * version, since it must box each value in the array.
	 * 
	 * @param array
	 *        An array of longs.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static long[] filter(long[] array, Filter.Long filter) {
		long[] buffer = new long[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given long array.
	 * 
	 * @param array
	 *        An array of longs.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static long[] filter(long[] array, Filter filter) {
		long[] buffer = new long[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given float array.
	 *
	 * This method is potentially more efficient than the generic Filter 
	 * version, since it must box each value in the array.
	 * 
	 * @param array
	 *        An array of floats.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static float[] filter(float[] array, Filter.Float filter) {
		float[] buffer = new float[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given float array.
	 * 
	 * @param array
	 *        An array of floats.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static float[] filter(float[] array, Filter filter) {
		float[] buffer = new float[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given double array.
	 *
	 * This method is potentially more efficient than the generic Filter 
	 * version, since it must box each value in the array.
	 * 
	 * @param array
	 *        An array of doubles.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static double[] filter(double[] array, Filter.Double filter) {
		double[] buffer = new double[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}

	/**
	 * Filter the values in the given double array.
	 * 
	 * @param array
	 *        An array of doubles.
	 * @param filter
	 *        A filter.
	 * @return
	 *        The filtered array.
	 */
	public static double[] filter(double[] array, Filter filter) {
		double[] buffer = new double[array.length];
		
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (filter.retain(array[i])) {
				buffer[count++] = array[i];
			}
		}
		
		return subarray(buffer, 0, count);
	}
	
	/**
	 * Returns a sub-array of the given array.
	 * 
	 * @param array
	 *        The array of bytes.
	 * @param startPos
	 *        The start position.
	 * @param count
	 *        The number of elements.
	 * @return
	 *        The sub-array.
	 * @throws IndexOutOfBoundsException
	 *        If the operation would result in an index out of bounds
	 *        error.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] subarray(T[] array, int startPos, int count) {
		checkSubarray(array.length, startPos, count);
		T[] result = (T[]) Array.newInstance(
				array.getClass().getComponentType(), count);
		System.arraycopy(array, 0, result, 0, count);
		return result;
	}
	
	/**
	 * Returns a sub-array of the given array.
	 * 
	 * @param array
	 *        The array of bytes.
	 * @param startPos
	 *        The start position.
	 * @param count
	 *        The number of elements.
	 * @return
	 *        The sub-array.
	 * @throws IndexOutOfBoundsException
	 *        If the operation would result in an index out of bounds
	 *        error.
	 */
	public static byte[] subarray(byte[] array, int startPos, int count) {
		checkSubarray(array.length, startPos, count);
		byte[] result = new byte[count];
		System.arraycopy(array, 0, result, 0, count);
		return result;
	}

	/**
	 * Returns a sub-array of the given array.
	 * 
	 * @param array
	 *        The array of shorts.
	 * @param startPos
	 *        The start position.
	 * @param count
	 *        The number of elements.
	 * @return
	 *        The sub-array.
	 * @throws IndexOutOfBoundsException
	 *        If the operation would result in an index out of bounds
	 *        error.
	 */
	public static short[] subarray(short[] array, int startPos, int count) {
		checkSubarray(array.length, startPos, count);
		short[] result = new short[count];
		System.arraycopy(array, 0, result, 0, count);
		return result;
	}

	/**
	 * Returns a sub-array of the given array.
	 * 
	 * @param array
	 *        The array of ints.
	 * @param startPos
	 *        The start position.
	 * @param count
	 *        The number of elements.
	 * @return
	 *        The sub-array.
	 * @throws IndexOutOfBoundsException
	 *        If the operation would result in an index out of bounds
	 *        error.
	 */
	public static int[] subarray(int[] array, int startPos, int count) {
		checkSubarray(array.length, startPos, count);
		int[] result = new int[count];
		System.arraycopy(array, 0, result, 0, count);
		return result;
	}

	/**
	 * Returns a sub-array of the given array.
	 * 
	 * @param array
	 *        The array of longs.
	 * @param startPos
	 *        The start position.
	 * @param count
	 *        The number of elements.
	 * @return
	 *        The sub-array.
	 * @throws IndexOutOfBoundsException
	 *        If the operation would result in an index out of bounds
	 *        error.
	 */
	public static long[] subarray(long[] array, int startPos, int count) {
		checkSubarray(array.length, startPos, count);
		long[] result = new long[count];
		System.arraycopy(array, 0, result, 0, count);
		return result;
	}

	/**
	 * Returns a sub-array of the given array.
	 * 
	 * @param array
	 *        The array of floats.
	 * @param startPos
	 *        The start position.
	 * @param count
	 *        The number of elements.
	 * @return
	 *        The sub-array.
	 * @throws IndexOutOfBoundsException
	 *        If the operation would result in an index out of bounds
	 *        error.
	 */
	public static float[] subarray(float[] array, int startPos, int count) {
		checkSubarray(array.length, startPos, count);
		float[] result = new float[count];
		System.arraycopy(array, 0, result, 0, count);
		return result;
	}

	/**
	 * Returns a sub-array of the given array.
	 * 
	 * @param array
	 *        The array of doubles.
	 * @param startPos
	 *        The start position.
	 * @param count
	 *        The number of elements.
	 * @return
	 *        The sub-array.
	 * @throws IndexOutOfBoundsException
	 *        If the operation would result in an index out of bounds
	 *        error.
	 */
	public static double[] subarray(double[] array, int startPos, int count) {
		checkSubarray(array.length, startPos, count);
		double[] result = new double[count];
		System.arraycopy(array, 0, result, 0, count);
		return result;
	}
	
	/**
	 * Check if the subarray operation will result in an 
	 * {@link IndexOutOfBoundsException}
	 */
	private static void checkSubarray(int length, int startPos, int count) {
		if (startPos < 0) {
			throw new IndexOutOfBoundsException("startPos < 0");
		}
		
		if (count < 0) {
			throw new IndexOutOfBoundsException("count < 0");
		}
		
		if (startPos + count > length) {
			throw new IndexOutOfBoundsException("startPos + count > array.length");
		}
	}
	
	/**
	 * Perform a reduction on the given array of bytes.
	 * 
	 * @param array 
	 *        An array.
	 * @param initialValue 
	 *        The initial value for the reduction.
	 * @param operation
	 *        The reduction operation.
	 * @return
	 *        The result of the reduction.
	 */
	public static <T, U> U reduce(T[] array, U initialValue, 
			Reduction<T,U> operation) 
	{
		U accumulator = initialValue;
		for (int i = 0; i < array.length; i++) {
			accumulator = operation.reduce(accumulator, array[i], i);
		}
		return accumulator;
	}

	/**
	 * Perform a reduction on the given array of bytes.
	 * 
	 * @param array 
	 *        An array of bytes.
	 * @param initialValue 
	 *        The initial value for the reduction.
	 * @param operation
	 *        The reduction operation.
	 * @return
	 *        The result of the reduction.
	 */
	public static long reduce(byte[] array, long initialValue, 
			Reduction.Long operation) 
	{
		long accumulator = initialValue;
		for (int i = 0; i < array.length; i++) {
			accumulator = operation.reduce(accumulator, array[i], i);
		}
		return accumulator;
	}

	/**
	 * Perform a reduction on the given array of shorts.
	 * 
	 * @param array 
	 *        An array of shorts.
	 * @param initialValue 
	 *        The initial value for the reduction.
	 * @param operation
	 *        The reduction operation.
	 * @return
	 *        The result of the reduction.
	 */
	public static long reduce(short[] array, long initialValue, 
			Reduction.Long operation) 
	{
		long accumulator = initialValue;
		for (int i = 0; i < array.length; i++) {
			accumulator = operation.reduce(accumulator, array[i], i);
		}
		return accumulator;
	}

	/**
	 * Perform a reduction on the given array of ints.
	 * 
	 * @param array 
	 *        An array of ints.
	 * @param initialValue 
	 *        The initial value for the reduction.
	 * @param operation
	 *        The reduction operation.
	 * @return
	 *        The result of the reduction.
	 */
	public static long reduce(int[] array, long initialValue, 
			Reduction.Long operation) 
	{
		long accumulator = initialValue;
		for (int i = 0; i < array.length; i++) {
			accumulator = operation.reduce(accumulator, array[i], i);
		}
		return accumulator;
	}

	/**
	 * Perform a reduction on the given array of longs.
	 * 
	 * @param array 
	 *        An array of longs.
	 * @param initialValue 
	 *        The initial value for the reduction.
	 * @param operation
	 *        The reduction operation.
	 * @return
	 *        The result of the reduction.
	 */
	public static long reduce(long[] array, long initialValue, 
			Reduction.Long operation) 
	{
		long accumulator = initialValue;
		for (int i = 0; i < array.length; i++) {
			accumulator = operation.reduce(accumulator, array[i], i);
		}
		return accumulator;
	}

	/**
	 * Perform a reduction on the given array of floats.
	 * 
	 * @param array 
	 *        An array of floats.
	 * @param initialValue 
	 *        The initial value for the reduction.
	 * @param operation
	 *        The reduction operation.
	 * @return
	 *        The result of the reduction.
	 */
	public static double reduce(float[] array, double initialValue, 
			Reduction.Double operation) 
	{
		double accumulator = initialValue;
		for (int i = 0; i < array.length; i++) {
			accumulator = operation.reduce(accumulator, array[i], i);
		}
		return accumulator;
	}

	/**
	 * Perform a reduction on the given array of doubles.
	 * 
	 * @param array 
	 *        An array of doubles.
	 * @param initialValue 
	 *        The initial value for the reduction.
	 * @param operation
	 *        The reduction operation.
	 * @return
	 *        The result of the reduction.
	 */
	public static double reduce(double[] array, double initialValue, 
			Reduction.Double operation) 
	{
		double accumulator = initialValue;
		for (int i = 0; i < array.length; i++) {
			accumulator = operation.reduce(accumulator, array[i], i);
		}
		return accumulator;
	}
	
	/**
	 * Finds the sum of all elements in the given array.
	 * 
	 * The passed array must be one of the following types: 
	 * <ul>
	 *   <li><code>byte[]</code></li>
	 *   <li><code>short[]</code></li>
	 *   <li><code>int[]</code></li>
	 *   <li><code>long[]</code></li>
	 *   <li><code>float[]</code></li>
	 *   <li><code>double[]</code></li>
	 * </ul>
	 * 
	 * @param array
	 *        An array.
	 * @return 
	 *        The sum of all elements in the given array as a Number.
	 * @throws UnsupportedOperationException
	 *        If the passed array is not one of the supported types.
	 */
	public static Number sum(Object array) 
		throws UnsupportedOperationException 
	{	
		if (array instanceof byte[]) {
			return sum((byte[]) array);
		} else if (array instanceof short[]) {
			return sum((short[]) array);
		} else if (array instanceof int[]) {
			return sum((int[]) array);
		} else if (array instanceof long[]) {
			return sum((long[]) array);
		} else if (array instanceof float[]) {
			return sum((float[]) array);
		} else if (array instanceof double[]) {
			return sum((double[]) array);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Computes the sum of all the elements in the given byte array.
	 * 
	 * @param array
	 *        An array of bytes.
	 * @return
	 *        The sum of all elements in the array.
	 */
	public static long sum(byte[] array) {
		long sumValue = 0;
		for (int i = 0; i < array.length; i++) {
			sumValue += array[i];
		}
		return sumValue;
	}

	/**
	 * Computes the sum of all the elements in the given short array.
	 * 
	 * @param array
	 *        An array of shorts.
	 * @return
	 *        The sum of all elements in the array.
	 */
	public static long sum(short[] array) {
		long sumValue = 0;
		for (int i = 0; i < array.length; i++) {
			sumValue += array[i];
		}
		return sumValue;
	}

	/**
	 * Computes the sum of all the elements in the given int array.
	 * 
	 * @param array
	 *        An array of ints.
	 * @return
	 *        The sum of all elements in the array.
	 */
	public static long sum(int[] array) {
		long sumValue = 0;
		for (int i = 0; i < array.length; i++) {
			sumValue += array[i];
		}
		return sumValue;
	}

	/**
	 * Computes the sum of all the elements in the given long array.
	 * 
	 * @param array
	 *        An array of longs.
	 * @return
	 *        The sum of all elements in the array.
	 */
	public static long sum(long[] array) {
		long sumValue = 0;
		for (int i = 0; i < array.length; i++) {
			sumValue += array[i];
		}
		return sumValue;
	}

	/**
	 * Computes the sum of all the elements in the given float array.
	 * 
	 * @param array
	 *        An array of floats.
	 * @return
	 *        The sum of all elements in the array.
	 */
	public static double sum(float[] array) {
		double sumValue = 0;
		for (int i = 0; i < array.length; i++) {
			sumValue += array[i];
		}
		return sumValue;
	}

	/**
	 * Computes the sum of all the elements in the given double array.
	 * 
	 * @param array
	 *        An array of doubles.
	 * @return
	 *        The sum of all elements in the array.
	 */
	public static double sum(double[] array) {
		double sumValue = 0;
		for (int i = 0; i < array.length; i++) {
			sumValue += array[i];
		}
		return sumValue;
	}
	
	/**
	 * Finds the minimum number in the given array.
	 * 
	 * The passed array must be one of the following types: 
	 * <ul>
	 *   <li><code>byte[]</code></li>
	 *   <li><code>short[]</code></li>
	 *   <li><code>int[]</code></li>
	 *   <li><code>long[]</code></li>
	 *   <li><code>float[]</code></li>
	 *   <li><code>double[]</code></li>
	 * </ul>
	 * 
	 * @param array
	 *        An array.
	 * @return The smallest number, or <code>null</code> if the length of the
	 *        array is zero.
	 * @throws UnsupportedOperationException
	 *        If the passed array is not one of the supported types.
	 */
	public static Number min(Object array) 
		throws UnsupportedOperationException 
	{	
		if (array instanceof byte[]) {
			return min((byte[]) array);
		} else if (array instanceof short[]) {
			return min((short[]) array);
		} else if (array instanceof int[]) {
			return min((int[]) array);
		} else if (array instanceof long[]) {
			return min((long[]) array);
		} else if (array instanceof float[]) {
			return min((float[]) array);
		} else if (array instanceof double[]) {
			return min((double[]) array);
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
	/**
	 * Finds the maximum number in the given array.
	 * 
	 * The passed array must be one of the following types: 
	 * <ul>
	 *   <li><code>byte[]</code></li>
	 *   <li><code>short[]</code></li>
	 *   <li><code>int[]</code></li>
	 *   <li><code>long[]</code></li>
	 *   <li><code>float[]</code></li>
	 *   <li><code>double[]</code></li>
	 * </ul>
	 * 
	 * @param array
	 *        An array.
	 * @return The largest number, or <code>null</code> if the length of the
	 *        array is zero.
	 * @throws UnsupportedOperationException
	 *        If the passed array is not one of the supported types.
	 */
	public static Number max(Object array) 
		throws UnsupportedOperationException 
	{	
		if (array instanceof byte[]) {
			return max((byte[]) array);
		} else if (array instanceof short[]) {
			return max((short[]) array);
		} else if (array instanceof int[]) {
			return max((int[]) array);
		} else if (array instanceof long[]) {
			return max((long[]) array);
		} else if (array instanceof float[]) {
			return max((float[]) array);
		} else if (array instanceof double[]) {
			return max((double[]) array);
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
	/**
	 * Returns the smallest element in the given byte array.
	 * 
	 * @param array
	 *        An array of bytes.
	 * @return The smallest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Byte min(byte[] array) {
		if (array.length > 0) {
			byte minimum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] < minimum) {
					minimum = array[i];
				}
			}
			return minimum;
		}
		return null;
	}

	/**
	 * Returns the largest element in the given byte array.
	 * 
	 * @param array
	 *        An array of bytes.
	 * @return The largest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Byte max(byte[] array) {
		if (array.length > 0) {
			byte maximum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] > maximum) {
					maximum = array[i];
				}
			}
			return maximum;
		}
		return null;
	}

	/**
	 * Returns the smallest element in the given short array.
	 * 
	 * @param array
	 *        An array of shorts.
	 * @return The smallest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Short min(short[] array) {
		if (array.length > 0) {
			short minimum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] < minimum) {
					minimum = array[i];
				}
			}
			return minimum;
		}
		return null;
	}

	/**
	 * Returns the largest element in the given short array.
	 * 
	 * @param array
	 *        An array of shorts.
	 * @return The largest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Short max(short[] array) {
		if (array.length > 0) {
			short maximum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] > maximum) {
					maximum = array[i];
				}
			}
			return maximum;
		}
		return null;
	}

	/**
	 * Returns the smallest element in the given int array.
	 * 
	 * @param array
	 *        An array of ints.
	 * @return The smallest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Integer min(int[] array) {
		if (array.length > 0) {
			int minimum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] < minimum) {
					minimum = array[i];
				}
			}
			return minimum;
		}
		return null;
	}

	/**
	 * Returns the largest element in the given int array.
	 * 
	 * @param array
	 *        An array of ints.
	 * @return The largest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Integer max(int[] array) {
		if (array.length > 0) {
			int maximum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] > maximum) {
					maximum = array[i];
				}
			}
			return maximum;
		}
		return null;
	}

	/**
	 * Returns the smallest element in the given long array.
	 * 
	 * @param array
	 *        An array of longs.
	 * @return The smallest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Long min(long[] array) {
		if (array.length > 0) {
			long minimum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] < minimum) {
					minimum = array[i];
				}
			}
			return minimum;
		}
		return null;
	}

	/**
	 * Returns the largest element in the given long array.
	 * 
	 * @param array
	 *        An array of longs.
	 * @return The largest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Long max(long[] array) {
		if (array.length > 0) {
			long maximum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] > maximum) {
					maximum = array[i];
				}
			}
			return maximum;
		}
		return null;
	}

	/**
	 * Returns the smallest element in the given float array.
	 * 
	 * @param array
	 *        An array of floats.
	 * @return The smallest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Float min(float[] array) {
		if (array.length > 0) {
			float minimum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] < minimum) {
					minimum = array[i];
				}
			}
			return minimum;
		}
		return null;
	}

	/**
	 * Returns the largest element in the given float array.
	 * 
	 * @param array
	 *        An array of floats.
	 * @return The largest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Float max(float[] array) {
		if (array.length > 0) {
			float maximum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] > maximum) {
					maximum = array[i];
				}
			}
			return maximum;
		}
		return null;
	}

	/**
	 * Returns the smallest element in the given double array.
	 * 
	 * @param array
	 *        An array of doubles.
	 * @return The smallest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Double min(double[] array) {
		if (array.length > 0) {
			double minimum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] < minimum) {
					minimum = array[i];
				}
			}
			return minimum;
		}
		return null;
	}

	/**
	 * Returns the largest element in the given double array.
	 * 
	 * @param array
	 *        An array of doubles.
	 * @return The largest element, or <code>null</code> if the length of the
	 *        array is zero.
	 */
	public static Double max(double[] array) {
		if (array.length > 0) {
			double maximum = array[0];
			for (int i = 1; i < array.length; i++) {
				if (array[i] > maximum) {
					maximum = array[i];
				}
			}
			return maximum;
		}
		return null;
	}
	
	/**
	 * Copies an array from the specified source array, beginning at the
     * specified position, to the specified position of the destination array.
     * 
     * This method uses {@link System#arraycopy(Object, int, Object, int, int)}
     * if the source and destination array are of the same type, otherwise
     * it sees if there is a compatible static copy method in this class
     * that can perform the conversion copy. If one cannot be found, an
     * {@link ArrayStoreException} is thrown.
	 *
	 * If the positions or lengths specified are out of range then an 
	 * {@link IndexOutOfBoundsException} is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws ArrayStoreException  
	 *        if an element in the source array could not be stored into the
	 *        destination array because of a type mismatch, and there is no
	 *        primitive copy conversion routine to handle the conversion.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(Object src, Object dst) {
		copy(src, 0, dst, 0, Array.getLength(dst));
	}
	
	/**
	 * Copies an array from the specified source array, beginning at the
     * specified position, to the specified position of the destination array.
     * 
     * @see #copy(Object, int, Object, int, int)
     * 
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws ArrayStoreException  
	 *        if an element in the source array could not be stored into the
	 *        destination array because of a type mismatch, and there is no
	 *        primitive copy conversion routine to handle the conversion.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(Object src, int srcPos, 
			Object dst, int dstPos, int length) 
	{
		if (dst.getClass().equals(src.getClass())) {

			// Use the fast system copy
			System.arraycopy(src, srcPos, dst, dstPos, length);

		} else {
			// Try for arrays of different primitive types
			
			if (src instanceof byte[] && dst instanceof short[]) {

				// Copy from an array of bytes to array of shorts
				copy((byte[]) src, srcPos, (short[]) dst, dstPos, length);

			} else if (src instanceof byte[] && dst instanceof int[]) {

				// Copy from an array of bytes to array of ints
				copy((byte[]) src, srcPos, (int[]) dst, dstPos, length);

			} else if (src instanceof byte[] && dst instanceof long[]) {

				// Copy from an array of bytes to array of longs
				copy((byte[]) src, srcPos, (long[]) dst, dstPos, length);

			} else if (src instanceof byte[] && dst instanceof float[]) {

				// Copy from an array of bytes to array of floats
				copy((byte[]) src, srcPos, (float[]) dst, dstPos, length);

			} else if (src instanceof byte[] && dst instanceof double[]) {

				// Copy from an array of bytes to array of doubles
				copy((byte[]) src, srcPos, (double[]) dst, dstPos, length);

			} else if (src instanceof short[] && dst instanceof byte[]) {

				// Copy from an array of shorts to array of bytes
				copy((short[]) src, srcPos, (byte[]) dst, dstPos, length);

			} else if (src instanceof short[] && dst instanceof int[]) {

				// Copy from an array of shorts to array of ints
				copy((short[]) src, srcPos, (int[]) dst, dstPos, length);

			} else if (src instanceof short[] && dst instanceof long[]) {

				// Copy from an array of shorts to array of longs
				copy((short[]) src, srcPos, (long[]) dst, dstPos, length);

			} else if (src instanceof short[] && dst instanceof float[]) {

				// Copy from an array of shorts to array of floats
				copy((short[]) src, srcPos, (float[]) dst, dstPos, length);

			} else if (src instanceof short[] && dst instanceof double[]) {

				// Copy from an array of shorts to array of doubles
				copy((short[]) src, srcPos, (double[]) dst, dstPos, length);

			} else if (src instanceof int[] && dst instanceof byte[]) {

				// Copy from an array of ints to array of bytes
				copy((int[]) src, srcPos, (byte[]) dst, dstPos, length);

			} else if (src instanceof int[] && dst instanceof short[]) {

				// Copy from an array of ints to array of shorts
				copy((int[]) src, srcPos, (short[]) dst, dstPos, length);

			} else if (src instanceof int[] && dst instanceof long[]) {

				// Copy from an array of ints to array of longs
				copy((int[]) src, srcPos, (long[]) dst, dstPos, length);

			} else if (src instanceof int[] && dst instanceof float[]) {

				// Copy from an array of ints to array of floats
				copy((int[]) src, srcPos, (float[]) dst, dstPos, length);

			} else if (src instanceof int[] && dst instanceof double[]) {

				// Copy from an array of ints to array of doubles
				copy((int[]) src, srcPos, (double[]) dst, dstPos, length);

			} else if (src instanceof long[] && dst instanceof byte[]) {

				// Copy from an array of longs to array of bytes
				copy((long[]) src, srcPos, (byte[]) dst, dstPos, length);

			} else if (src instanceof long[] && dst instanceof short[]) {

				// Copy from an array of longs to array of shorts
				copy((long[]) src, srcPos, (short[]) dst, dstPos, length);

			} else if (src instanceof long[] && dst instanceof int[]) {

				// Copy from an array of longs to array of ints
				copy((long[]) src, srcPos, (int[]) dst, dstPos, length);

			} else if (src instanceof long[] && dst instanceof float[]) {

				// Copy from an array of longs to array of floats
				copy((long[]) src, srcPos, (float[]) dst, dstPos, length);

			} else if (src instanceof long[] && dst instanceof double[]) {

				// Copy from an array of longs to array of doubles
				copy((long[]) src, srcPos, (double[]) dst, dstPos, length);

			} else if (src instanceof float[] && dst instanceof byte[]) {

				// Copy from an array of floats to array of bytes
				copy((float[]) src, srcPos, (byte[]) dst, dstPos, length);

			} else if (src instanceof float[] && dst instanceof short[]) {

				// Copy from an array of floats to array of shorts
				copy((float[]) src, srcPos, (short[]) dst, dstPos, length);

			} else if (src instanceof float[] && dst instanceof int[]) {

				// Copy from an array of floats to array of ints
				copy((float[]) src, srcPos, (int[]) dst, dstPos, length);

			} else if (src instanceof float[] && dst instanceof long[]) {

				// Copy from an array of floats to array of longs
				copy((float[]) src, srcPos, (long[]) dst, dstPos, length);

			} else if (src instanceof float[] && dst instanceof double[]) {

				// Copy from an array of floats to array of doubles
				copy((float[]) src, srcPos, (double[]) dst, dstPos, length);

			} else if (src instanceof double[] && dst instanceof byte[]) {

				// Copy from an array of doubles to array of bytes
				copy((double[]) src, srcPos, (byte[]) dst, dstPos, length);

			} else if (src instanceof double[] && dst instanceof short[]) {

				// Copy from an array of doubles to array of shorts
				copy((double[]) src, srcPos, (short[]) dst, dstPos, length);

			} else if (src instanceof double[] && dst instanceof int[]) {

				// Copy from an array of doubles to array of ints
				copy((double[]) src, srcPos, (int[]) dst, dstPos, length);

			} else if (src instanceof double[] && dst instanceof long[]) {

				// Copy from an array of doubles to array of longs
				copy((double[]) src, srcPos, (long[]) dst, dstPos, length);

			} else if (src instanceof double[] && dst instanceof float[]) {

				// Copy from an array of doubles to array of floats
				copy((double[]) src, srcPos, (float[]) dst, dstPos, length);

			} else {

				throw new ArrayStoreException();
			}
		}
	}

	/**
	 * Copy from an array of bytes to array of shorts. 
	 *
	 * The values will be cast to short. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, int srcPos, 
			short[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (short) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of bytes to array of shorts. 
	 *
	 * The values will be cast to short.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, short[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of bytes to array of ints. 
	 *
	 * The values will be cast to int. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, int srcPos, 
			int[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (int) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of bytes to array of ints. 
	 *
	 * The values will be cast to int.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, int[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of bytes to array of longs. 
	 *
	 * The values will be cast to long. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, int srcPos, 
			long[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (long) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of bytes to array of longs. 
	 *
	 * The values will be cast to long.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, long[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of bytes to array of floats. 
	 *
	 * The values will be cast to float. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, int srcPos, 
			float[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (float) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of bytes to array of floats. 
	 *
	 * The values will be cast to float.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, float[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of bytes to array of doubles. 
	 *
	 * The values will be cast to double. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, int srcPos, 
			double[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (double) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of bytes to array of doubles. 
	 *
	 * The values will be cast to double.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(byte[] src, double[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of shorts to array of bytes. 
	 *
	 * The values will be cast to byte. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, int srcPos, 
			byte[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (byte) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of shorts to array of bytes. 
	 *
	 * The values will be cast to byte.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, byte[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of shorts to array of ints. 
	 *
	 * The values will be cast to int. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, int srcPos, 
			int[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (int) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of shorts to array of ints. 
	 *
	 * The values will be cast to int.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, int[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of shorts to array of longs. 
	 *
	 * The values will be cast to long. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, int srcPos, 
			long[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (long) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of shorts to array of longs. 
	 *
	 * The values will be cast to long.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, long[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of shorts to array of floats. 
	 *
	 * The values will be cast to float. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, int srcPos, 
			float[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (float) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of shorts to array of floats. 
	 *
	 * The values will be cast to float.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, float[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of shorts to array of doubles. 
	 *
	 * The values will be cast to double. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, int srcPos, 
			double[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (double) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of shorts to array of doubles. 
	 *
	 * The values will be cast to double.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(short[] src, double[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of ints to array of bytes. 
	 *
	 * The values will be cast to byte. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, int srcPos, 
			byte[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (byte) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of ints to array of bytes. 
	 *
	 * The values will be cast to byte.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, byte[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of ints to array of shorts. 
	 *
	 * The values will be cast to short. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, int srcPos, 
			short[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (short) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of ints to array of shorts. 
	 *
	 * The values will be cast to short.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, short[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of ints to array of longs. 
	 *
	 * The values will be cast to long. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, int srcPos, 
			long[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (long) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of ints to array of longs. 
	 *
	 * The values will be cast to long.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, long[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of ints to array of floats. 
	 *
	 * The values will be cast to float. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, int srcPos, 
			float[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (float) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of ints to array of floats. 
	 *
	 * The values will be cast to float.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, float[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of ints to array of doubles. 
	 *
	 * The values will be cast to double. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, int srcPos, 
			double[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (double) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of ints to array of doubles. 
	 *
	 * The values will be cast to double.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(int[] src, double[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of longs to array of bytes. 
	 *
	 * The values will be cast to byte. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, int srcPos, 
			byte[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (byte) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of longs to array of bytes. 
	 *
	 * The values will be cast to byte.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, byte[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of longs to array of shorts. 
	 *
	 * The values will be cast to short. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, int srcPos, 
			short[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (short) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of longs to array of shorts. 
	 *
	 * The values will be cast to short.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, short[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of longs to array of ints. 
	 *
	 * The values will be cast to int. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, int srcPos, 
			int[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (int) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of longs to array of ints. 
	 *
	 * The values will be cast to int.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, int[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of longs to array of floats. 
	 *
	 * The values will be cast to float. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, int srcPos, 
			float[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (float) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of longs to array of floats. 
	 *
	 * The values will be cast to float.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, float[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of longs to array of doubles. 
	 *
	 * The values will be cast to double. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, int srcPos, 
			double[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (double) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of longs to array of doubles. 
	 *
	 * The values will be cast to double.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(long[] src, double[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of floats to array of bytes. 
	 *
	 * The values will be cast to byte. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, int srcPos, 
			byte[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (byte) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of floats to array of bytes. 
	 *
	 * The values will be cast to byte.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, byte[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of floats to array of shorts. 
	 *
	 * The values will be cast to short. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, int srcPos, 
			short[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (short) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of floats to array of shorts. 
	 *
	 * The values will be cast to short.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, short[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of floats to array of ints. 
	 *
	 * The values will be cast to int. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, int srcPos, 
			int[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (int) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of floats to array of ints. 
	 *
	 * The values will be cast to int.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, int[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of floats to array of longs. 
	 *
	 * The values will be cast to long. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, int srcPos, 
			long[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (long) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of floats to array of longs. 
	 *
	 * The values will be cast to long.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, long[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of floats to array of doubles. 
	 *
	 * The values will be cast to double. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, int srcPos, 
			double[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (double) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of floats to array of doubles. 
	 *
	 * The values will be cast to double.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(float[] src, double[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of doubles to array of bytes. 
	 *
	 * The values will be cast to byte. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, int srcPos, 
			byte[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (byte) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of doubles to array of bytes. 
	 *
	 * The values will be cast to byte.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, byte[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of doubles to array of shorts. 
	 *
	 * The values will be cast to short. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, int srcPos, 
			short[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (short) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of doubles to array of shorts. 
	 *
	 * The values will be cast to short.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, short[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of doubles to array of ints. 
	 *
	 * The values will be cast to int. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, int srcPos, 
			int[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (int) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of doubles to array of ints. 
	 *
	 * The values will be cast to int.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, int[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of doubles to array of longs. 
	 *
	 * The values will be cast to long. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, int srcPos, 
			long[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (long) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of doubles to array of longs. 
	 *
	 * The values will be cast to long.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, long[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Copy from an array of doubles to array of floats. 
	 *
	 * The values will be cast to float. 
	 *
	 * If the positions or lengths specified are out of range then an 
	 * IndexOutOfBoundsException is thrown and the destination is not
	 * modified.
	 *
	 * @param src
	 *        the source array.
	 * @param srcPos
	 *        starting position in the source array.
	 * @param dst
	 *        the destination array.
	 * @param dstPos
	 *        starting position in the destination data.
	 * @param length   
	 *        the number of array elements to be copied.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, int srcPos, 
			float[] dst, int dstPos, int length) 
	{
		checkCopy(src.length, srcPos, dst.length, dstPos, length);
		
		for (int i = 0; i < length; i++) {
			dst[dstPos++] = (float) src[srcPos++];
		}
	}

	/**
	 * Copy from an array of doubles to array of floats. 
	 *
	 * The values will be cast to float.
	 *
	 * @param src
	 *        the source array.
	 * @param dst
	 *        the destination array.
	 * @throws IndexOutOfBoundsException  
	 *        if copying would cause access of data outside array bounds.
	 * @throws NullPointerException 
	 *        if either <code>src</code> or <code>dest</code> is 
	 *        <code>null</code>.
	 */
	public static void copy(double[] src, float[] dst) {
	  copy(src, 0, dst, 0, dst.length);
	}

	/**
	 * Throws an {@link IndexOutOfBoundsException} if the arguments
	 * would result in an array copy that would go out of bounds.
	 */
	private static void checkCopy(int srcLen, int srcPos, 
			int dstLen, int dstPos, int length) 
	{
		if (srcPos < 0) {
			throw new IndexOutOfBoundsException("srcPos < 0");
		}
		
		if (dstPos < 0) {
			throw new IndexOutOfBoundsException("dstPos < 0");
		}
		
		if (length < 0) {
			throw new IndexOutOfBoundsException("length < 0");
		}
		
		if (srcPos + length > srcLen) {
			throw new IndexOutOfBoundsException("srcPos + length > src.length");
		}
		
		if (dstPos + length > dstLen) {
			throw new IndexOutOfBoundsException("dstPos + length > dst.length");
		}
	}
}
