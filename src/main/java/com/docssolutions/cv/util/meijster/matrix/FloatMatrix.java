/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.matrix;

import com.docssolutions.cv.util.meijster.array.Arrays;

/**
 * Two dimensional matrix of float values.
 *
 * @author Kevin McGuinness
 */
public class FloatMatrix extends Matrix {
    
    /**
     * Serialization UID.
     */
	private static final long serialVersionUID = 8664083529390696723L;
	
	/**
	 * Shared empty matrix 
	 */
	private static final FloatMatrix EMPTY_MATRIX = new FloatMatrix(0,0);
	
	/**
	 * The float values of the matrix in row-major order.
	 */
	public final float[] values;

    /**
     * Construct an uninitialized matrix of the given size.
     *
     * @param rows
     *        The number of rows
     * @param cols
     *        The number of columns
     */
	public FloatMatrix(int rows, int cols) {
		super(Type.Float, rows, cols);
		values = new float[size];
	}
	
	/**
	 * Construct a matrix of the given size with the given values.
	 * 
	 * @param rows
     *        The number of rows
     * @param cols
     *        The number of columns
	 * @param values
	 *        The values to the matrix will have (not copied).
	 */
	public FloatMatrix(int rows, int cols, float ... values) {		
		super(Type.Float, rows, cols, values.length);
		this.values = values;
	}
	
	/**
	 * Construct a matrix of the given size by copying the values in the 
	 * given two dimensional array.
	 * 
	 * The passed two dimensional array must consist of arrays that have
	 * the same dimension.
	 * 
	 * @param matrix
	 *        A two dimensional array of float values.
	 * @throws IllegalArgumentException
	 *        If the matrix does not contain arrays of the same dimension.       
	 */
	public FloatMatrix(float[][] matrix) {
		this(rowsIn(matrix), colsIn(matrix));
		flatten(matrix, values);
	}
	
	/**
	 * Returns the float value at the given row and column index.
	 *
	 * @param row
	 *        The row index.
	 * @param col
	 *        The column index.
	 * @return 
	 *        The float value at (row, col).
	 */
	public final float floatAt(int row, int col) {
		return values[offsetOf(row, col)];
	}
	
    /**
	 * Returns the float value at the index.
	 *
	 * @param index
	 *        The index
	 * @return 
	 *        The float value
	 */
	public final float floatAt(Index2D index) {
		return values[offsetOf(index)];
	}

    /**
	 * Set the float value at the given row and column index.
	 *
	 * @param row
	 *        The row index.
	 * @param col
	 *        The column index.
	 * @param value 
	 *        The float value to set.
	 */
	public final void setFloatAt(int row, int col, float value) {
		values[offsetOf(row, col)] = value;
	}
	
	/**
	 * Set the float value at the given index.
	 *
	 * @param index
	 *        The index.
	 * @param value 
	 *        The float value to set.
	 */
	public final void setFloatAt(Index2D index, float value) {
		values[offsetOf(index)] = value;
	}
	
	/**
	 * Returns the float value at the given offset in the matrix.
	 *
	 * @param offset
	 *        The absolute offset in the matrix.
	 * @return 
	 *        The float value at the given offset.
	 */
	public final float floatAt(int offset) {
		return values[offset];
	}

    /**
	 * Set the float value at the given offset in the matrix.
	 *
	 * @param offset
	 *        The matrix offset
	 * @param value 
	 *        The float value to set.
	 */
	public final void setFloatAt(int offset, float value) {
		values[offset] = value;
	}
	
	/**
	 * Returns the smallest float value in the matrix.
	 *
	 * @return
	 *        The smallest float value, or <code>null</code> if the 
	 *        matrix is empty.
	 */
	public final Float minValue() {
	    return Arrays.min(values);
	}
	
	/**
	 * Returns the largest float value in the matrix.
	 *
	 * @return
	 *        The largest float value, or <code>null</code> if the 
	 *        matrix is empty.
	 */
	public final Float maxValue() {
		return Arrays.max(values);
	}
	
	/**
	 * Returns a transposed version of the matrix.
	 */
	public final FloatMatrix transpose() {
		FloatMatrix m = new FloatMatrix(cols, rows);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				m.values[offsetOf(j,i)] = values[offsetOf(i, j)];
			}
		}
		return m;
	}

    /**
     * Returns a deep copy of the matrix.
     */
	@Override
	public FloatMatrix clone() {
		return new FloatMatrix(rows, cols, values.clone());
	}

    /**
     * Returns true if both matrices are equal in value.
     */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FloatMatrix) {
			FloatMatrix m = (FloatMatrix) obj;
			if (sizeEquals(m)) {
				return java.util.Arrays.equals(values, m.values);
			}
			return false;
		}
		return super.equals(obj);		
	}

    /**
     * Fills the matrix with the %{primative} value of the given number.
     *
     * @param number
     *        The number to fill the matrix with (will be truncated if 
     *        necessary).
     * @return 
     *        This matrix.
     */
	@Override
	public final FloatMatrix fill(Number number) {
		java.util.Arrays.fill(values, number.floatValue());
		return this;
	}

    /**
     * Returns the Float value at the given row and column index.
     *
     * @param row
 	 *        The row index.
 	 * @param col
 	 *        The column index.
 	 * @return 
 	 *        The Float value at (row, col).
     */
	@Override
	public final Float valueAt(int row, int col) {
		return values[offsetOf(row, col)];
	}
	
	/**
   	 * Set the Number value at the given row and column index.
   	 *
   	 * @param row
   	 *        The row index.
   	 * @param col
   	 *        The column index.
   	 * @param value 
   	 *        The Number value to set (will be truncated if necessary).
   	 */
	@Override
	public final void setValueAt(int row, int col, Number value) {
		values[offsetOf(row, col)] = value.floatValue();
	}
	
	/**
     * Returns the Float value at the given matrix offset.
     *
     * @param offset
     *        The absolute offset in the matrix.
 	 * @return 
 	 *        The Float value at the given offset.
     */
	@Override
	public final Float valueAt(int offset) {
		return values[offset];
	}
	
	/**
   	 * Set the Number value at the given matrix offset.
   	 *
   	 * @param offset
   	 *        The absolute offset in the matrix.
   	 * @param value 
   	 *        The Number value to set (will be truncated if necessary).
   	 */
	@Override
	public final void setValueAt(int offset, Number value) {
		values[offset] = value.floatValue();
	}
	
	/**
	 * Returns the matrix values (not copied).
	 */
	@Override
	public final float[] values() {
		return values;
	}
	
	/**
	 * Returns a shared empty matrix instance.
	 */
	public static FloatMatrix empty() {
		return EMPTY_MATRIX;
	}
	
	/**
	 * Construct and return an identity matrix with the given number of rows.
	 *
	 * @param rows
	 *        The number of rows/columns for the matrix to have.
	 * @return
	 *        A new identity matrix.
	 */
	public static FloatMatrix eye(int rows) {
		FloatMatrix m = new FloatMatrix(rows, rows);
		m.fill(0);
		for (int i = 0; i < rows; i++) {
			m.setFloatAt(i, i, (float) 1);
		}
		return m;
	}
	
	/**
	 * Construct and return an zero matrix with the given number of rows
	 * and columns.
	 *
	 * @param rows
	 *        The number of rows for the matrix to have.
	 * @param cols
 	 *        The number of cols for the matrix to have.
	 * @return
	 *        A new zero matrix.
	 */
	public static FloatMatrix zeros(int rows, int cols) {
		FloatMatrix m = new FloatMatrix(rows, rows);
		m.fill(0);
		return m;
	}
	
	/**
	 * Construct and return a matrix of ones with the given number of rows
	 * and columns.
	 *
	 * @param rows
	 *        The number of rows for the matrix to have.
	 * @param cols
 	 *        The number of cols for the matrix to have.
	 * @return
	 *        A new matrix of ones.
	 */
	public static FloatMatrix ones(int rows, int cols) {
		FloatMatrix m = new FloatMatrix(rows, rows);
		m.fill(1);
		return m;
	}
	
	/**
     * Copy the given two dimensional array of float values into the given 
	 * array. If the passed array does not contain at enough room for the
	 * passed matrix, a new array is allocated and returned.
	 * 
	 * The passed two dimensional array must consist of arrays that have
	 * the same dimension.
	 *
	 * The copy is performed such that the flattened array is in row major
	 * order. 
	 * 
	 * @param matrix
	 *        The 2D array of float values to flatten
	 * @param array
	 *        The array to copy to (can be <code>null</code>).
	 * @return
	 *        A flattened version of the matrix.
	 * @throws IllegalArgumentException
	 *        If the matrix does not contain arrays of the same dimension.
	 */
	public static final float[] flatten(float[][] matrix, float[] array) {
		int rows = matrix.length;
		int cols = matrix.length > 0 ? matrix[0].length : 0;
		int size = rows * cols;

		if (array == null || array.length < size) { 
			array = new float[size];
		}

		for (int i = 0; i < rows; i++) {

			if (cols != matrix[i].length) {
				throw new IllegalArgumentException();
			}

			System.arraycopy(matrix[i], 0, array, cols * i, cols);
		}

		return array;
	}
	
    /**
     * Returns the number of rows in the given two dimensional array.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The number of rows.
	 */
	public static final int rowsIn(float[][] matrix) {
		return matrix.length;
	}

	/**
	 * Returns the number of columns in the given two dimensional array.
	 * 
	 * This method assumes that the arrays in the given matrix all have
	 * equal lengths.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The number of columns.
	 */
	public static final int colsIn(float[][] matrix) {
		return matrix.length > 0 ? matrix[0].length : 0;
	}

	/**
	 * Returns the total number of elements in the given two dimensional 
	 * array.
	 * 
	 * This method assumes that the arrays in the given matrix all have
	 * equal lengths.
	 * 
	 * @param matrix
	 *        A two dimensional array.
	 * @return
	 *        The total number of elements.
	 */
	public static final int sizeOf(float[][] matrix) {
		return rowsIn(matrix) * colsIn(matrix);
	}
}
