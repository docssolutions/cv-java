/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.array;

/**
 * A reduce operation.
 * 
 * @author Kevin McGuinness
 */
public interface Reduction<T, U> {

	/**
	 * Perform a reduction.
	 * 
	 * @param accumulator 
	 *        The accumulated value so far.
	 * @param value
	 *        The next value in the array.
	 * @param index
	 *        The index of the value in the array.
	 * @return
	 *        The result of the reduction.
	 */
	public U reduce(U accumulator, T value, int index);
	
	
	/**
	 * A long width reduction.
	 */
	public interface Long {
		
		/**
		 * Perform a reduction.
		 * 
		 * @param accumulator 
		 *        The accumulated value so far.
		 * @param value
		 *        The next value in the array.
		 * @param index
		 *        The index of the value in the array.
		 * @return
		 *        The result of the reduction.
		 */
		public long reduce(long accumulator, long value, int index);
	}
	
	/**
	 * A double precision reduction.
	 */
	public interface Double {
		
		/**
		 * Perform a reduction.
		 * 
		 * @param accumulator 
		 *        The accumulated value so far.
		 * @param value
		 *        The next value in the array.
		 * @param index
		 *        The index of the value in the array.
		 * @return
		 *        The result of the reduction.
		 */
		public long reduce(double accumulator, double value, int index);
	}
}
