/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.array;

/**
 * Interfaces for filters
 * 
 * @author Kevin McGuinness
 */
public interface Filter {
	
	/**
	 * Returns <code>true</code> if the given value should be retained.
	 * 
     * @param object
     *        An object.
     * @return
     *        <code>true</code> if the value should be retained.
	 */
	public boolean retain(Object object);


	/**
	 * An byte filter.
	 */
	public interface Byte {
		
		/**
		 * Returns <code>true</code> if the given value should be retained.
		 * 
		 * @param value
		 *        An byte value.
		 * @return
		 *        <code>true</code> if the value should be retained.
		 */
		public boolean retain(byte value);
	}

	/**
	 * An short filter.
	 */
	public interface Short {
		
		/**
		 * Returns <code>true</code> if the given value should be retained.
		 * 
		 * @param value
		 *        An short value.
		 * @return
		 *        <code>true</code> if the value should be retained.
		 */
		public boolean retain(short value);
	}

	/**
	 * An int filter.
	 */
	public interface Integer {
		
		/**
		 * Returns <code>true</code> if the given value should be retained.
		 * 
		 * @param value
		 *        An int value.
		 * @return
		 *        <code>true</code> if the value should be retained.
		 */
		public boolean retain(int value);
	}

	/**
	 * An long filter.
	 */
	public interface Long {
		
		/**
		 * Returns <code>true</code> if the given value should be retained.
		 * 
		 * @param value
		 *        An long value.
		 * @return
		 *        <code>true</code> if the value should be retained.
		 */
		public boolean retain(long value);
	}

	/**
	 * An float filter.
	 */
	public interface Float {
		
		/**
		 * Returns <code>true</code> if the given value should be retained.
		 * 
		 * @param value
		 *        An float value.
		 * @return
		 *        <code>true</code> if the value should be retained.
		 */
		public boolean retain(float value);
	}

	/**
	 * An double filter.
	 */
	public interface Double {
		
		/**
		 * Returns <code>true</code> if the given value should be retained.
		 * 
		 * @param value
		 *        An double value.
		 * @return
		 *        <code>true</code> if the value should be retained.
		 */
		public boolean retain(double value);
	}
}
