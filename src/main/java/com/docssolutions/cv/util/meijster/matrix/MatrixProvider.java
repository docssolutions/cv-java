/*
 * Copyright (c) 2015 Kevin McGuinness, DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.meijster.matrix;

/**
 * Interface for classes that can provide matrices.
 * 
 * @author Kevin McGuinness
 */
public interface MatrixProvider {
	
	/**
	 * Returns the default type created by this matrix provider if no type
	 * is specified when calling <code>getMatrix()</code>. This may be null
	 * if it is unknown.
	 * 
	 * @return The default matrix type.
	 */
	public Matrix.Type getDefaultMatrixType();
	
	/**
	 * Request a matrix from the matrix provider.
	 * 
	 * @param alwaysCopy
	 *            If <code>true</code> the a copy is always returned, otherwise
	 *            the matrix may or may not be a copy.
	 * @return A matrix.
	 */
	public Matrix getMatrix(boolean alwaysCopy);
	
	/**
	 * Request a matrix of the given type from the matrix provider.
	 * 
	 * @param type
	 *            The matrix type. If <code>null</code> then the most convenient
	 *            type for the receiver will be returned.
	 * @param alwaysCopy
	 *            If <code>true</code> the a copy is always returned, otherwise
	 *            the matrix may or may not be a copy.
	 * @return A matrix.
	 */
	public Matrix getMatrix(Matrix.Type type, boolean alwaysCopy);
}
