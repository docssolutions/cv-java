/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import java.awt.Point;
import java.util.*;

import java.util.concurrent.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.filter.*;
import com.docssolutions.cv.util.*;

public class RegionProperties
{
    private LinkedList<ConnectedComponentMetrics> components_metrics;
    private int next_available_component_id;
    private float[][] img_mx;
    
    public RegionProperties() 
    {
        components_metrics = new LinkedList<ConnectedComponentMetrics>();
        next_available_component_id = 0;
        img_mx = null;
    }

    public void setImageMatrix(float[][] img_mx) {
        this.img_mx = img_mx;
    }
    
    public void calculatePropertiesAPriori(Set<String> ps) // Change set of properties from String to enum
    {
        ExecutorService executor = Executors.newFixedThreadPool(CVUtils.NUM_THREADS);
        Iterator i = components_metrics.iterator();
        while(i.hasNext()) {
            ConnectedComponentMetrics ccm = (ConnectedComponentMetrics) i.next();
            MultiplePropertiesCalculation mpc = new MultiplePropertiesCalculation(ccm, ps);
            executor.execute(mpc);
        }
        try {
            executor.shutdown();
            executor.awaitTermination(30000, TimeUnit.SECONDS);
        } catch (InterruptedException ie) {
            executor.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private class MultiplePropertiesCalculation implements Runnable {
        private ConnectedComponentMetrics ccm;
        private Set<String> ps;
        MultiplePropertiesCalculation (ConnectedComponentMetrics ccm, Set<String> ps) {
            this.ccm = ccm;
            this.ps = ps;
        }
        public void run() {
            Iterator i = ps.iterator();
            while(i.hasNext()) {
                String p = (String) i.next();
                switch(p) {
                case "Area":            ccm.getArea();            break;
                case "Perimeter":       ccm.getPerimeter();       break; 
                case "Centroid":        ccm.getCentroid();        break; 
                case "MajorAxisLength": ccm.getMajorAxisLength(); break; 
                case "MinorAxisLength": ccm.getMinorAxisLength(); break;
                case "Eccentricity":    ccm.getEccentricity();    break; 
                case "EquivDiameter":   ccm.getEquivDiameter();   break;
                }
            }
        }
    }

    public LinkedList<ConnectedComponentMetrics> getRawRegionProperties(float[][] mx, float[][] img_mx)
    {
        this.img_mx = img_mx;
        return getRawRegionProperties(mx);
    }
    
    public LinkedList<ConnectedComponentMetrics> getRawRegionProperties(float[][] mx)
    {
        // assert ConnectedComponentCVUtils.validateMatrixOfComponents(mx);
        int n = mx.length;
        int m = mx[0].length;
        int i,j;
        LinkedList<Point> pixels_list = null;

        //Labeller lb = new Labeller();
        //float[][] labels = lb.getRawLabels(mx);

        //int size = lb.getComponentsNumber();
        int size = (int)CVUtils.maxValueOfMatrix(mx);
        //CVUtils.println(":: region properties. "+size+" components found in matrix of ("+m+","+n+")...");
        //CVUtils.printPresenceInMatrix(mx);

        ArrayList<LinkedList<Point>> components_pixels_list = new ArrayList<LinkedList<Point>>(size);
        for (i=0; i<size; i++) {
            components_pixels_list.add(i, new LinkedList<Point>());
        }

        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                if(mx[i][j] != 0.0f) {
                    pixels_list = (LinkedList<Point>) components_pixels_list.get((int)(mx[i][j]-1.0f));
                    pixels_list.add(new Point(j,i));
                }


        for(i=0;i<size;i++)
            components_metrics.add(new ConnectedComponentMetrics(i+1, (LinkedList<Point>) components_pixels_list.get(i)));
        
        return components_metrics;
    }

    public float[][] getCentroids() {
		int size = this.components_metrics.size();

		float[][] centroids = new float[size][2];
		int k = 0;

		for (ConnectedComponentMetrics m : components_metrics) {
			centroids[k][0] = m.getCentroid().x;
			centroids[k][1] = m.getCentroid().y;
			k++;
		}
		return centroids;
	}

    public float[] getAreas() {
		int size = this.components_metrics.size();

		float[] areas = new float[size];
		int k = 0;

		for (ConnectedComponentMetrics m : components_metrics)
			areas[k++] = m.getArea();

		return areas;
	}

    public float[] getPerimeters() {
		int size = this.components_metrics.size();

		float[] perimeters = new float[size];
		int k = 0;

		for (ConnectedComponentMetrics m : components_metrics)
			perimeters[k++] = m.getPerimeter();

		return perimeters;
	}

    public float[] getMajorAxisLengths() {
		int size = this.components_metrics.size();

		float[] majorAxisLengths = new float[size];
		int k = 0;

		for (ConnectedComponentMetrics m : components_metrics)
			majorAxisLengths[k++] = m.getMajorAxisLength();

		return majorAxisLengths;
	}

    public float[] getMinorAxisLengths() {
		int size = this.components_metrics.size();

		float[] minorAxisLengths = new float[size];
		int k = 0;

		for (ConnectedComponentMetrics m : components_metrics)
			minorAxisLengths[k++] = m.getMinorAxisLength();

		return minorAxisLengths;
	}

    public float[] getEccentricities() {
		int size = this.components_metrics.size();

		float[] eccentricities = new float[size];
		int k = 0;

		for (ConnectedComponentMetrics m : components_metrics)
			eccentricities[k++] = m.getEccentricity();

		return eccentricities;
	}

    public float[] getEquivDiameters() {
		int size = this.components_metrics.size();

		float[] equivDiameters = new float[size];
		int k = 0;

		for (ConnectedComponentMetrics m : components_metrics)
			equivDiameters[k++] = m.getEquivDiameter();

		return equivDiameters;
	}

    public LinkedList<float[]> getPixelValues() {
        assert img_mx != null;
        
		LinkedList<float[]> pixel_values_list = new LinkedList<float[]>();
        float[] px_vals = null;

		for (ConnectedComponentMetrics m : components_metrics) {
			px_vals = m.getPixelValues(img_mx);
            pixel_values_list.add(px_vals);
        }

		return pixel_values_list;
    }
}
