/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.moore;

import java.awt.Point;

import com.docssolutions.cv.CVUtils;

public class MoorePoint extends Point
{
    private boolean[][] visited;
    private Point last_visited;
    private Point entry_point;
    public MoorePoint(Point p, Point b) {
        super(p);
        entry_point = new Point(b);
        last_visited = new Point(b);
        initVisitedNeighborhood();
        setNeighborAsVisited(b);
    }
    public void initVisitedNeighborhood()
    {
        boolean[][] vd = {{false, false, false},
                          {false, true,  false},
                          {false, false, false}};
        visited = vd;
    }
    private void setNeighborAsVisited(Point b)
    {
        int u = b.x - this.x + 1;
        int v = b.y - this.y + 1;

        //System.out.println(":: ...setNeighborAsVisited b="+b+", u="+u+", v="+v);
        assert 0 <= u && u < 3;
        assert 0 <= v && v < 3;
        assert u != 1 || v != 1;
        visited[v][u] = true;
    }
    private boolean isNeighborAlreadyVisited(Point p)
    {
        int u = p.x - this.x + 1;
        int v = p.y - this.y + 1;

        //System.out.println(":: ...isNeighborAlreadyVisited p="+p+", u="+u+", v="+v);
        assert 0 <= u && u < 3;
        assert 0 <= v && v < 3;
        assert u != 1 || v != 1;
        return visited[v][u];
    }
    public boolean[][] getVisitedNeighbors() {
        return visited;
    }
    public Point entryPoint() {
        return new Point(entry_point);
    }
    public Point lastVisited() {
        return new Point(last_visited);
    }
    public MoorePoint nextClockwisePoint() 
    {
        return nextClockwisePoint(last_visited);
    }
    public MoorePoint nextClockwisePoint(Point previous) 
    {
        setNeighborAsVisited(previous);
        Point next = nextClockwisePoint(this, previous);
        if (!isNeighborAlreadyVisited(next)) {
            setNeighborAsVisited(next);
            last_visited = new Point(next);
            return new MoorePoint(next, previous);
        }else{
            return null; // All neighborhood already visited.
        }
    }
    private Point nextClockwisePoint(Point center, Point previous)
    {
        Point next = null;
        int u = previous.x - center.x;
        int v = center.y - previous.y;
        //System.out.println(":: Simple next clockwise point: \ncenter="+center+", previous="+previous+". \nu="+u+",v="+v);
        if      ( u == 1  && v == 0  ) { // E
            // System.out.println(":: E->SE");
            next = new Point(center.x+1, center.y+1);
        }else if( u == 1  && v == -1 ) { // SE
            // System.out.println(":: SE->S");
            next = new Point(center.x, center.y+1);
        }else if( u == 0  && v == -1 ) { // S
            // System.out.println(":: S->SW");
            next = new Point(center.x-1, center.y+1);
        }else if( u == -1 && v == -1 ) { // SW
            // System.out.println(":: SW->W");
            next = new Point(center.x-1, center.y);
        }else if( u == -1 && v == 0  ) { // W
            // System.out.println(":: W->NW");
            next = new Point(center.x-1, center.y-1);
        }else if( u == -1 && v == 1  ) { // NW
            // System.out.println(":: NW->N");
            next = new Point(center.x, center.y-1);
        }else if( u == 0  && v == 1  ) { // N
            // System.out.println(":: N->NE");
            next = new Point(center.x+1, center.y-1);
        }else if( u == 1  && v == 1  ) { // NE
            // System.out.println(":: NE->E");
            next = new Point(center.x+1, center.y);
        }else{
            System.out.println(":: IMPOSSIBLE CASE. (u,v) is ("+u+","+v+")... <MUST DEBUG>");
            System.exit(-1);
        }
        return next;
    }
    public boolean equals(Object obj) {
        MoorePoint q = (MoorePoint) obj;
        CVUtils.print(":: MoorePoint.equals, ");
        CVUtils.print(":: points ("+this.x+","+this.y+") - ("+q.x+","+q.y+")");
        CVUtils.print(":: entrys ("+this.entry_point.x+","+this.entry_point.y+") - ("+q.entryPoint().x+","+q.entryPoint().y+")");
        CVUtils.println("\n");
        return super.equals((Point) q) && this.entryPoint().equals(q.entryPoint());
    }
    public boolean alreadyPlacedAndVisitedNeighborsOf(MoorePoint q) {
        if (!super.equals(q)) return false;
        return alreadyVisitedNeighbors(q.getVisitedNeighbors());
    }
    private boolean alreadyVisitedNeighbors(boolean[][] visited) {
        for(int i=0;i<3;i++)
            for(int j=0;j<3;j++)
                if(visited[i][j] && !this.visited[i][j])
                    return false;
        return true;
    }
    public String toString() {
        return "\n|MoorePoint("+x+","+y+")::"+
            "NW:"+(visited[0][0]?1:0)+","+ "N:"+(visited[0][1]?1:0)+","+ "NE:"+(visited[0][2]?1:0)+","+
            "W:"+(visited[1][0]?1:0)+","+                                "E:"+(visited[1][2]?1:0)+","+
            "SW:"+(visited[2][0]?1:0)+","+ "S:"+(visited[2][1]?1:0)+","+ "SE:"+(visited[2][2]?1:0)+"\t|"+
            "\n|lastVisited("+last_visited.x+","+last_visited.y+"), entryPoint("+entry_point.x+","+entry_point.y+")\t\t\t|\n";
    }
}
