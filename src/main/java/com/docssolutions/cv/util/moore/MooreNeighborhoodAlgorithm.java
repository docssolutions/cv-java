/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util.moore;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.util.ConnectedComponentUtils;
import com.docssolutions.cv.util.moore.MoorePoint;

public class MooreNeighborhoodAlgorithm
{
    private Point[] bounding_box;
    private LinkedList<Point> pixels_list;
    private LinkedList<Point> boundary;

    public MooreNeighborhoodAlgorithm()
    {
        this(null);
    }

    public MooreNeighborhoodAlgorithm(LinkedList<Point> pixels_list)
    {
        this.pixels_list = new LinkedList<Point>(pixels_list);
        this.boundary = null;
        this.bounding_box = null;
    }

    /**
     * Reference: 
     * - {@url https://en.wikipedia.org/wiki/Moore_neighborhood}
     * - {@url http://www.imageprocessingplace.com/downloads_V3/root_downloads/tutorials/contour_tracing_Abeer_George_Ghuneim/moore.html}
     */

    public LinkedList<Point> computeBoundaryPixelsWithMooreNeighborhoodAlgorithm()
    {
        if (boundary != null) return boundary;
        if (pixels_list == null) {
            //throws new IllegalArgumentException("Cannot compute area without pixel's list.");
            System.out.println("Cannot compute area without pixel's list.");
            System.exit(-1);
        }

        if (bounding_box == null) 
            bounding_box = ConnectedComponentUtils.boundingBox(pixels_list);

        int m = bounding_box[1].x - bounding_box[0].x + 1;
        int n = bounding_box[1].y - bounding_box[0].y + 1;
        Point offset = bounding_box[0];

        int i = 0, j = 0;

        byte[][] box = new byte[n+2][m+2];
        Point q;

        Iterator it = pixels_list.iterator();
        while(it.hasNext()) { 
            q = (Point) it.next(); 
            box[q.y - offset.y + 1][q.x - offset.x + 1] = 1;
        }

        //System.out.println(":: Printing component");
        //CVUtils.printPresenceInMatrix(box);

        // Moore Neighborhood Algorithm
        boundary = new LinkedList<Point>(); // Result
        Stack<MoorePoint> BT = new Stack<MoorePoint>(); // Backtrack
        MoorePoint p, c, s;
        Point b;

        //Point[] pxs = ConnectedComponentCVUtils.leftBottomMostPixel(box);
        Point[] pxs = ConnectedComponentUtils.leftTopMostPixel(box);
        //CVUtils.println(":: "+pxs[0]+" con "+pxs[1]);
        s = new MoorePoint(pxs[0], pxs[1]); // First pixel from boundary.
        p = new MoorePoint(pxs[0], pxs[1]); // Current pixel from boundary.
        //CVUtils.println(p.toString());
        //System.out.println(":: Left bottom most pixel="+p);
        boundary.add(p);
        BT.push(s); // Keep in backtrace first pixel.
        //b = new Point(pxs[1]); // Pixel from which pxs[0] was accessed.
        c = p.nextClockwisePoint();
        p.initVisitedNeighborhood();

        while( !p.alreadyPlacedAndVisitedNeighborsOf(s) ) { // Comparing checks actual point coordinates and entry_point coordinates.
            //System.out.println(":: Processing p="+p+" and next clockwise pixel c="+c);
            if (box[c.y][c.x] == 1) {
                box[c.y][c.x] = 2;
                boundary.add(c);
                BT.push(c); // Include point to backtrack stack
                //b = p;
                p = c;
                c = p.nextClockwisePoint();
                if (c == null) {
                    CVUtils.println(":: null in presence... this branch may never happens");
                    //CVUtils.println(":: null reached in present point... stats are:");
                    //CVUtils.println(":: BT size: "+BT.size());
                    //CVUtils.printAFewElemsFromStack(BT);
                    BT.pop(); // Dispose null point
                    p = BT.pop();
                    //b = p.lastVisited();
                    c = p.nextClockwisePoint();
                }
            }else{
                //b = c;
                //c = p.nextClockwisePoint(b);
                c = p.nextClockwisePoint();
                if (c == null) {
                    //CVUtils.println(":: null reached in abscent point... stats are:");
                    //CVUtils.println(":: BT size: "+BT.size());
                    //CVUtils.printAFewElemsFromStack(BT);
                    BT.pop(); // Disposing last position where no new pixels can be reached
                    p = BT.pop();
                    //b = p.lastVisited();
                    c = p.nextClockwisePoint();
                }
            }
        }
        return boundary;
    }
    public void setPixelsList(LinkedList<Point> pixels_list) {
        this.pixels_list = new LinkedList<Point>(pixels_list);
    }
    public boolean hasCalculatedBoundingBox() { return bounding_box != null; }
    public Point[] boundingBox() { return bounding_box; }
}
