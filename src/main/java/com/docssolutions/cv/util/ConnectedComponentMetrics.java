/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.util.EllipseMetrics;
import com.docssolutions.cv.util.Pixel;
import com.docssolutions.cv.util.moore.MooreNeighborhoodAlgorithm;

public class ConnectedComponentMetrics
{
    private int id;
    private LinkedList<Point> boundary;
    private float perimeter;
    private float equiv_diameter;
    private int area;
    private Point2D.Float centroid;
    private Point[] bounding_box;
    private LinkedList<Point> pixels_list;

    private MooreNeighborhoodAlgorithm moorenhood;
    private EllipseMetrics ellipse_metrics;
    private float eccentricity;
    private float orientation;
    private float major_axis_length;
    private float minor_axis_length;

    private float[][] image;

    public ConnectedComponentMetrics() { this(-1, null); }

    public ConnectedComponentMetrics(int id, LinkedList<Point> pixels_list)
    {
        this.id = id;
        boundary = null;
        perimeter = -1.0f;
        equiv_diameter = -1.0f;
        area = -1;
        centroid = null;
        bounding_box = new Point[2]; // Top-left (0) and bottom-right (1) points.
        if (pixels_list != null) {
            this.pixels_list = (LinkedList<Point>) pixels_list.clone();
        }else{
            this.pixels_list = null;
        }
        ellipse_metrics = new EllipseMetrics(pixels_list);
        moorenhood = new MooreNeighborhoodAlgorithm(pixels_list);
        image = null; // DAFUQ??
        eccentricity = -1.0f;
        orientation = -1.0f;
        major_axis_length = -1.0f;
        minor_axis_length = -1.0f;
    }

    public LinkedList<Pixel> getPixelsValues()
    {
        if (image == null) {
            System.out.println(":: [ERROR] getPixelValues cannot be called without a prior set of an image.");
            System.exit(-1);
        }

        LinkedList<Pixel> pixels = new LinkedList<Pixel>();
        Iterator it = pixels_list.iterator();
        Point p;

        while(it.hasNext()) {
            p = (Point) it.next();
            pixels.add(new Pixel(p, image[p.y][p.x]));
        }
        return pixels;
    }
    
    public float getPerimeter()
    {
        if (perimeter != -1) return perimeter;
        if (pixels_list == null) {
            //throws new IllegalArgumentException("Cannot compute area without pixel's list.");
            System.out.println("Cannot compute area without pixel's list.");
            System.exit(-1);
        }

        moorenhood.setPixelsList(pixels_list);
        boundary = moorenhood.computeBoundaryPixelsWithMooreNeighborhoodAlgorithm();

        //perimeter = 0.0f;

        //if (boundary.size() == 1)
        //    return perimeter;

        //float d = 0.0f;

        //Iterator i = boundary.iterator();
        //Point p0, p, q;
        //p = p0 = (Point) i.next();
        //q = null;
        //System.out.println(":: Printing boundary pixels...");
        //System.out.print(":: ");
        //while(i.hasNext()) {
        //    q = (Point) i.next();
        //    perimeter += p.distance(q);
        //    p = q;
        //    if(i.hasNext())
        //        System.out.print("("+p.x+","+p.y+") -=- ");
        //    else
        //        System.out.println("("+p.x+","+p.y+")");
        //}
        //perimeter += q.distance(p0);

        // Do some magic with the boundary
        //
        // Or do as Matlab does:
        // function perimeter = computePerimeterFromBoundary(B)
        // 
        // delta = diff(B).^2;
        // if(size(delta,1) > 1)
        //     isCorner  = any(diff([delta;delta(1,:)]),2); % Count corners.
        //     isEven    = any(~delta,2);
        //     perimeter = sum(isEven)*0.980 + sum(~isEven)*1.406 - sum(isCorner)*0.091;
        // else
        //     perimeter = 0; % if the number of pixels is 1 or less.
        // end
        //
        // Reported functions need to be implemented:
        // diff, .^2, any, ~, sum

        perimeter = 0.0f;

        if (boundary.size() == 1)
            return perimeter;
        
        Iterator i = boundary.iterator(); 
        Point p;
        while(i.hasNext()) {
            p = (Point) i.next();
            //System.out.println(":: B("+p.x+","+p.y+")");
        }

        LinkedList<Point> diff_square = new LinkedList<Point>();

        i = boundary.iterator();
        Point q;
        double x, y;
        p = (Point) i.next();
        q = null;
        while(i.hasNext()) {
            q = (Point) i.next();
            x = Math.pow(q.x-p.x, 2);
            y = Math.pow(q.y-p.y, 2);
            //System.out.println(":: v_diff=("+x+","+y+")");
            diff_square.add(new Point((int) x, (int) y));
            p = q;
        }
        //System.out.println(":: Pixels list size: "+pixels_list.size()+", diff list size:"+diff_square.size());
        int even_count = 0;
        i = diff_square.iterator();
        while(i.hasNext()) {
            q = (Point) i.next();
            even_count += q.x == 0 || q.y == 0 ? 1 : 0;
        }
        
        int not_even_count = diff_square.size() - even_count;
        
        diff_square.add(diff_square.getFirst());

        LinkedList<Point> double_diff = new LinkedList<Point>();
        i = diff_square.iterator();
        p = (Point) i.next();
        q = null;
        while(i.hasNext()) {
            q = (Point) i.next();
            //System.out.println(":: v_double_diff=("+(q.x-p.x)+","+(q.y-p.y)+")");
            double_diff.add(new Point(q.x-p.x, q.y-p.y));
            p = q;
        }
        
        int corner_count = 0;
        i = double_diff.iterator();
        while(i.hasNext()) {
            q = (Point) i.next();
            corner_count += q.x != 0 || q.y != 0 ? 1 : 0;
        }
        
        //System.out.println(":: even="+even_count+", corner="+corner_count);
        perimeter = even_count * 0.980f + not_even_count * 1.406f - corner_count * 0.091f ;

        return perimeter;
    }

    public int getArea()
    {
        if (area != -1) return area;
        if (pixels_list == null) {
            //throws IllegalArgumentException("Cannot compute area without pixel's list.");
            System.out.println("Cannot compute area without pixel's list.");
            System.exit(-1);
        }

        area = pixels_list.size();
        return area;
    }

    public float getEquivDiameter()
    {
        if (equiv_diameter != -1.0f) { return equiv_diameter; }

        float a = (float) getArea();
        equiv_diameter = (float) Math.sqrt(4 * a / Math.PI);
        return equiv_diameter;
    }

    public Point[] getBoundingBox()
    {
        if(bounding_box[0] != null && bounding_box[1] != null) { return bounding_box; }
        else if(moorenhood.hasCalculatedBoundingBox()) {
            bounding_box = moorenhood.boundingBox();
            return bounding_box;
        } else { 
            bounding_box = ConnectedComponentUtils.boundingBox(pixels_list);
            return bounding_box;
        }
    }

    public Point2D.Float getCentroid()
    {
        if(centroid != null) { return centroid; }
        centroid = ConnectedComponentUtils.getCentroid(pixels_list);
        return centroid;
    }

    public float getEccentricity()
    {
        if (eccentricity != -1.0f) return eccentricity;
        if (pixels_list == null) {
            //throws IllegalArgumentException("Cannot compute area without pixel's list.");
            System.out.println("Cannot compute area without pixel's list.");
            System.exit(-1);
        }

        if(!ellipse_metrics.hasExistingPixelsList()) {
            ellipse_metrics.setPixelsList(pixels_list);
        }
        return ellipse_metrics.getEccentricity();
    }
    
    public float getMajorAxisLength()
    {
        if (major_axis_length != -1.0f) return major_axis_length;
        if (pixels_list == null) {
            //throws IllegalArgumentException("Cannot compute area without pixel's list.");
            System.out.println("Cannot compute area without pixel's list.");
            System.exit(-1);
        }

        if(!ellipse_metrics.hasExistingPixelsList()) {
            ellipse_metrics.setPixelsList(pixels_list);
        }
        return ellipse_metrics.getMajorAxisLength();
    }

    public float getMinorAxisLength()
    {
        if (minor_axis_length != -1.0f) return minor_axis_length;
        if (pixels_list == null) {
            //throws IllegalArgumentException("Cannot compute area without pixel's list.");
            System.out.println("Cannot compute area without pixel's list.");
            System.exit(-1);
        }

        if(!ellipse_metrics.hasExistingPixelsList()) {
            ellipse_metrics.setPixelsList(pixels_list);
        }
        return ellipse_metrics.getMinorAxisLength();
    }

    public float[] getPixelValues(float[][] img_mx)
    {
        float[] u_mx = new float[pixels_list.size()];
        int i = 0;
        for(Point p : pixels_list) {
            u_mx[i++] = img_mx[p.y][p.x];
        }
        return u_mx;
    }
}
