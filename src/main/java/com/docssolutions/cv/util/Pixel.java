/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import java.awt.Point;

public class Pixel extends Point
{
    private float gray;
    private float r, g, b;
    public Pixel(Point p, float gray) {
        this(p.x, p.y, gray);
    }
    public Pixel(Point p, float r, float g, float b) {
        this(p.x, p.y, r, g, b);
    }
    public Pixel(int x, int y, float gray) {
        super(x, y);
        setGray(gray);
    }
    public Pixel(int x, int y, float r, float g, float b) {
        super(x, y);
        setRGB(r, g, b);
    }
    public float gray() {
        return gray;
    }
    public float[] rgb() {
        float[] rgb = {r, g, b};
        return rgb;
    }
    public void setGray(float gray) {
        this.gray = r = g = b = gray;
    }
    public void setRGB(float r, float g, float b) {
        this.r = r; this.g = g; this.b = b; 
        gray = (r + g + b) / 3.0f;
    }
}
