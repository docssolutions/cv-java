/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.util;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.*;

public class ConnectedComponentUtils
{
    public static Point[] boundingBox(LinkedList<Point> pixels_list)
    {
        Point[] bounding_box = new Point[2];

        int left_most  = Integer.MAX_VALUE, right_most = Integer.MIN_VALUE;
        int upper_most = Integer.MAX_VALUE, lower_most = Integer.MIN_VALUE;
        
        Iterator i = pixels_list.iterator();
        Point p;

        while(i.hasNext()) {
            p = (Point) i.next();
            if (left_most > p.x)  left_most  = p.x;
            if (right_most < p.x) right_most = p.x;
            if (lower_most < p.y) lower_most = p.y;
            if (upper_most > p.y) upper_most = p.y;
        }

        bounding_box[0] = new Point(left_most, upper_most);
        bounding_box[1] = new Point(right_most, lower_most);

        return bounding_box;
    }

    public static Point[] leftBottomMostPixel(byte[][] box)
    {
        Point[] pxs = new Point[2];
        int n = box.length;
        int m = box[0].length;
        int i,j;

        // If box[n-1][0] we should launch an error, there's no preceding pixels from which box[n-1][0] was entered.

        for(j=0;j<m;j++)
            for(i=n-1;i>=0;i--)
                if (box[i][j] == 1) {
                    pxs[0] = new Point(j,i);
                    if(i!=n-1)
                        pxs[1] = new Point(j,i+1);
                    else
                        pxs[1] = new Point(j-1,n-1); // Pixel from which (j,i) was entered must be the left one if in lower bounding.
                    return pxs;
                }
        return null;
    }

    public static Point[] leftTopMostPixel(byte[][] box)
    {
        Point[] pxs = new Point[2];
        int n = box.length;
        int m = box[0].length;
        int i,j;

        // If box[n-1][0] we should launch an error, there's no preceding pixels from which box[n-1][0] was entered.

        for(j=0;j<m;j++)
            //for(i=n-1;i>=0;i--)
            for(i=0;i<n-1;i++)
                if (box[i][j] == 1) {
                    pxs[0] = new Point(j,i);
                    if(i!=0)
                        pxs[1] = new Point(j,i-1);
                    else
                        pxs[1] = new Point(j-1,0); // Pixel from which (j,i) was entered must be the left one if in lower bounding.
                    return pxs;
                }
        return null;
    }

    public static Point2D.Double getDoubleCentroid(LinkedList<Point> pixels_list)
    {
        double cx = 0.0;
        double cy = 0.0;

        for(Point p : pixels_list) {
            cx += (double) p.x;
            cy += (double) p.y;
        }

        cx /= pixels_list.size();
        cy /= pixels_list.size();

        return new Point2D.Double(cx+1.0, cy+1.0);
    }
    public static Point2D.Float getCentroid(LinkedList<Point> pixels_list)
    {
        float cx = 0.0f;
        float cy = 0.0f;

        for(Point p : pixels_list) {
            cx += (float) p.x;
            cy += (float) p.y;
        }

        cx /= pixels_list.size();
        cy /= pixels_list.size();

        return new Point2D.Float(cx+1, cy+1);
    }
}
