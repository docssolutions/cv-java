/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.filter.ImgPlusPrinter;

/** This plugin convolves images using user user defined kernels. */
public class Convolver {

    private int kw, kh;
    private boolean canceled;
    private float[] kernel;
    private boolean isLineRoi;
    private Button open, save;
    private boolean normalize = true;
    private int nSlices;
    // °J° private int flags = DOES_ALL|CONVERT_TO_FLOAT|SUPPORTS_MASKING|KEEP_PREVIEW|FINAL_PROCESSING|SNAPSHOT;
    private int nPasses = 1;
    private boolean kernelError;
    // °J° private PlugInFilterRunner pfr;
    // °J° private Thread mainThread;
    private int pass;

    
    static String kernelText = "-1 -1 -1 -1 -1\n-1 -1 -1 -1 -1\n-1 -1 24 -1 -1\n-1 -1 -1 -1 -1\n-1 -1 -1 -1 -1\n";
    static boolean normalizeFlag = true;
    static boolean debug = false;

    /** Convolves <code>ip</code> with a kernel of width <code>kw</code> and
        height <code>kh</code>. Returns false if the user cancels the operation. */
    public float[] convolve(float[] ip, int w, int h, float[] kernel, int kw, int kh) {
        float[] result;
        if (canceled || kw*kh!=kernel.length) return null;
        if ((kw&1)!=1 || (kh&1)!=1)
            throw new IllegalArgumentException("Kernel width or height not odd ("+kw+"x"+kh+")");
        // °J° boolean notFloat = !(ip instanceof FloatProcessor);
        // °J° if (notFloat) {
            // °J° if (ip2 instanceof ColorProcessor) 
                // °J° throw new IllegalArgumentException("RGB images not supported");
            // °J° ip2 = ip2.convertToFloat();
        // °J° }
        // °J° if (kw==1 || kh==1)
            // °J° result  = convolveFloat1D((FloatProcessor)ip2, kernel, kw, kh, normalize?getScale(kernel):1.0);
        // °J° else
            result  = convolveFloat(ip, w, h, kernel, kw, kh);
        // °J° if (notFloat) {
            // °J° if (ip instanceof ByteProcessor)
                // °J° ip2 = ip2.convertToByte(false);
            // °J° else
                // °J° ip2 = ip2.convertToShort(false);
            // °J° ip.setPixels(ip2.getPixels());
        // °J° }
        return result;
    }
    
    /** If 'normalize' is true (the default), the convolve(), convolveFloat() and
        convolveFloat1D() (4 argument version) methods divide each kernel
        coefficient by the sum of the coefficients, preserving image brightness. */
    public void setNormalize(boolean normalizeKernel) {
        normalize = normalizeKernel;
    }
     
    /** Convolves the float image <code>ip</code> with a kernel of width 
        <code>kw</code> and height <code>kh</code>. Returns false if 
        the user cancels the operation by pressing 'Esc'. */
    public float[] convolveFloat(float[] ip, int width, int height, float[] kernel, int kw, int kh) {
        int x1 = 0;
        int y1 = 0;
        int x2 = x1 + width;
        int y2 = y1 + height;
        int uc = kw/2;    
        int vc = kh/2;
        float[] pixels = ip.clone();
        float[] pixels2 = ip.clone();
        double scale = normalize?getScale(kernel):1.0;
        double sum;
        int offset, i;
        boolean edgePixel;
        int xedge = width-uc;
        int yedge = height-vc;
        for (int y=y1; y<y2; y++) {
            for (int x=x1; x<x2; x++) {
                if (canceled) return null;
                sum = 0.0;
                i = 0;
                edgePixel = y<vc || y>=yedge || x<uc || x>=xedge;
                if((x < 4)&&(y < 4)&&(debug)) System.out.println("("+x+", "+y+"): "+getPixel(x, y, pixels2, width, height));
                for (int v= vc; v >= -vc; v--) {
                    offset = x+(y+v)*width;
                    for(int u = uc; u >= -uc; u--) {
                        if (edgePixel) {
                            if (i>=kernel.length) // work around for JIT compiler bug on Linux
                                System.out.println("kernel index error: "+i);
                            // °J° if((x < 4)&&(y < 4)) System.out.print(String.format("(%3d * %3d)", Math.round(getPixel(x+u, y+v, pixels2, width, height)), Math.round(kernel[i])));
                            if((x < 4)&&(y < 4)&&(debug)) System.out.print(String.format("(%3.1f * %3.1f)", getPixel(x+u, y+v, pixels2, width, height), kernel[i]));
                            if ( ((y + v >= 0) && (y + v <= height - 1)) && ((u + x >= 0) && (u + x <= width - 1)) )
								sum += getPixel(x+u, y+v, pixels2, width, height) * kernel[i++];
							else {
								sum += 0;
								i++;
							}
                        } else{
                            // °J° if((x < 4)&&(y < 4)) System.out.print(String.format("(%3d * %3d)", Math.round(pixels2[offset+u]), Math.round(kernel[i])));
                            if((x < 4)&&(y < 4)&&(debug)) System.out.print(String.format("(%3.1f * %3.1f)", pixels2[offset+u], kernel[i]));
                            sum += pixels2[offset+u] * kernel[i++];
                        }
                        if((x < 4)&&(y < 4)&&(u != -uc)&&(debug)) System.out.print(" + ");
                    }
                    if((x < 4)&&(y < 4)&&(v != -vc)&&(debug)) System.out.println();
                }
                if((x < 4)&&(y < 4)&&(debug)) System.out.println();
                float real = (float)(sum*scale);
                // °J° real = (real >= 0)? real: 0;
                // °J° real = (real <= 255)? real: 255;
                pixels[x+y*width] = real;
                if((x < 4)&&(y < 4)&&(debug)) System.out.println(String.format(" = %3d * %.3f = %f\n", Math.round(sum), scale, real));
                // °J° if((x < 4)&&(y < 4)) System.out.println("("+x+", "+y+"): "+getPixel(x, y, pixels, width, height));
            }
        }
        return pixels;
     }

    /** Convolves the float image <code>ip</code> with a kernel of width 
        <code>kw</code> and height <code>kh</code>. Returns false if 
        the user cancels the operation by pressing 'Esc'. */
    public float[] convolveFloat(float[] ip, int width, int height, double[] kernel, int kw, int kh) {
        // °J° if (debug) {
            // °J° System.out.println(String.format("convolveFloat %d, %d", kw, kh));
            // °J° ImgPlusPrinter.print_img_pixels_F(kernel, kw, kh);
        // °J° }
        int x1 = 0;
        int y1 = 0;
        int x2 = x1 + width;
        int y2 = y1 + height;
        int uc = kw/2;    
        int vc = kh/2;
        float[] pixels = ip.clone();
        float[] pixels2 = ip.clone();
        double scale = normalize?getScale(kernel):1.0;
        double sum;
        int offset, i;
        boolean edgePixel;
        int xedge = width-uc;
        int yedge = height-vc;
        int total_pixels = (y2-y1) * (x2-x1);
        int processed = 0;
        float processed_percentage = 0.0f;
        for (int y=y1; y<y2; y++) {
            //CVUtils.println(":: En y="+y);
            for (int x=x1; x<x2; x++) {
                if (((int) processed_percentage) < processed/total_pixels )
                    CVUtils.println(":: Image processed: "+processed_percentage+"%");
                processed++;
                processed_percentage = ((float)processed)/total_pixels;
                if (canceled) return null;
                sum = 0.0;
                i = 0;
                edgePixel = y<vc || y>=yedge || x<uc || x>=xedge;
                if((x < 4)&&(y < 4)&&(debug)) System.out.println("("+x+", "+y+"): "+getPixel(x, y, pixels2, width, height));
                for (int v= vc; v >= -vc; v--) {
                    //CVUtils.println(":: En v="+v);
                    offset = x+(y+v)*width;
                    for(int u = uc; u >= -uc; u--) {
                        //CVUtils.println(":: En u="+u);
                        if (edgePixel) {
                            if (i>=kernel.length) // work around for JIT compiler bug on Linux
                                System.out.println("kernel index error: "+i);
                            if((x < 4)&&(y < 4)&&(debug)) System.out.print(String.format("(%3.1f * %3.1f)", getPixel(x+u, y+v, pixels2, width, height), kernel[i]));
                            if ( ((y + v >= 0) && (y + v <= height - 1)) && ((u + x >= 0) && (u + x <= width - 1)) )
								sum += getPixel(x+u, y+v, pixels2, width, height) * kernel[i++];
							else {
								sum += 0;
								i++;
							}
                        } else{
                            // °J° if((x < 4)&&(y < 4)) System.out.print(String.format("(%3d * %3d)", Math.round(pixels2[offset+u]), Math.round(kernel[i])));
                            if((x < 4)&&(y < 4)&&(debug)) System.out.print(String.format("(%3.1f * %3.1f)", pixels2[offset+u], kernel[i]));
                            sum += pixels2[offset+u] * kernel[i++];
                        }
                        if((x < 4)&&(y < 4)&&(u != -uc)&&(debug)) System.out.print(" + ");
                    }
                    if((x < 4)&&(y < 4)&&(v != -vc)&&(debug)) System.out.println();
                }
                if((x < 4)&&(y < 4)&&(debug)) System.out.println();
                float real = (float)(sum*scale);
                // °J° real = (real >= 0)? real: 0;
                // °J° real = (real <= 255)? real: 255;
                pixels[x+y*width] = real;
                if((x < 4)&&(y < 4)&&(debug)) System.out.println(String.format(" = %3d * %.3f = %f\n", Math.round(sum), scale, real));
                // °J° if((x < 4)&&(y < 4)) System.out.println("("+x+", "+y+"): "+getPixel(x, y, pixels, width, height));
            }
        }
        return pixels;
     }

    /** Convolves the float image <code>ip</code> with a kernel of width 
        <code>kw</code> and height <code>kh</code>. Returns false if 
        the user cancels the operation by pressing 'Esc'. */

    /**
     * Convolve image with circulary symmetrical kernel. It decomposes the kernel in a single vector that represents the
     * hole kernel. Now, with this vector the convolution takes to steps. First, it convolves only rows with the vector and
     * and then with columns in separate iterations. So complexity drops from O(M^2*N^2) to O(M^2*N) where M is the size of
     * the largest side of <code>mx</code> and N is the size of the largest side of <code>ckernel</code>.
     * References:
     * - {@url http://www.dspguide.com/ch24/3.htm}
     * @param mx Matrix of float values that represents an image.
     * @param ckernel A circulary symmetrical kernel.
     * @return A matrix with the application of the convolution.
     * @author Joshua E. Mendoza Mendieta
     */
    public float[] convolve(float[] ip, float[] wk_ip,int width, int height, float[] kernel, int kw, int kh) {
        assert ip != wk_ip; //, "ip and wk_ip must not be the same matrix";
        int x1 = 0;
        int y1 = 0;
        int x2 = x1 + width;
        int y2 = y1 + height;
        int uc = kw/2;    
        int vc = kh/2;
        double scale = normalize?getScale(kernel):1.0;
        double sum;
        int offset, i;
        boolean edgePixel;
        int xedge = width-uc;
        int yedge = height-vc;
        for (int y=y1; y<y2; y++) {
            for (int x=x1; x<x2; x++) {
                if (canceled) return null;
                sum = 0.0;
                i = 0;
                edgePixel = y<vc || y>=yedge || x<uc || x>=xedge;
                if((x < 4)&&(y < 4)&&(debug)) System.out.println("("+x+", "+y+"): "+getPixel(x, y, ip, width, height));
                for (int v= vc; v >= -vc; v--) {
                    offset = x+(y+v)*width;
                    for(int u = uc; u >= -uc; u--) {
                        if (edgePixel) {
                            if (i>=kernel.length) // work around for JIT compiler bug on Linux
                                System.out.println("kernel index error: "+i);
                            // °J° if((x < 4)&&(y < 4)) System.out.print(String.format("(%3d * %3d)", Math.round(getPixel(x+u, y+v, ip, width, height)), Math.round(kernel[i])));
                            if((x < 4)&&(y < 4)&&(debug)) System.out.print(String.format("(%3.1f * %3.1f)", getPixel(x+u, y+v, ip, width, height), kernel[i]));
                            if ( ((y + v >= 0) && (y + v <= height - 1)) && ((u + x >= 0) && (u + x <= width - 1)) )
								sum += getPixel(x+u, y+v, ip, width, height) * kernel[i++];
							else {
								sum += 0;
								i++;
							}
                        } else{
                            // °J° if((x < 4)&&(y < 4)) System.out.print(String.format("(%3d * %3d)", Math.round(ip[offset+u]), Math.round(kernel[i])));
                            if((x < 4)&&(y < 4)&&(debug)) System.out.print(String.format("(%3.1f * %3.1f)", ip[offset+u], kernel[i]));
                            sum += ip[offset+u] * kernel[i++];
                        }
                        if((x < 4)&&(y < 4)&&(u != -uc)&&(debug)) System.out.print(" + ");
                    }
                    if((x < 4)&&(y < 4)&&(v != -vc)&&(debug)) System.out.println();
                }
                if((x < 4)&&(y < 4)&&(debug)) System.out.println();
                float real = (float)(sum*scale);
                // °J° real = (real >= 0)? real: 0;
                // °J° real = (real <= 255)? real: 255;
                wk_ip[x+y*width] = real;
                if((x < 4)&&(y < 4)&&(debug)) System.out.println(String.format(" = %3d * %.3f = %f\n", Math.round(sum), scale, real));
                // °J° if((x < 4)&&(y < 4)) System.out.println("("+x+", "+y+"): "+getPixel(x, y, pixels, width, height));
            }
        }
        return wk_ip;
     }

    /**
     * Convolve image with 8 3x3 kernels.
     * References:
     * @return A matrix with the application of all the convolutions.
     * @author Joshua E. Mendoza Mendieta
     */
    public float[][] convolveMultipleKernels(float[] ip, float[][] wk_ips, int m, int _n, float[][] ks, int km, int kn) {
        int nm = _n * m;
        int i = 0, j = 0;
        int m_1 = m-1, n_1 = _n-1, m_xn_1 = m * (_n-1);
        int n, ne, e, se, s, sw, w, nw;
        n = ne = e = se = s = sw = w = nw = 0;

        float[] wkN  = wk_ips[0];
        float[] wkS  = wk_ips[1];
        float[] wkE  = wk_ips[2];
        float[] wkW  = wk_ips[3];
        float[] wkNE = wk_ips[4];
        float[] wkSE = wk_ips[5];
        float[] wkSW = wk_ips[6];
        float[] wkNW = wk_ips[7];

        float[] kN  = ks[0];
        float[] kS  = ks[1];
        float[] kE  = ks[2];
        float[] kW  = ks[3];
        float[] kNE = ks[4];
        float[] kSE = ks[5];
        float[] kSW = ks[6];
        float[] kNW = ks[7];

        float ip_c  = 0.0f;
        float ip_n  = 0.0f;
        float ip_s  = 0.0f;
        float ip_e  = 0.0f;
        float ip_w  = 0.0f;
        float ip_ne = 0.0f;
        float ip_se = 0.0f;
        float ip_sw = 0.0f;
        float ip_nw = 0.0f;

        for(i=m+1;i<nm-m-1;i++) {
            if(i%m == m_1) { continue; }

            ip_c  = ip[i];
            ip_n  = ip[i - m];
            ip_e  = ip[i + 1];
            ip_s  = ip[i + m];
            ip_w  = ip[i - 1];
            ip_ne = ip[i - m + 1];
            ip_se = ip[i + m + 1];
            ip_sw = ip[i + m - 1];
            ip_nw = ip[i - m - 1];

            // Not in the border
            wkN[i]  = ip_c*kN[4] + ip_n*kN[1] + ip_ne*kN[2] + ip_e*kN[5] + ip_se*kN[8] + ip_s*kN[7] + ip_sw*kN[6] + ip_w*kN[3] + ip_nw*kN[0];
            wkE[i]  = ip_c*kE[4] + ip_n*kE[1] + ip_ne*kE[2] + ip_e*kE[5] + ip_se*kE[8] + ip_s*kE[7] + ip_sw*kE[6] + ip_w*kE[3] + ip_nw*kE[0];
            wkS[i]  = ip_c*kS[4] + ip_n*kS[1] + ip_ne*kS[2] + ip_e*kS[5] + ip_se*kS[8] + ip_s*kS[7] + ip_sw*kS[6] + ip_w*kS[3] + ip_nw*kS[0];
            wkW[i]  = ip_c*kW[4] + ip_n*kW[1] + ip_ne*kW[2] + ip_e*kW[5] + ip_se*kW[8] + ip_s*kW[7] + ip_sw*kW[6] + ip_w*kW[3] + ip_nw*kW[0];
            wkNE[i] = ip_c*kNE[4] + ip_n*kNE[1] + ip_ne*kNE[2] + ip_e*kNE[5] + ip_se*kNE[8] + ip_s*kNE[7] + ip_sw*kNE[6] + ip_w*kNE[3] + ip_nw*kNE[0];
            wkSE[i] = ip_c*kSE[4] + ip_n*kSE[1] + ip_ne*kSE[2] + ip_e*kSE[5] + ip_se*kSE[8] + ip_s*kSE[7] + ip_sw*kSE[6] + ip_w*kSE[3] + ip_nw*kSE[0];
            wkSW[i] = ip_c*kSW[4] + ip_n*kSW[1] + ip_ne*kSW[2] + ip_e*kSW[5] + ip_se*kSW[8] + ip_s*kSW[7] + ip_sw*kSW[6] + ip_w*kSW[3] + ip_nw*kSW[0];
            wkNW[i] = ip_c*kNW[4] + ip_n*kNW[1] + ip_ne*kNW[2] + ip_e*kNW[5] + ip_se*kNW[8] + ip_s*kNW[7] + ip_sw*kNW[6] + ip_w*kNW[3] + ip_nw*kNW[0];
        }

        // Northen border
        for(i=1;i<m_1;i++) {
            ip_c  = ip[i];
            ip_e  = ip[i + 1];
            ip_s  = ip[i + m];
            ip_w  = ip[i - 1];
            ip_se = ip[i + m + 1];
            ip_sw = ip[i + m - 1];

            wkN[i]  = ip_c*kN[4] + ip_e*kN[5] + ip_se*kN[8] + ip_s*kN[7] + ip_sw*kN[6] + ip_w*kN[3];
            wkE[i]  = ip_c*kE[4] + ip_e*kE[5] + ip_se*kE[8] + ip_s*kE[7] + ip_sw*kE[6] + ip_w*kE[3];
            wkS[i]  = ip_c*kS[4] + ip_e*kS[5] + ip_se*kS[8] + ip_s*kS[7] + ip_sw*kS[6] + ip_w*kS[3];
            wkW[i]  = ip_c*kW[4] + ip_e*kW[5] + ip_se*kW[8] + ip_s*kW[7] + ip_sw*kW[6] + ip_w*kW[3];
            wkNE[i] = ip_c*kNE[4] + ip_e*kNE[5] + ip_se*kNE[8] + ip_s*kNE[7] + ip_sw*kNE[6] + ip_w*kNE[3];
            wkSE[i] = ip_c*kSE[4] + ip_e*kSE[5] + ip_se*kSE[8] + ip_s*kSE[7] + ip_sw*kSE[6] + ip_w*kSE[3];
            wkSW[i] = ip_c*kSW[4] + ip_e*kSW[5] + ip_se*kSW[8] + ip_s*kSW[7] + ip_sw*kSW[6] + ip_w*kSW[3];
            wkNW[i] = ip_c*kNW[4] + ip_e*kNW[5] + ip_se*kNW[8] + ip_s*kNW[7] + ip_sw*kNW[6] + ip_w*kNW[3];
        }

        // Southern border
        for(j=1;j<m_1;j++) {
            i = j + m_xn_1;
            ip_c  = ip[i];
            ip_n  = ip[i - m];
            ip_e  = ip[i + 1];
            ip_w  = ip[i - 1];
            ip_ne = ip[i - m + 1];
            ip_nw = ip[i - m - 1];

            wkN[i]  = ip_c*kN[4] + ip_n*kN[1] + ip_ne*kN[2] + ip_e*kN[5] + ip_w*kN[3] + ip_nw*kN[0];
            wkE[i]  = ip_c*kE[4] + ip_n*kE[1] + ip_ne*kE[2] + ip_e*kE[5] + ip_w*kE[3] + ip_nw*kE[0];
            wkS[i]  = ip_c*kS[4] + ip_n*kS[1] + ip_ne*kS[2] + ip_e*kS[5] + ip_w*kS[3] + ip_nw*kS[0];
            wkW[i]  = ip_c*kW[4] + ip_n*kW[1] + ip_ne*kW[2] + ip_e*kW[5] + ip_w*kW[3] + ip_nw*kW[0];
            wkNE[i] = ip_c*kNE[4] + ip_n*kNE[1] + ip_ne*kNE[2] + ip_e*kNE[5] + ip_w*kNE[3] + ip_nw*kNE[0];
            wkSE[i] = ip_c*kSE[4] + ip_n*kSE[1] + ip_ne*kSE[2] + ip_e*kSE[5] + ip_w*kSE[3] + ip_nw*kSE[0];
            wkSW[i] = ip_c*kSW[4] + ip_n*kSW[1] + ip_ne*kSW[2] + ip_e*kSW[5] + ip_w*kSW[3] + ip_nw*kSW[0];
            wkNW[i] = ip_c*kNW[4] + ip_n*kNW[1] + ip_ne*kNW[2] + ip_e*kNW[5] + ip_w*kNW[3] + ip_nw*kNW[0];
        }

        // Western border
        for(j=1;j<n_1;j++) {
            i = j * m;
            ip_c  = ip[i];
            ip_n  = ip[i - m];
            ip_e  = ip[i + 1];
            ip_s  = ip[i + m];
            ip_ne = ip[i - m + 1];
            ip_se = ip[i + m + 1];

            wkN[i]  = ip_c*kN[4] + ip_n*kN[1] + ip_ne*kN[2] + ip_e*kN[5] + ip_se*kN[8] + ip_s*kN[7];
            wkE[i]  = ip_c*kE[4] + ip_n*kE[1] + ip_ne*kE[2] + ip_e*kE[5] + ip_se*kE[8] + ip_s*kE[7];
            wkS[i]  = ip_c*kS[4] + ip_n*kS[1] + ip_ne*kS[2] + ip_e*kS[5] + ip_se*kS[8] + ip_s*kS[7];
            wkW[i]  = ip_c*kW[4] + ip_n*kW[1] + ip_ne*kW[2] + ip_e*kW[5] + ip_se*kW[8] + ip_s*kW[7];
            wkNE[i] = ip_c*kNE[4] + ip_n*kNE[1] + ip_ne*kNE[2] + ip_e*kNE[5] + ip_se*kNE[8] + ip_s*kNE[7];
            wkSE[i] = ip_c*kSE[4] + ip_n*kSE[1] + ip_ne*kSE[2] + ip_e*kSE[5] + ip_se*kSE[8] + ip_s*kSE[7];
            wkSW[i] = ip_c*kSW[4] + ip_n*kSW[1] + ip_ne*kSW[2] + ip_e*kSW[5] + ip_se*kSW[8] + ip_s*kSW[7];
            wkNW[i] = ip_c*kNW[4] + ip_n*kNW[1] + ip_ne*kNW[2] + ip_e*kNW[5] + ip_se*kNW[8] + ip_s*kNW[7];
        }

        // Eastern border
        for(j=1;j<n_1;j++) {
            i = (j+1) * m - 1;
            ip_c  = ip[i];
            ip_n  = ip[i - m];
            ip_s  = ip[i + m];
            ip_w  = ip[i - 1];
            ip_sw = ip[i + m - 1];
            ip_nw = ip[i - m - 1];

            wkN[i]  = ip_c*kN[4] + ip_n*kN[1] + ip_s*kN[7] + ip_sw*kN[6] + ip_w*kN[3] + ip_nw*kN[0];
            wkE[i]  = ip_c*kE[4] + ip_n*kE[1] + ip_s*kE[7] + ip_sw*kE[6] + ip_w*kE[3] + ip_nw*kE[0];
            wkS[i]  = ip_c*kS[4] + ip_n*kS[1] + ip_s*kS[7] + ip_sw*kS[6] + ip_w*kS[3] + ip_nw*kS[0];
            wkW[i]  = ip_c*kW[4] + ip_n*kW[1] + ip_s*kW[7] + ip_sw*kW[6] + ip_w*kW[3] + ip_nw*kW[0];
            wkNE[i] = ip_c*kNE[4] + ip_n*kNE[1] + ip_s*kNE[7] + ip_sw*kNE[6] + ip_w*kNE[3] + ip_nw*kNE[0];
            wkSE[i] = ip_c*kSE[4] + ip_n*kSE[1] + ip_s*kSE[7] + ip_sw*kSE[6] + ip_w*kSE[3] + ip_nw*kSE[0];
            wkSW[i] = ip_c*kSW[4] + ip_n*kSW[1] + ip_s*kSW[7] + ip_sw*kSW[6] + ip_w*kSW[3] + ip_nw*kSW[0];
            wkNW[i] = ip_c*kNW[4] + ip_n*kNW[1] + ip_s*kNW[7] + ip_sw*kNW[6] + ip_w*kNW[3] + ip_nw*kNW[0];
        }

        i = 0;
        ip_c  = ip[i];
        ip_e  = ip[i + 1];
        ip_se = ip[i + m + 1];
        ip_s  = ip[i + m];

        // Northweastern border
        wkN[i]  = ip_c*kN[4] + ip_e*kN[5] + ip_se*kN[8] + ip_s*kN[7];
        wkE[i]  = ip_c*kE[4] + ip_e*kE[5] + ip_se*kE[8] + ip_s*kE[7];
        wkS[i]  = ip_c*kS[4] + ip_e*kS[5] + ip_se*kS[8] + ip_s*kS[7];
        wkW[i]  = ip_c*kW[4] + ip_e*kW[5] + ip_se*kW[8] + ip_s*kW[7];
        wkNE[i] = ip_c*kNE[4] + ip_e*kNE[5] + ip_se*kNE[8] + ip_s*kNE[7];
        wkSE[i] = ip_c*kSE[4] + ip_e*kSE[5] + ip_se*kSE[8] + ip_s*kSE[7];
        wkSW[i] = ip_c*kSW[4] + ip_e*kSW[5] + ip_se*kSW[8] + ip_s*kSW[7];
        wkNW[i] = ip_c*kNW[4] + ip_e*kNW[5] + ip_se*kNW[8] + ip_s*kNW[7];

        i = m_1;
        ip_c  = ip[i];
        ip_s  = ip[i + m];
        ip_sw = ip[i + m - 1];
        ip_w  = ip[i - 1];

        // Northeastern border
        wkN[i]  = ip_c*kN[4] + ip_s*kN[7] + ip_sw*kN[6] + ip_w*kN[3];
        wkE[i]  = ip_c*kE[4] + ip_s*kE[7] + ip_sw*kE[6] + ip_w*kE[3];
        wkS[i]  = ip_c*kS[4] + ip_s*kS[7] + ip_sw*kS[6] + ip_w*kS[3];
        wkW[i]  = ip_c*kW[4] + ip_s*kW[7] + ip_sw*kW[6] + ip_w*kW[3];
        wkNE[i] = ip_c*kNE[4] + ip_s*kNE[7] + ip_sw*kNE[6] + ip_w*kNE[3];
        wkSE[i] = ip_c*kSE[4] + ip_s*kSE[7] + ip_sw*kSE[6] + ip_w*kSE[3];
        wkSW[i] = ip_c*kSW[4] + ip_s*kSW[7] + ip_sw*kSW[6] + ip_w*kSW[3];
        wkNW[i] = ip_c*kNW[4] + ip_s*kNW[7] + ip_sw*kNW[6] + ip_w*kNW[3];

        i = m * (_n-1);
        ip_c  = ip[i];
        ip_n  = ip[i - m];
        ip_ne = ip[i - m + 1];
        ip_e  = ip[i + 1];

        // Southwestern border
        wkN[i]  = ip_c*kN[4] + ip_n*kN[1] + ip_ne*kN[2] + ip_e*kN[5];
        wkE[i]  = ip_c*kE[4] + ip_n*kE[1] + ip_ne*kE[2] + ip_e*kE[5];
        wkS[i]  = ip_c*kS[4] + ip_n*kS[1] + ip_ne*kS[2] + ip_e*kS[5];
        wkW[i]  = ip_c*kW[4] + ip_n*kW[1] + ip_ne*kW[2] + ip_e*kW[5];
        wkNE[i] = ip_c*kNE[4] + ip_n*kNE[1] + ip_ne*kNE[2] + ip_e*kNE[5];
        wkSE[i] = ip_c*kSE[4] + ip_n*kSE[1] + ip_ne*kSE[2] + ip_e*kSE[5];
        wkSW[i] = ip_c*kSW[4] + ip_n*kSW[1] + ip_ne*kSW[2] + ip_e*kSW[5];
        wkNW[i] = ip_c*kNW[4] + ip_n*kNW[1] + ip_ne*kNW[2] + ip_e*kNW[5];

        i = nm - 1;
        ip_c  = ip[i];
        ip_n  = ip[i - m];
        ip_nw = ip[i - m - 1];
        ip_w  = ip[i - 1];

        // Southeastern border
        wkN[i]  = ip_c*kN[4] + ip_n*kN[1] + ip_w*kN[3] + ip_nw*kN[0];
        wkE[i]  = ip_c*kE[4] + ip_n*kE[1] + ip_w*kE[3] + ip_nw*kE[0];
        wkS[i]  = ip_c*kS[4] + ip_n*kS[1] + ip_w*kS[3] + ip_nw*kS[0];
        wkW[i]  = ip_c*kW[4] + ip_n*kW[1] + ip_w*kW[3] + ip_nw*kW[0];
        wkNE[i] = ip_c*kNE[4] + ip_n*kNE[1] + ip_w*kNE[3] + ip_nw*kNE[0];
        wkSE[i] = ip_c*kSE[4] + ip_n*kSE[1] + ip_w*kSE[3] + ip_nw*kSE[0];
        wkSW[i] = ip_c*kSW[4] + ip_n*kSW[1] + ip_w*kSW[3] + ip_nw*kSW[0];
        wkNW[i] = ip_c*kNW[4] + ip_n*kNW[1] + ip_w*kNW[3] + ip_nw*kNW[0];
        
        return wk_ips;
     }


    /**
     * Convolve image with circulary symmetrical kernel. It decomposes the kernel in a single vector that represents the
     * hole kernel. Now, with this vector the convolution takes to steps. First, it convolves only rows with the vector and
     * and then with columns in separate iterations. So complexity drops from O(M^2*N^2) to O(M^2*N) where M is the size of
     * the largest side of <code>mx</code> and N is the size of the largest side of <code>ckernel</code>.
     * References:
     * - {@url http://www.dspguide.com/ch24/3.htm}
     * @param mx Matrix of float values that represents an image.
     * @param ckernel A circulary symmetrical kernel.
     * @return A matrix with the application of the convolution.
     * @author Joshua E. Mendoza Mendieta
     */
    public float[][] convolveFloatWithCircularySymmetricalKernel(float[][] mx, float[][] ckernel)
    {
        //CVUtils.println(":: In convolve float matrix with circulary symmetrical kernel");
        float[] kc = FilterUtils.getComponentFromCircularySymmetricKernel(ckernel);

        int n = mx.length;
        int m = mx[0].length;
        int kn = kc.length;
        int c = kn / 2;
        int i, j, k;
        
        float[][] new_mx = new float[n][m];
        float[][] interm_mx = new float[n][m];
        float sum = 0.0f;

        //CVUtils.println(":: Main matrix of "+m+"x"+n+", kernel of "+kn+"x"+kn);
        //CVUtils.printMatrix(mx);

        // Convolving each column of original matrix into an new intermediate.
        //CVUtils.println(":: Calculating intermediate matrix...");
        for (i = 0; i < n; i++)
            for (j = 0; j < m; j++)
                for (k = 0; k < kn; k++)
                    if (0 <= j+k-c && j+k-c < m)
                        interm_mx[i][j] += mx[i][j+k-c] * kc[k];

        //CVUtils.printMatrix(interm_mx);

        // Convolving each row of intermediate matrix into the result matrix.
        //CVUtils.println(":: Calculating final matrix...");
        for (i = 0; i < n; i++)
            for (j = 0; j < m; j++)
                for (k = 0; k < kn; k++)
                    if (0 <= i+k-c && i+k-c < n)
                        new_mx[i][j] += interm_mx[i+k-c][j] * kc[k];

        //CVUtils.printMatrix(new_mx);

        return new_mx;
    }

    public static double getScale(float[] kernel) {
        double scale = 1.0;
        double sum = 0.0;
        for (int i=0; i<kernel.length; i++)
            sum += kernel[i];
        if (sum!=0.0)
            scale = 1.0/sum;
        return scale;
    }

     
    public static double getScale(double[] kernel) {
        double scale = 1.0;
        double sum = 0.0;
        for (int i=0; i<kernel.length; i++)
            sum += kernel[i];
        if (sum!=0.0)
            scale = 1.0/sum;
        return scale;
    }
    
    public static void adjust(float[] kernel) {
        // °J° double scale = 1.0;
        // °J° double sum = 0.0;
        for (int i=0; i<kernel.length; i++){
            // °J° sum += kernel[i];
            if (kernel[i] > 255)
                kernel[i] = 255;
            
            if (kernel[i] < 0)
                kernel[i] = 0;
            }
        // °J° return scale;
    }

    private float getPixel(int x, int y, float[] pixels, int width, int height) {
        if (x<=0) x = 0;
        if (x>=width) x = width-1;
        if (y<=0) y = 0;
        if (y>=height) y = height-1;
        return pixels[x+y*width];
    }
    
// °J° 
}

