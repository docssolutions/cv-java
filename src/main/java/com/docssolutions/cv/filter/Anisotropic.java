/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import java.util.*;
import java.util.concurrent.*;

import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.util.MatrixUtils;

public class Anisotropic  
{
    private float dx = 1;
    private float dy = 1;
    private float dd = 1.4142135623730951f;
    //private float dd = (float) Math.sqrt(2);

    private float c_dx = 1;
    private float c_dy = 1;
    private float c_dd = 0.5f;

    private float  kappa     = 10;
    private float  delta_t   = 0.142857f;

    private int       num_iter;
    private boolean   option;
    
    private float[][] hhs;
    private float[] hN  = {0, 1, 0, 0, -1, 0, 0, 0, 0};
    private float[] hS  = {0, 0, 0, 0, -1, 0, 0, 1, 0};
    private float[] hE  = {0, 0, 0, 0, -1, 1, 0, 0, 0};
    private float[] hW  = {0, 0, 0, 1, -1, 0, 0, 0, 0};
    private float[] hNE = {0, 0, 1, 0, -1, 0, 0, 0, 0};
    private float[] hSE = {0, 0, 0, 0, -1, 0, 0, 0, 1};
    private float[] hSW = {0, 0, 0, 0, -1, 0, 1, 0, 0};
    private float[] hNW = {1, 0, 0, 0, -1, 0, 0, 0, 0};
    
    Convolver myConv = new Convolver();
	
    public Anisotropic()
    {
        this(25, 0.1429f, 10, true);
    }

    public Anisotropic(int num_iter, float delta_t, float kappa, boolean option) 
	{
        this(num_iter, delta_t, kappa, option, 1, 1);
	}

    public Anisotropic(int num_iter, float delta_t, float kappa, boolean option, float dx, float dy) 
	{
        this.num_iter = num_iter;
        this.delta_t = delta_t;
        this.kappa = kappa;
        this.option = option;
        initKernels();
        setDeltas(dx, dy);
	}

    public void setDeltas(float dx, float dy) {
        this.dx = dx;
        this.dy = dy;
        dd = (float) Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

        c_dx = (float) (1.0 / Math.pow(dx, 2));
        c_dy = (float) (1.0 / Math.pow(dy, 2));
        c_dd = (float) (1.0 / Math.pow(dd, 2));
    }

    private void initKernels()
    {
        hhs = new float[8][];
        hhs[0] = hN;
        hhs[1] = hS;
        hhs[2] = hW;
        hhs[3] = hE;
        hhs[4] = hNE;
        hhs[5] = hSE;
        hhs[6] = hSW;
        hhs[7] = hNW;
    }

	public float[] applyAnisotropicDiffusionArray(float[] im_diff, int width, int height)
    {
        // Finite differences. 
        float[][] nablas = new float[8][];
        float[] nablaN, nablaS, nablaW, nablaE, nablaNE, nablaSE, nablaSW, nablaNW; 
        nablas[0] = nablaN  = new float[im_diff.length];
        nablas[1] = nablaS  = new float[im_diff.length];
        nablas[2] = nablaW  = new float[im_diff.length];
        nablas[3] = nablaE  = new float[im_diff.length];
        nablas[4] = nablaNE = new float[im_diff.length];
        nablas[5] = nablaSE = new float[im_diff.length];
        nablas[6] = nablaSW = new float[im_diff.length];
        nablas[7] = nablaNW = new float[im_diff.length];
        
        float[][] ccs = new float[8][];
        float[] cN, cS, cW, cE, cNE, cSE, cSW, cNW;
        ccs[0] = cN  = new float[im_diff.length];
        ccs[1] = cS  = new float[im_diff.length];
        ccs[2] = cW  = new float[im_diff.length];
        ccs[3] = cE  = new float[im_diff.length];
        ccs[4] = cNE = new float[im_diff.length];
        ccs[5] = cSE = new float[im_diff.length];
        ccs[6] = cSW = new float[im_diff.length];
        ccs[7] = cNW = new float[im_diff.length];
        
        float[] result, temp;
        result = new float[im_diff.length];
        temp   = new float[im_diff.length];

        Date d1, d2;
        long duration;

        int nm = width * height;
        
        for (int i = 0; i < num_iter; i++) {
			CVUtils.println("Iter: "+i);

            //CVUtils.print(":: Applying optimized convolutions... ");
            d1 = new Date();
            myConv.convolveMultipleKernels(im_diff, nablas, width, height, hhs, 3, 3); // High-rate errors...
			//myConv.convolve(im_diff, nablaN, width, height, hN,3,3);
			//myConv.convolve(im_diff, nablaS, width, height,hS,3,3);   
			//myConv.convolve(im_diff, nablaW, width, height,hW,3,3);
			//myConv.convolve(im_diff, nablaE, width, height,hE,3,3);   
			//myConv.convolve(im_diff, nablaNE, width, height,hNE,3,3);
			//myConv.convolve(im_diff, nablaSE, width, height,hSE,3,3);   
			//myConv.convolve(im_diff, nablaSW, width, height,hSW,3,3);
			//myConv.convolve(im_diff, nablaNW, width, height,hNW,3,3); 
            d2 = new Date();
            duration = d2.getTime() - d1.getTime();
            //CVUtils.println("(lasted for "+duration+"ns)");

            //CVUtils.println(":: Applying diffusions... ");
            d1 = new Date();
            applyMultithreadedDiffusion(nablas, ccs, option);
            //applyDiffusion(nablaN, cN, option);
            //applyDiffusion(nablaS, cS, option);
            //applyDiffusion(nablaW, cW, option);
            //applyDiffusion(nablaE, cE, option);
            //applyDiffusion(nablaNE, cNE, option);
            //applyDiffusion(nablaSE, cSE, option);
            //applyDiffusion(nablaSW, cSW, option);
            //applyDiffusion(nablaNW, cNW, option);
            d2 = new Date();
            duration = d2.getTime() - d1.getTime();
            //CVUtils.println("(lasted for "+duration+"ns)");
			
			//  (1/(dy^2))*cN.*nablaN + (1/(dy^2))*cS.*nablaS
            //CVUtils.print(":: Calculating differences... ");
            d1 = new Date();
            for (int j = 0; j < nm; j++) {
                im_diff[j] += delta_t *
                    (c_dy*cN[j]*nablaN[j] + c_dy*cS[j]*nablaS[j] +
                     c_dx*cW[j]*nablaW[j] + c_dx*cE[j]*nablaE[j] +
                     c_dd*cNE[j]*nablaNE[j] + c_dd*cSE[j]*nablaSE[j] +
                     c_dd*cSW[j]*nablaSW[j] + c_dd*cNW[j]*nablaNW[j]);
            }
            //MatrixUtils.timesMatEW(cN, temp, nablaN);
			//MatrixUtils.timesScalarEW(temp, result, c_dy);
			//
			//MatrixUtils.timesMatEW(cS, temp, nablaS);
			//MatrixUtils.timesScalarEW(temp, temp, c_dy);
			//MatrixUtils.addMatEW(result, result, temp);
			//
			//MatrixUtils.timesMatEW(cW, temp, nablaW);
			//MatrixUtils.timesScalarEW(temp, temp, c_dx);
			//MatrixUtils.addMatEW(result, result, temp);
			//
			//MatrixUtils.timesMatEW(cE, temp, nablaE);
			//MatrixUtils.timesScalarEW(temp, temp, c_dx);
			//MatrixUtils.addMatEW(result, result, temp);
			//
			//MatrixUtils.timesMatEW(cNE, temp, nablaNE);
			//MatrixUtils.timesScalarEW(temp, temp, c_dd);
			//MatrixUtils.addMatEW(result, result, temp);
			//
			//MatrixUtils.timesMatEW(cSE, temp, nablaSE);
			//MatrixUtils.timesScalarEW(temp, temp, c_dd);
			//MatrixUtils.addMatEW(result, result, temp);
			//
			//MatrixUtils.timesMatEW(cSW, temp, nablaSW);
			//MatrixUtils.timesScalarEW(temp, temp, c_dd);
			//MatrixUtils.addMatEW(result, result, temp);
			//
			//MatrixUtils.timesMatEW(cNW, temp, nablaNW);
			//MatrixUtils.timesScalarEW(temp, temp, c_dd);
			//MatrixUtils.addMatEW(result, result, temp);
			//
			//MatrixUtils.timesScalarEW(result, result, delta_t);
			//MatrixUtils.addMatEW(im_diff, im_diff, result);
            d2 = new Date();
            duration = d2.getTime() - d1.getTime();
            //CVUtils.println("(lasted for "+duration+"ns)");
		}
		
		return im_diff;
	}

    public float[][] applyRawAnisotropicFilter(float[][] mx)
    {
        int n = mx.length;
        int m = mx[0].length;
        float[] im_diff = CVUtils.transformMatrixFrom2To1D(mx);
        float[] new_im_diff = applyAnisotropicDiffusionArray(im_diff, m, n);
        float[][] new_mx = CVUtils.transformMatrixFrom1To2D(new_im_diff, m, n);

		return new_mx;
	}

    private float[] applyDiffusion(float[] nabla, boolean option)
    {
        int n = nabla.length;
        float[] diffusion = new float[n];
        return applyDiffusion(nabla, diffusion, option);
    }
    
    private float[] applyDiffusion(float[] nabla, float[] diffusion, boolean option)
    {
        int nm = diffusion.length;
        if (option) {
            // Caso para exp(-(nablaS/kappa).^2)
            for(int i=0;i<nm;i++) {
                diffusion[i] = (float) Math.exp(-Math.pow(nabla[i]/kappa, 2));
            }
            //diffusion = MatrixUtils.divEW(nabla, diffusion, kappa);
            //diffusion = MatrixUtils.powEW(diffusion, diffusion, 2);
            //diffusion = MatrixUtils.timesScalarEW(diffusion, diffusion, -1);
            //diffusion = MatrixUtils.expEW(diffusion, diffusion);
            //diffusion = CVUtils.divEW(nabla, kappa);
            //diffusion = CVUtils.powEW(diffusion, 2);
            //diffusion = CVUtils.timesScalarEW(diffusion, -1);
            //diffusion = CVUtils.expEW(diffusion);
        } else {
            // Caso para diffusion = 1./(1 + (nablaN/kappa).^2)
            diffusion = MatrixUtils.divEW(nabla, diffusion, kappa);
            diffusion = MatrixUtils.powEW(diffusion, diffusion, 2);
            diffusion = MatrixUtils.addEW(diffusion, diffusion, 1);
            diffusion = MatrixUtils.divInvEW(diffusion, diffusion, 1);
            //diffusion = CVUtils.divEW(nabla, kappa);
            //diffusion = CVUtils.powEW(diffusion, 2);
            //diffusion = CVUtils.addEW(diffusion, 1);
            //diffusion = CVUtils.divInvEW(diffusion, 1);
        }
        return diffusion;
    }

    /**
     * Diffusion application to the 8 matrices given with 8 different threads.
     * References:
     * - {@url http://www.vogella.com/tutorials/JavaConcurrency/article.html}
     * - {@url http://docs.oracle.com/javase/tutorial/essential/concurrency/exinter.html}
     * - {@url http://howtodoinjava.com/2014/05/27/forkjoin-framework-tutorial-forkjoinpool-example/}
     * @see java.util.concurrent.ExecutorService
     * @author Joshua E. Mendoza Mendieta
     */

    private float[][] applyMultithreadedDiffusion(float[][] nablas, float[][] diffusions, boolean option)
    {
        int c_n = diffusions.length;
        int nm = diffusions[0].length;

        if (option) {
            // Caso para exp(-(nablaS/kappa).^2)
            ExecutorService executor = Executors.newFixedThreadPool(CVUtils.NUM_THREADS);
            for (int t = 0; t < 8; t++) {
                ConcurrentDiffusionApplication cda = new ConcurrentDiffusionApplication(t+1, diffusions[t], nablas[t], kappa);
                executor.execute(cda);
            }
            try {
                executor.shutdown();
                executor.awaitTermination(20000, TimeUnit.SECONDS);
            } catch (InterruptedException ie) {
                // (Re-)Cancel if current thread also interrupted
                executor.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
            //CVUtils.println("All diffusions applied.");
        } else {
            for (int c = 0; c < c_n; c++) {
                diffusions[c] = MatrixUtils.divEW(nablas[c], diffusions[c], kappa);
                diffusions[c] = MatrixUtils.powEW(diffusions[c], diffusions[c], 2);
                diffusions[c] = MatrixUtils.addEW(diffusions[c], diffusions[c], 1);
                diffusions[c] = MatrixUtils.divInvEW(diffusions[c], diffusions[c], 1);
            }
        }
        return diffusions;
    }

    private class ConcurrentDiffusionApplication implements Runnable {
        private int id;
        private float[] diffusion;
        private float[] nabla;
        private float kappa;

        ConcurrentDiffusionApplication(int id, float[] diffusion, float[] nabla, float kappa) {
            this.id = id;
            this.diffusion = diffusion;
            this.nabla = nabla;
            this.kappa = kappa;
        }

        @Override
        public void run() {
            //CVUtils.println(":: T("+id+"): Applying diffusion...");
            int nm = diffusion.length;
            for (int i = 0; i < nm; i++)
                diffusion[i] = (float) Math.exp(-Math.pow(nabla[i]/kappa, 2));
            //CVUtils.println(":: T("+id+"): Application finished.");
        }
    }
}
