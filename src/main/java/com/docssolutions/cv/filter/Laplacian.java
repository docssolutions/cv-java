/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import com.docssolutions.cv.CVUtils;

public class Laplacian
{
    static int kw            = 5;
    static int kh            = 5;
    static double sigma      = 0.5;
    static double sigma2     = Math.pow(sigma,2);
	static boolean sstats    = false; // display xdt value in each iteration
	static boolean tstats    = false; // measure needed runtime
    // °J° y = exp((-x.*x)/(2*((0.5)^2)))
    
		
    Convolver myConv = new Convolver();

    public Laplacian() {this(25,25,0.5);}
	   
    public Laplacian(int kw, int kh, double sigma) 
	{
        this.kw = kw;
        this.kh = kh;
        this.sigma = sigma;
        this.sigma2 = Math.pow(sigma,2);
	}

	public float[][] applyRawLaplacianFilter(float[][] mx) 
	{
        int n = mx.length;
        int m = mx[0].length;
        int i, j = 0;

        double sum = 0.0, prod = kh * kw, max = 0.0;
        int w = (kw-1)/2;
        int h = (kh-1)/2;
        double[][] kernel = new double[kh][kw]; // kh is the number of rows. kw is the number of cols.
        float[] mx_1d;
        double[] kernel_1d;
        
        // Meshgrid with Gaussian
        for(i=0;i<kh;i++)
            for(j=0;j<kw;j++)
                kernel[i][j] = (double) Math.exp(-((Math.pow(i-h,2)+Math.pow(j-w,2))/(2*sigma2)));

        //printKernel(kernel, kw, kh, "just after calculating gaussian");
        
        kernel = CVUtils.applyRawThresholdInLower(kernel, 2.2204e-16 * CVUtils.max_of(kernel));

        //printKernel(kernel, kw, kh, "just after thresholding with eps");

        sum = CVUtils.sum_of(kernel);

        if(sum != 0.0) 
            for(i=0;i<kh;i++)
                for(j=0;j<kw;j++)
                    kernel[i][j] = kernel[i][j] / ((double) sum);
        
        //printKernel(kernel, kw, kh, "just after dividing by the sum");

        // Laplacian
        for(i=0;i<kh;i++)
            for(j=0;j<kw;j++)
                kernel[i][j] = kernel[i][j] * ((double) ((Math.pow(i-h,2)+Math.pow(j-w,2) - 2*sigma2) / (Math.pow(sigma2,2))));

        //printKernel(kernel, kw, kh, "just after calculating laplacian");

        sum = CVUtils.sum_of(kernel);
        
        // Normalize filter
        for(i=0;i<kh;i++)
            for(j=0;j<kw;j++)
                kernel[i][j] = (double) (kernel[i][j] - ((double) sum/prod));

        kernel_1d = CVUtils.transformMatrixFrom2To1D(kernel);
        mx_1d = CVUtils.transformMatrixFrom2To1D(mx);
        
        myConv.setNormalize(false);
        float[] result = myConv.convolveFloat(mx_1d, m, n, kernel_1d,kw,kh);
        myConv.adjust(result);

        float[][] new_mx = CVUtils.transformMatrixFrom1To2D(result, m, n);

        return CVUtils.roundElementsInMatrix(new_mx);
	}
}
