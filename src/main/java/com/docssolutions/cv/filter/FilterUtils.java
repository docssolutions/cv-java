/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

import com.docssolutions.cv.CVUtils;

public class FilterUtils
{
    public static boolean isKernelSeparable(double[][] _kernel)
    {
        //Matrix kernel = new Matrix(CVUtils.floatMatrixToDouble(_kernel));
        Matrix kernel = new Matrix(_kernel);
        SingularValueDecomposition svd = kernel.svd();
        CVUtils.println(":: S: ");
        CVUtils.printMatrix(svd.getS().getArray());

        CVUtils.println(":: rank of kernel is: "+svd.rank());
        return svd.rank() == 1;
    }

    public static double[][] getKernelComponents(double[][] _kernel)
    {
        //assert isKernelSeparable(_kernel);

        //Matrix kernel = new Matrix(CVUtils.floatMatrixToDouble(_kernel));
        Matrix kernel = new Matrix(_kernel);
        SingularValueDecomposition svd = kernel.svd();
        Matrix s = svd.getS();
        Matrix u = svd.getU();
        Matrix v = svd.getV();
        double[][] _s = s.getArray();
        double[][] _u = u.getArray();
        double[][] _v = v.getArray();
        CVUtils.println(":: S: ");
        CVUtils.printMatrix(_s);
        CVUtils.println(":: U: ");
        CVUtils.printMatrix(_u);
        CVUtils.println(":: V: ");
        CVUtils.printMatrix(_v);

        int n = _s.length;
        double[][] uv = new double[2][n];

        return uv;
    }

    public static boolean isKernelCircularySymmetric(float[][] ckernel)
    {
        assert ckernel.length == ckernel[0].length;
        assert ckernel.length % 2 == 1;

        int n = ckernel.length;
        int c = n / 2;

        for (int i = 0; i <= c; i++)
            for (int j = 0; j <= c; j++)
                if (ckernel[i][j] != ckernel[i][n-j-1] ||
                    ckernel[i][j] != ckernel[n-i-1][j] ||
                    ckernel[i][j] != ckernel[n-i-1][n-j-1])
                    return false;
        return true;
    }

    public static boolean isKernelCircularySymmetric(double[][] ckernel)
    {
        assert ckernel.length == ckernel[0].length;
        assert ckernel.length % 2 == 1;

        int n = ckernel.length;
        int c = n / 2;

        for (int i = 0; i <= c; i++)
            for (int j = 0; j <= c; j++)
                if (ckernel[i][j] != ckernel[i][n-j-1] ||
                    ckernel[i][j] != ckernel[n-i-1][j] ||
                    ckernel[i][j] != ckernel[n-i-1][n-j-1])
                    return false;
        return true;
    }

    public static float[] getComponentFromCircularySymmetricKernel(float[][] ckernel)
    {
        assert isKernelCircularySymmetric(ckernel);

        int n = ckernel.length;
        float[] kc = new float[n];

        for (int i = 0; i < n; i++)
            kc[i] = (float) Math.sqrt(ckernel[i][i]);

        return kc;
    }

    public static double[] getComponentFromCircularySymmetricKernel(double[][] ckernel)
    {
        assert isKernelCircularySymmetric(ckernel);

        int n = ckernel.length;
        double[] kc = new double[n];

        for (int i = 0; i < n; i++)
            kc[i] = Math.sqrt(ckernel[i][i]);

        return kc;
    }
}
