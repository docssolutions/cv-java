/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import com.docssolutions.cv.CVUtils;

public class Gaussian
{
    static int    ksize  = 3;
    static double sigma  = 0.5;
    static double sigma2 = Math.pow(sigma,2);
		
    Convolver myConv = new Convolver();

    public Gaussian() {this(59, 21.0);}
	   
    public Gaussian(int ksize, double sigma) 
	{
        this.ksize = ksize;
        this.sigma = sigma;
        this.sigma2     = Math.pow(sigma,2);
	}

    public float[][] applyRawGaussianFilter(float[][] mx)
    {
        int n = mx.length;
        int m = mx[0].length;
        int i, j = 0;

        double sum = 0.0, prod = ksize * ksize, max = 0.0;
        int s = (ksize-1)/2;
        double[][] kernel = new double[ksize][ksize]; // ksize is the number of rows. ksize is the number of cols.
        double[] kernel_1d;
        
        // Meshgrid with Gaussian
        for(i=0;i<ksize;i++)
            for(j=0;j<ksize;j++)
                kernel[i][j] = (double) Math.exp(-((Math.pow(i-s,2)+Math.pow(j-s,2))/(2*sigma2)));

        sum = CVUtils.sum_of(kernel);

        if(sum != 0.0) 
            for(i=0;i<ksize;i++)
                for(j=0;j<ksize;j++)
                    kernel[i][j] = kernel[i][j] / sum;
        
        //myConv.setNormalize(false);

        //CVUtils.println(":: Aplicar convolución...");
        return myConv.convolveFloatWithCircularySymmetricalKernel(mx, CVUtils.doubleMatrixToFloat(kernel));
    }
}
