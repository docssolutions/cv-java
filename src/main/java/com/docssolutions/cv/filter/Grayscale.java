/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class Grayscale
{

    private BufferedImage imgSrc;

	public Grayscale(BufferedImage inputImage) {
		this.imgSrc = inputImage;
	}

	public float[][] applyRgb2Gray() {
		int i, j;
		WritableRaster rIn = this.imgSrc.getRaster();
		
		int nRows = this.imgSrc.getHeight();
		int nCols = this.imgSrc.getWidth();
		float v = 0;

		float[] rgb = new float[3];
		float[][] result = new float[nRows][nCols];

		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++) {
				rIn.getPixel(j, i, rgb);
				v = 0.2989f * rgb[0] + 0.5870f * rgb[1] + 0.1140f * rgb[2];
				result[i][j] = (float)(Math.round(v));
			}
		}

		return result;
	}
}
