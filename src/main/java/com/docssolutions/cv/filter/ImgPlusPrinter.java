/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.filter;

import java.io.*;

import com.docssolutions.cv.CVUtils;

/** This plugin convolves images using user user defined kernels. */
public class ImgPlusPrinter
{
    public static void printKernel_1D(float[] _kernel, int _kw, int _kh, String msg) 
    {
        int i, j = 0;
        System.out.println("\n"+msg+" - kernel output");
        for(i=0;i<_kh;i++)
            {
                for(j=0;j<_kw;j++)
                    {
                        System.out.print(String.format("%d ", (int)_kernel[i*_kh+j]).toString());
                    }
                System.out.println();
            }
    }

    public static void printKernel(int[][] _kernel, String msg) 
    {
        int i, j = 0;
        System.out.println("\n"+msg+" - kernel output");
        for(i=0;i<_kernel.length;i++)
            {
                for(j=0;j<_kernel[i].length;j++)
                    {
                        System.out.print(String.format("%d ", _kernel[i][j]).toString());
                    }
                System.out.println();
            }
    }
    
    public static void printKernel(float[][] _kernel, String msg) 
    {
        int i, j = 0;
        System.out.println("\n"+msg+" - kernel output");
        for(i=0;i<_kernel.length;i++)
            {
                for(j=0;j<_kernel[i].length;j++)
                    {
                        System.out.print(String.format("%f ", _kernel[i][j]).toString());
                    }
                System.out.println();
            }
    }

    public static void printKernel(double[][] _kernel, String msg) 
    {
        int i, j = 0;
        System.out.println("\n"+msg+" - kernel output");
        for(i=0;i<_kernel.length;i++)
            {
                for(j=0;j<_kernel[i].length;j++)
                    {
                        System.out.print(String.format("%f ", _kernel[i][j]).toString());
                    }
                System.out.println();
            }
    }

    public static void print_img_pixels_F(float[] fs, int w, int h)
    {
        StringBuilder sb = new StringBuilder(4 * w);
        // °J° int size = w*h;
        // °J° if (size != fs.length) throw new IllegalArgumentException("size is incorrect");
        int i, j;
        CVUtils.log("pixels: len "+fs.length+", w "+w+", h "+h);
        // °J° for(i=0;i<size;i++){
        for(i=0;i<w;i++)
            {
                for(j=0;j<h;j++)
                    {
                        // °J° System.out.print(i+" ");
                        // °J° System.out.print(String.format("%3d ", (int)getPixel(i,j)));
                        // °J° System.out.print(String.format("%3d ", j+i*w));
                        System.out.print(String.format("%4.4f ", fs[j+i*w]));
                        // °J° System.out.print(String.format("%4d ", Math.round(fs[j+i*w])));
                        // °J° System.out.print(String.format("%9.4f ", fs[i]));
                    }
                System.out.println();
                // °J° IJ.log(sb.toString());
                // °J° sb = sb.delete(0, sb.length()-1);
                // °J° i--;
            }
        System.out.println();
    }
    
    public static void print_img_pixels_F(float[] fs, int w, int h, int window)
    {
        int i, j;
        CVUtils.log("pixels: len "+fs.length+", w "+w+", h "+h);
        for(i=0;i<window;i++)
            {
                for(j=0;j<window;j++)
                    {
                        // °J° System.out.print(i+" ");
                        // °J° System.out.print(String.format("%3d ", (int)getPixel(i,j)));
                        // °J° System.out.print(String.format("%3d ", j+i*w));
                        System.out.print(String.format("%4.4f ", fs[j+i*w]));
                        // °J° System.out.print(String.format("%9.4f ", fs[i]));
                    }
                System.out.println();
                // °J° IJ.log(sb.toString());
                // °J° sb = sb.delete(0, sb.length()-1);
                // °J° i--;
            }
        System.out.println();
    }
    
    public static void print_img_pixels_I(int[] fs, int w, int h){
        print_img_pixels_I(fs, w, h, w, h);
    }
    
    public static void print_img_pixels_I(int[] fs, int w, int h, int window){
        print_img_pixels_I(fs, w, h, window, window);
    }
    
    public static void print_img_pixels_I(int[] fs, int w, int h, int wwin, int hwin)
    {
        int i, j;
        CVUtils.log("pixels: len "+fs.length+", w "+w+", h "+h);
        if (wwin > w) 
            wwin = w;
        if (hwin > h)
            hwin = h;
            
        for(i=0;i<wwin;i++)
            {
                for(j=0;j<hwin;j++)
                    {
                        // °J° System.out.print(i+" ");
                        // °J° System.out.print(String.format("%3d ", (int)getPixel(i,j)));
                        // °J° System.out.print(String.format("%3d ", j+i*w));
                        System.out.print(String.format("%4d ", fs[j+i*w]));
                        // °J° System.out.print(String.format("%9.4f ", fs[i]));
                    }
                System.out.println();
                // °J° IJ.log(sb.toString());
                // °J° sb = sb.delete(0, sb.length()-1);
                // °J° i--;
            }
        System.out.println();
    }

    public static void print_img_pixels_D(double[] fs, int w, int h, int window)
    {
        int i, j;
        for(i=0;i<window;i++)            {
                for(j=0;j<window;j++)                    {
                        System.out.print(String.format("%4.4f ", fs[j+i*w]));
                    }
                System.out.println();
            }
        System.out.println();
    }

    public static void print_img_pixels_D(double[] fs, int w, int h)
    {
        int i, j;
        for(i=0;i<w;i++)            {
                for(j=0;j<h;j++)                    {
                        System.out.print(String.format("%d- %4.8f, ",j+i*w, fs[j+i*w]));
                    }
                System.out.println();
            }
        System.out.println();
    }


    public static double getScale(float[] kernel) {
        double scale = 1.0;
        double sum = 0.0;
        for (int i=0; i<kernel.length; i++)
            sum += kernel[i];
        if (sum!=0.0)
            scale = 1.0/sum;
        return scale;
    }

    private float getPixel(int x, int y, float[] pixels, int width, int height) {
        if (x<=0) x = 0;
        if (x>=width) x = width-1;
        if (y<=0) y = 0;
        if (y>=height) y = height-1;
        return pixels[x+y*width];
    }
}
