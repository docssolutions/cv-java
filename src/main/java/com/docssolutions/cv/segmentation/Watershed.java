/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.segmentation;

import java.util.Iterator; 
import java.util.Vector; 
import java.util.Arrays; 
import java.awt.*;
import java.awt.image.*;
import java.io.*; 
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import com.docssolutions.cv.CVUtils;
import com.docssolutions.cv.util.RedBlackTree;
import com.docssolutions.cv.util.RedBlackNode;
// °J° import com.docssolutions.cv.filter.ImgPlusPrinter;
import com.docssolutions.cv.filter.Convolver;

public class Watershed
{
	// °J° THE FOLLOWING ARE THE INPUT PARAMETERS, WITH DEFAULT VALUES ASSIGNED TO THEM		
    
    private int connectedPixels = 8; // °J° Neighborhood size
    private float[][] dists;
    private int width; 
    private int height; 
    
    public Watershed(float[][] dists){
        if (dists.length == 0) 
            throw new IllegalArgumentException("matrix cannot be empty");
        this.dists = dists;
        this.width = dists[0].length; // COLUMNS
        this.height = dists.length; // ROWS
    }
    
	//-----------------------------------------------------------------------------------

    /*
     * Computes a label matrix identifying the watershed regions of the input matrix A, 
     * which can have any dimension. The elements of L are integer values greater than 
     * or equal to 0. The elements labeled 0 do not belong to a unique watershed region. 
     * These are called watershed pixels. The elements labeled 1 belong to the first 
     * watershed region, the elements labeled 2 belong to the second watershed region, 
     * and so on.
     * name: applyWatershed
     * @return int array with the labeled matrix
     * 
     */
	public int[][] applyWatershed(){
        int[][] lut = new int[height][width]; 

        // °J° SEARCH MINIMUMS
        Vector<FloodPoint> minimums = getMinSrch();
        int connectedPixels=8;
        RedBlackTree<FloodPoint> queue = new RedBlackTree<FloodPoint>(); 
        int[][] inqueue = new int[height][width]; 
        
        for (int lr = 0; lr < height; lr++){
            for (int lc = 0; lc < width; lc++){
                if (dists[lr][lc] != Float.NEGATIVE_INFINITY){                
                    lut[lr][lc] = -1;
                } else {
                    lut[lr][lc] = 1;
                }
                inqueue[lr][lc] = 0;
            }
        }
        
        // INITIALIZE QUEUE WITH EACH FOUND MINIMUM 
        for (int i=0;i<minimums.size();i++) { 
            FloodPoint p = minimums.elementAt(i); 
            int label = p.label; 
            if (label == 1)
                continue;
            // INSERT STARTING PIXELS AROUND MINIMUMS 
            addPointRB(queue, inqueue, p.r-1,   p.c,   label); 
            addPointRB(queue, inqueue,   p.r, p.c-1,   label); 
            addPointRB(queue, inqueue, p.r+1,   p.c,   label); 
            addPointRB(queue, inqueue,   p.r,  p.c+1,   label); 
            if (connectedPixels==8) { 
                addPointRB(queue, inqueue, p.r-1, p.c-1, label); 
                addPointRB(queue, inqueue, p.r+1, p.c-1, label); 
                addPointRB(queue, inqueue, p.r+1, p.c+1, label); 
                addPointRB(queue, inqueue, p.r-1, p.c+1, label); 
            } 
            lut[p.r][p.c] = label; 
            inqueue[p.r][p.c] = 1; 
            
        } 

        RedBlackNode<FloodPoint> node = null; 
        FloodPoint extracted = null; 

        // START FLOODING 
        int[] labels = new int[connectedPixels]; 
        while (!queue.isEmpty()) { 
            // FIND MINIMUM & REMOVE IT FROM THE QUEUE 
            node = queue.treeMinimum(); 
            extracted = node.key;
            queue.remove(node);
            int r = extracted.r; 
            int c = extracted.c; 
            int label = extracted.label; 
            // °J° CHECK PIXELS AROUND EXTRACTED PIXEL 
            labels[0] = getLabel(lut,r,c-1); 
            labels[1] = getLabel(lut,r+1,c); 
            labels[2] = getLabel(lut,r,c+1); 
            labels[3] = getLabel(lut,r-1,c); 
            if (connectedPixels==8) { 
                labels[4] = getLabel(lut,r-1,c-1); 
                labels[5] = getLabel(lut,r+1,c-1); 
                labels[6] = getLabel(lut,r+1,c+1); 
                labels[7] = getLabel(lut,r-1,c+1); 
            } 

            boolean onEdge = isEdge(labels,extracted); 
            if (onEdge) { 
                // °J° LEAVE EDGES WITHOUT LABEL 
            } else { 
                // °J° SET PIXEL WITH LABEL 
                lut[r][c] = extracted.label;
            } 
            
            // °J° ADDS NEIGHBORS TO THE WAIT LIST
            if (!inQueue(inqueue,r,c-1)) { 
                addPointRB(queue, inqueue, r, c-1, label); 
            } 
            if (!inQueue(inqueue,r+1,c)) { 
                addPointRB(queue, inqueue, r+1, c, label); 
            } 
            if (!inQueue(inqueue,r,c+1)) { 
                addPointRB(queue, inqueue, r, c+1, label); 
            } 
            if (!inQueue(inqueue,r-1,c)) { 
                addPointRB(queue, inqueue, r-1, c, label); 
            } 
            if (connectedPixels==8) { 
                if (!inQueue(inqueue,r-1,c-1)) { 
                    addPointRB(queue, inqueue, r-1, c-1, label); 
                } 
                if (!inQueue(inqueue,r+1,c-1)) { 
                    addPointRB(queue, inqueue, r+1, c-1, label); 
                } 
                if (!inQueue(inqueue,r+1,c+1)) { 
                    addPointRB(queue, inqueue, r+1, c+1, label); 
                } 
                if (!inQueue(inqueue,r-1,c+1)) { 
                    addPointRB(queue, inqueue, r-1, c+1, label); 
                } 
            } 
        } 
        
        return lut; 
	} 
    
    /*
     * Find all the minimums of the distances image, and returns them labeled. 
     * It asigns the same label to minimums with value of NEGATIVE INFINITY
     * name: getMinSrch
     * @return A Vector of FloodPoints with the found minimums labeled
     * 
     */
    public Vector<FloodPoint> getMinSrch(){
        Vector<Integer> truemins = new Vector(); 
        Vector<FloodPoint> mins = new Vector<FloodPoint>(); 
        int[][] labels = new int[height][width];
        for (int li = 0; li < height; li++){
            int[] lr = new int[width];
            int[] mr = new int[width];
            java.util.Arrays.fill(mr, 0);
            java.util.Arrays.fill(lr, -1);
            labels[li] = lr;
        }
        
        // °J° Vector<FloodPoint> path = new Vector<FloodPoint>(); 
        int minsFound = 0;
        boolean keep_going;
        int next;
        int rn; // neighbors row
        int cn; // neighbors column
        int lbl;
        int act_r, act_c;
        // °J° int inf_lbl= -1;
        int act_lbl= 0;
        for (int r = 0; r < height; r++){
            for (int c = 0; c < width; c++){
                // °J° CHECK IF THAT POSITION HAS NOT BEEN VISITED
                if(labels[r][c]==-1){
                    act_r = r;
                    act_c = c;
                    keep_going = true;

                    // °J° PAINT AS A NEW COLOR
                    act_lbl = minsFound++;
                    labels[r][c] = act_lbl;

                    // °J° FIND IF ANOTHER STEP CAN BE TAKEN
                    while(keep_going){
                        // °J° FIND A DOWNSTEP
                        next = findWayLbl(act_r, act_c, labels, act_lbl);
                        rn = -1 + next / 3;
                        cn = -1 + next % 3;

                        // °J° THE STEP CAN BE TAKEN IFF THERE IS A MINIMUM AND IT IS
                        // °J°     NOT BIGGER THAN THE ACTUAL POSITION .
                        if ((next != -1) && (dists[act_r][act_c] >= dists[act_r+rn][act_c+cn])){
                            // °J° THE STEP CAN BE TAKEN

                            // °J° GET THE LABEL
                            lbl = labels[act_r+rn][act_c+cn];

                            // °J° THE NEXT POSITION BELONGS TO ANOTHER LABEL .
                            if (lbl != -1){

                                // °J° STOP
                                keep_going = false;
                                
                            // °J° THE NEXT POSITION DO NOT HAS ANY LABEL
                            }else{
                                
                                // °J° UPDATE POSITION
                                act_r += rn;
                                act_c += cn;
                                
                                // °J° ADD TO THE PATH
                                labels[act_r][act_c] = act_lbl;
                            }
                        
                        }else{
                            // °J° THE STEP CANNOT BE TAKEN BECAUSE YOU ARE IN A MINIMUM.
                            
                            // °J° STOP
                            keep_going = false;
                            
                            // °J° MARK PATH WITH NEW LABEL
                            mins.add(new FloodPoint(act_r,act_c, act_lbl, dists[act_r][act_c]));
                            truemins.add(act_lbl);
                        }
                    }
                }
            }
        }
        
        normalizePath(mins);
        
        return mins;
    }


    private boolean inQueue(int[][] inqueue, int r, int c) { 
        if (c<0||c>=width||r<0||r>=height) { 
            return false; 
        } 
        if (inqueue[r][c] == 1) { 
            return true; 
        } 
        return false; 
    } 
    
    private boolean isEdge(int[] labels, FloodPoint extracted) { 
        for (int i=0;i<labels.length;i++) { 
            if (labels[i]!=extracted.label&&labels[i]!=-1&&labels[i]!=-2) { 
                return true; 
            } 
        } 
        return false; 
    } 
    
    private int getLabel(int[][] lut, int r, int c) { 
        if (c<0||c>=width||r<0||r>=height) { 
            return -2; 
        } 
        return lut[r][c]; 
    } 
    
    private void addPointRB(RedBlackTree<FloodPoint> queue, int[][] inqueue, int r, int c, int label) { 
        if (c<0||c>=width||r<0||r>=height) { 
            return; 
        } 
        if (dists[r][c] == Float.NEGATIVE_INFINITY)
            return;
            
        queue.insert(new FloodPoint(r,c,label,dists[r][c])); 
        inqueue[r][c] = 1; 
    } 
    
    private void normalizePath(Vector<FloodPoint> path){
        Iterator it = path.iterator();
        FloodPoint vtemp = null; 
        int count = 2;
        // °J° int inf_lbl = 0;
        while(it.hasNext()){
            vtemp = (FloodPoint)it.next();
            if (vtemp.depth == Float.NEGATIVE_INFINITY)
                vtemp.label = 1;
            else 
                vtemp.label = count++;
        }
    }

    private int findWayLbl(int row, int col, int[][] labels, int act_lbl){
        double min = Double.POSITIVE_INFINITY;
        int mindex = -1;
        int tr=0, tc=0;
        float dst = dists[row][col];
        float ndst;
        
        // °J° LOOK IN THE NEIGHBORHOOD
        for (int r = 0; r < 3; r++){
            for (int c = 0; c < 3; c++){
                if((r != 1) || (c != 1)){
                    tr = row-1+r;
                    tc = col-1+c;
                    // °J° CHECK BOUNDARIES
                    if((tr >= 0) && (tc >= 0) && (tr < height) && (tc < width)){
                        ndst = dists[tr][tc] ;
                        // °J° FIND OUT IF THAT POSITIONS HAS BEEN VISITED BEFORE
                        if (labels[tr][tc] != act_lbl){
                            if (ndst < min){
                                if ((ndst == Float.NEGATIVE_INFINITY) && (dst != Float.NEGATIVE_INFINITY))
                                    continue;
                                min = ndst;
                                mindex = r*3 + c;
                            }
                        }
                    }
                }
            }
        }
        return mindex;
    }
    
    class FloodPoint implements Comparable<FloodPoint> { 
        int r; 
        int c; 
        int label; 
        float depth; 
         
        public FloodPoint(int r, int c, int label, float depth) { 
            this.r = r; 
            this.c = c; 
            this.label = label; 
            this.depth = depth; 
        } 
        
        @Override 
        public int compareTo(FloodPoint other) { 
            if (other.depth == Float.NEGATIVE_INFINITY) { 
                return 2;
            }else if (this.depth < other.depth ) { 
                return -1; 
            } else if (this.depth > other.depth ) { 
                return 1; 
            } 
            return 0; 
        } 
    } 
} // end of filter Class 


