/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv.segmentation;

import java.awt.Point;
import java.util.*;

import com.docssolutions.cv.CVUtils;

public class Labeller
{
    // Enumerators indicating 4 or 8 neighborhood connectivity.
    public static final int E_4CONN = 0;
    public static final int E_8CONN = 1;

    private int components_number = 0;
    private float[][] labels;
    
    public Labeller() {}

    public int getComponentsNumber() { return components_number; }
    public float[][] getLabels() { return labels; }

    /**
     *
     * @param mx must be a bidimensional float matrix.
     *
     * @return An bidimensional int matrix instance with the components.
     * 
     * @see 
     */
    
    public float[][] getRawLabels(float[][] mx)
    {
        //System.out.println(":: In applyRawAreaOpening");
        int m = mx[0].length;
        int n = mx.length;
        int i, j;
        this.labels = new float[n][m];
        float[][] new_mx = CVUtils.copyMatrix(mx);

        // Vector is a suitable collection struture for thread-safe improvements. To-do optimization for next release.
        Vector<ConnectedComponentTuple> components = new Vector<ConnectedComponentTuple>(100, 100);

        // For commodity regarding vector handling, ids start at 0.
        int next_available_component_id = 0;

        Point p;
        int pixels_count;
        Iterator<ConnectedComponentTuple> it;

        // Process all matrix looking for turned on pixels and process its connected component. Add it to components vector
        // and mark each visited pixel with special value 2.0f
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                if (new_mx[i][j] == 1.0f) {
                    p = new Point(j,i);
                    //System.out.println(":: New component at ("+p.x+","+p.y+"), processing...");
                    pixels_count = getPixelCountAndProcess8NeighboorConnectedComponent(new_mx, this.labels, p, next_available_component_id+1);
                    components.add (next_available_component_id,
                                    new ConnectedComponentTuple(next_available_component_id+1, p, pixels_count));
                    next_available_component_id++;
                }

        //System.out.println(":: There are "+components.size()+" connected components. Ids start at 0.");

        this.components_number = components.size();

        //// Take the special value of 2.0f to 1.0f and everything else turn it to 0.0f.
        //for(i=0;i<n;i++)
        //    for(j=0;j<m;j++)
        //        new_mx[i][j] = new_mx[i][j] == 2.0f ? 1.0f : 0.0f;

        //System.out.println(":: Print label matrix");
        //CVUtils.printMatrix(this.labels);

        return this.labels;
    }

    private int getPixelCountAndProcess4NeighboorConnectedComponent(float[][] mx, float[][] labels, Point p, int id)
    { return getPixelCountAndProcessConnectedComponent(mx, labels, p, E_4CONN, id); }

    private int getPixelCountAndProcess8NeighboorConnectedComponent(float[][] mx, float[][] labels, Point p, int id)
    { return getPixelCountAndProcessConnectedComponent(mx, labels, p, E_8CONN, id); }

    /**
     * @brief 
     *
     * @param mx must be a bidimensional float matrix.
     * @param labels must be a bidimensional int matrix.
     * @param p must be a given Point in the matrix.
     * @param conn indicates whether processing must be done with 4 or 8 neighbor connectivity.
     *
     * @return Compoment's pixel count.
     */

    private int getPixelCountAndProcessConnectedComponent(float[][] mx, float[][] labels, Point p, int conn, int id)
    {
        if ( mx[p.y][p.x] != 1.0f ) return 0;

        int pixels_count = 0;

        int m = mx[0].length;
        int n = mx.length;

        Queue<Point> queue = new LinkedList<Point>();
        queue.add(p);

        // Start consuming queue until empty. Increment counter for each pixel and add all 8-adjacent 1.0f pixels to queue.
        while (!queue.isEmpty()) {

            p = queue.remove();

            if (mx[p.y][p.x] == 1.0f) {

                mx[p.y][p.x] = 2.0f;
                pixels_count++;
                labels[p.y][p.x] = (float) id;
                //System.out.println(":: Point processed at ("+p.x+","+p.y+"), "+queue.size()+" remaining.");

                // 4 Neighbors
                if (0 <= p.y-1 && mx[p.y-1][p.x] == 1.0f) {queue.add(new Point(p.x, p.y-1));} // N
                if (p.y+1 < n  && mx[p.y+1][p.x] == 1.0f) {queue.add(new Point(p.x, p.y+1));} // S
                if (0 <= p.x-1 && mx[p.y][p.x-1] == 1.0f) {queue.add(new Point(p.x-1, p.y));} // W
                if (p.x+1 < m  && mx[p.y][p.x+1] == 1.0f) {queue.add(new Point(p.x+1, p.y));} // E

                if (conn == E_8CONN) {
                    // Last 4 neighbors
                    if (0 <= p.x-1 && 0 <= p.y-1 && mx[p.y-1][p.x-1] == 1.0f) {queue.add(new Point(p.x-1, p.y-1));} // NW
                    if (0 <= p.x-1 && p.y+1 < n  && mx[p.y+1][p.x-1] == 1.0f) {queue.add(new Point(p.x-1, p.y+1));} // SW
                    if (p.x+1 < m  && 0 <= p.y-1 && mx[p.y-1][p.x+1] == 1.0f) {queue.add(new Point(p.x+1, p.y-1));} // NE
                    if (p.x+1 < m  && p.y+1 < n  && mx[p.y+1][p.x+1] == 1.0f) {queue.add(new Point(p.x+1, p.y+1));} // SE
                }
            }
        }

        return pixels_count;
    }

    /**
     * @brief Auxiliary class holding values for component management.
     */

    private class ConnectedComponentTuple
    {
        public int id;
        public Point p;
        public int pixels_count;

        public ConnectedComponentTuple(int id, Point p, int pixels_count)
        {
            this.id = id;
            this.p = p;
            this.pixels_count = pixels_count;
        }
    }
}

