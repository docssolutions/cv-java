/*
 * Copyright (c) 2015 DoCS Solutions S.R.L.
 *
 * This file is part of DoCS Computer Vision.
 *
 *  DoCS Computer Vision is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License version 3 as 
 *  published by the Free Software Foundation.
 *
 *  DoCS Computer Vision is distributed in the hope that it will be useful, but 
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License 
 *  for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License in the 
 *  file LICENSE.LESSER along with DoCS Computer Vision.  If not, see 
 *  <https://www.gnu.org/licenses/lgpl.html>. 
 */

package com.docssolutions.cv;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;
import java.nio.file.Files;

import csv.*;
import csv.impl.*;

import Jama.Matrix;
import Jama.EigenvalueDecomposition;

import com.docssolutions.cv.util.moore.MoorePoint;

public class CVUtils
{
    public static final int NUM_THREADS;
    static {
        NUM_THREADS = Runtime.getRuntime().availableProcessors() * 2;
    }

	/**
	 * Obtain the covariance matrix for a input containing NaN-Values
	 * 
	 * @param nMtrx Input matrix with NaN-values
	 * @return float[][] The covariance matrix result
	 */
	public static float[][] covarianceNanMatrix(float[][] nMtrx) {
		int i, j, k = 0;
		int rows = nMtrx.length;
		int cols = nMtrx[0].length;

        int[] idx = new int[rows];
		boolean anyNan = false;
		int count = 0;

		for (i = 0; i < rows; i++) {
			anyNan = false;
			for (j = 0; j < cols; j++) {
				if (nMtrx[i][j] != nMtrx[i][j]) {
					anyNan = true;
					break;
				}
			}
			if (anyNan) count++;
			else idx[k++] = i;
		}

		int nRows = rows - count;

		if (nRows == 0) return null;
		else {
		    float[][] aMtrx = new float[nRows][cols];
		    for (k = 0; k < nRows; k++) {
				for (j = 0; j < cols; j++)
					aMtrx[k][j] = nMtrx[idx[k]][j];
			}

			return covarianceMatrix(aMtrx);
		}
	}

	/**
	 * Computes the covariance matrix corresponding
	 * to the input 2D-array
	 * 
	 * @param mtrx Input 2D-array
	 * @return float[][] Covarince matrix
	 */
	public static float[][] covarianceMatrix(float[][] mtrx) {
		int i, j;

		int rows = mtrx.length;
		int cols = mtrx[0].length;

		float[][] cMtrx = new float[rows][cols];
		float[][] cMtrxT = new float[cols][rows];
		float mean = 0.0f;

		for (j = 0; j < cols; j++) {
			mean = meanVector(mtrx, j);

			for (i = 0; i < rows; i++) {
				cMtrx[i][j] = mtrx[i][j] - mean;
				cMtrxT[j][i] = cMtrx[i][j];
			}
		}

		float[][] cov = multMatrices(cMtrxT, cMtrx);
		for (i = 0; i < cols; i++) {
			for (j = 0; j < cols; j++)
			    cov[i][j] /= (rows - 1);
		}

		return cov;
	}

	public static double[][] averageFilter(double[][] image, int padWidth, int padHeight) {
		int nRows, nCols;
		int pp, qq;

		int m = padHeight;
		int n = padWidth;

		if ( (m % 2) == 0 ) m = m - 1; 		// check for even window sizes
		if ( (n % 2) == 0 ) n = n - 1;

		// Initialization.
		nRows = image.length;
		nCols = image[0].length;

		// Pad the image.
		double[][] imageP = CVUtils.padarray(image, (m + 1) / 2, (n + 1) / 2, false);
		double[][] imagePP = CVUtils.padarray(imageP, (m - 1) / 2, (n - 1) / 2, true);

		// Matrix 'mt' is the sum of numbers on the left and above the current cell.
		double[][] imageD = imagePP;
		double[][] mt = CVUtils.cumsum(imageD);		


		// Calculate the mean values from the look up table 'mt'.
		double[][] imageI = new double[nRows][nCols];
		double v = 0.0;
		for (pp = 0; pp < nRows; pp++) {
			for (qq = 0; qq < nCols; qq++) {
				v = mt[pp + m][qq + n] + mt[pp][qq]; 		// imageI = t(1+m:rows+m, 1+n:columns+n) + t(1:rows, 1:columns)...
				v -= (mt[pp + m][qq] + mt[pp][qq + n]); 		// 			- t(1+m:rows+m, 1:columns) - t(1:rows, 1+n:columns+n);

				imageI[pp][qq] = v;
			}
		}

		// Now each pixel contains sum of the window. But we want the average value.
		double a = (double)(m * n);
		imageI = CVUtils.divideMatrixByScalar(imageI, a);

		return imageI;
	}

	public static double[][] padarray(double[][] A, int m, int n, boolean direction) {
		// compute indices then index into input image
		int r, c;
		double v = 0.0;
		
		int h = A.length;
		int w = A[0].length;

		int s = h + m;
		int t = w + n;
		
		double[][] b = new double[s][t];

		if (direction) {
			// input matrix
			for (r = 0; r < h; r++) {
				for (c = 0; c < w; c++)
					b[r][c] = A[r][c];
			}

			// Down rows
			for (r = 0; r < m; r++) {
				for (c = 0; c < w; c++)
					b[h + r][c] = A[h - 1][c];
			}

			// Right columns
			for (r = 0; r < h; r++) {
				for (c = 0; c < n; c++)
					b[r][w + c] = A[r][w - 1];
			}

			// All others
			v = A[h - 1][w - 1];
			for (r = h; r < s; r++) {
				for (c = w; c < t; c++)
					b[r][c] = v;
			}
		}
		else {
			// Up rows
			for (r = 0; r < m; r++) {
				for (c = 0; c < w; c++)
					b[r][n + c] = A[0][c];
			}

			// Left columns
			for (r = 0; r < h; r++) {
				for (c = 0; c < n; c++)
					b[m + r][c] = A[r][0];
			}

			// All Others
			v = A[0][0];
			for (r = 0; r < m; r++) {
				for (c = 0; c < n; c++)
					b[r][c] = v;
			}

			// input matrix
			for (r = 0; r < h; r++) {
				for (c = 0; c < w; c++)
					b[m + r][n + c] = A[r][c];
			}
		}

		return b;
	}

	public static double[][] cumsum(double[][] A) {
		double[][] acumByCols = CVUtils.cumsumCols(A);
		return CVUtils.cumsumRows(acumByCols);
	}

	private static double[][] cumsumCols(double[][] A) {
		int pp, qq;
		int h, w;

		h = A.length;
		w = A[0].length;
		double[][] B = new double[h][w];

		double acum = 0.0f;
		for (qq = 0; qq < w; qq++) {
			acum = 0.0f;
			for (pp = 0; pp < h; pp++) {
				acum += A[pp][qq];
				B[pp][qq] = acum;
			}
		}

		return B;
	}

	private static double[][] cumsumRows(double[][] A) {
		int pp, qq;
		int h, w;

		h = A.length;
		w = A[0].length;
		double[][] B = new double[h][w];

		double acum = 0.0;
		for (pp = 0; pp < h; pp++) {
			acum = 0.0f;
			for (qq = 0; qq < w; qq++) {
				acum += A[pp][qq];
				B[pp][qq] = acum;
			}
		}

		return B;
	}

    /**
     * Calculates a multiplication between matrices
     * 
     * @param A First 2D-array input
     * @param B Second 2D-array input
     * 
     * @return float[][] Contains a matrix result of AxB
     */
    public static float[][] multMatrices(float[][] A, float[][] B) {
		int i, j, k;

        int l = A.length;
        int m = B.length;
        int n = B[0].length;

        float[][] matriz_result = new float[l][n];
        float suma_ent = 0;

        for(i = 0; i < l; i++) {
            for(j = 0; j < n; j++) {
                suma_ent = 0;

                for (k = 0; k < m; k++)
                    suma_ent += A[i][k] * B[k][j];

                matriz_result[i][j] = suma_ent;
            }
        }

        return matriz_result;
    }

	/**
	 * Calculates the mean value for the input 2D-array
	 * 
	 * @param mtrx Input 2D-Array
	 * 
	 * @return float Mean value
	 */
	public static float meanMtrx(float[][] mtrx) {
		int i, j;
		int nRows = mtrx.length;
		int nCols = mtrx[0].length;

		double acum = 0.0;
		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				acum += (double)mtrx[i][j];
		}

		return ((float)acum / (nRows * nCols));
	}

	public static float meanMtrx1D(float[] vec) {
		int i, j;
		int size = vec.length;

		double acum = 0.0;
		for (i = 0; i < size; i++)
			acum += (double)vec[i];

		return ((float)acum / size);
	}

	public static float std1D(float[] vec, float mean) {
		int i, j;
		int size = vec.length;
		double v = 0.0;

		double acum = 0.0;
		for (i = 0; i < size; i++) {
			v = (double)(vec[i] - mean);
			acum += v * v;
		}
		acum /= size;

		double std = Math.sqrt(acum);
		return (float)std;
	}

	public static float[][] andMatrices(float[][] A, float[][] B) {
		int i, j;
		int nRows = A.length;
		int nCols = A[0].length;

		float[][] result = new float[nRows][nCols];
		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = ( (A[i][j] > 0.0f) && (B[i][j] > 0.0f) ) ? 1.0f : 0.0f;
		}

		return result;
	}

	public static float[][] xorMatrices(float[][] A, float[][] B) {
		int i, j;
		int nRows = A.length;
		int nCols = A[0].length;

		float[][] result = new float[nRows][nCols];
		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = ( A[i][j] != B[i][j] ) ? 1.0f : 0.0f;
		}

		return result;
	}

	public static float[][] negMtrx(float[][] inputMtrx) {
		int i, j;
		int nRows = inputMtrx.length;
		int nCols = inputMtrx[0].length;

		float[][] result = new float[nRows][nCols];

		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = (inputMtrx[i][j] == 0.0f) ? 1.0f : 0.0f;
		}

		return result;
	}

	public static double[][] squareMat(double[][] A) {
		int i, j;
		int nRows = A.length;
		int nCols = A[0].length;

		double[][] result = new double[nRows][nCols];
		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = A[i][j] * A[i][j];
		}

		return result;
	}

	public static double[][] squareRootMtrx(double[][] A) {
		int i, j;
		int nRows = A.length;
		int nCols = A[0].length;

		double[][] result = new double[nRows][nCols];
		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = (double)Math.sqrt(A[i][j]);
		}

		return result;
	}

	public static double[][] divideMatrixByScalar(double[][] A, double k) {
		int i, j;
		int nRows = A.length;
		int nCols = A[0].length;

		double[][] result = new double[nRows][nCols];
		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = A[i][j] / k;
		}

		return result;
	}

	public static double[][] sustractMatrices(double[][] A, double[][] B) {
		int i, j;
		int nRows = A.length;
		int nCols = A[0].length;

		double[][] result = new double[nRows][nCols];
		for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = A[i][j] - B[i][j];
		}

		return result;
	}

	/**
	 * Calculates the mean value for the iCol-th column for 
	 * the input 2D-array
	 * 
	 * @param mtrx Input 2D-Array
	 * @param iCol j-th column index
	 * 
	 * @return float Mean value for the selected column
	 */
	public static float meanVector(float[][] mtrx, int iCol) {
		int i;

		float acum = 0.0f;
		for (i = 0; i < mtrx.length; i++)
			acum += mtrx[i][iCol];

		return (acum / mtrx.length);
	}

    public static float sum_of(float[][] mx) 
    {
        int n = mx.length;
        int m = mx[0].length;
        int i, j = 0;
        float sum = 0.0f;
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                sum += mx[i][j];
        return sum;
    }

    public static double sum_of(double[][] mx) 
    {
        int n = mx.length;
        int m = mx[0].length;
        int i, j = 0;
        double sum = 0.0;
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                sum += mx[i][j];
        return sum;
    }

    public static double max_of(double[][] mx) 
    {
        int n = mx.length;
        int m = mx[0].length;
        int i, j = 0;
        double max = mx[0][0];
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                if(max < mx[i][j])
                    max = mx[i][j];
        return max;
    }

    /*
     * Element wise pow function. Performs a pow operation over every element
     * of a float[][]
     * name: powEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static float[] powEW(float[] arr, int exponent){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)Math.pow(arr[i], exponent);
        }
        return result;
    }
    
    /*
     * Element wise division function. Performs a division operation over every element
     * of a float[][]
     * name: divEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static float[] divEW(int[] arr, float divisor){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
            // °J° System.out.println("Div between "+dividend);
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)(arr[i]/divisor);
        }
        return result;
    }
    
    
    /*
     * Element wise division function. Performs a division operation over every element
     * of a float[][]
     * name: divEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static float[] divEW(float[] arr, float dividend){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
            // °J° System.out.println("Div between "+dividend);
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)(arr[i]/dividend);
        }
        return result;
    }
    
    /*
     * Element wise division function. Performs a division operation over every element
     * of a float[][]
     * name: divEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static double[] divEW(double[] arr, double dividend){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        double[] result =  new double[arr.length];
            // °J° System.out.println("Div between "+dividend);
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (double)(arr[i]/dividend);
        }
        return result;
    }
    
    /*
     * Element wise division function. Performs a division operation over every element
     * of an int[]
     * name: divEW
     * @param arr an int unidimentional array which is going to be modified
     * 
     */
    public static double[] divEW(int[] arr, double dividend){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        double[] result =  new double[arr.length];
            // °J° System.out.println("Div between "+dividend);
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (double)(arr[i]/dividend);
        }
        return result;
    }
    
    /*
     * Element wise dinverted division function. Performs a division operation over every element
     * of a float[][]
     * name: divInvEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static float[] divInvEW(float[] arr, float divisor){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)(divisor/arr[i]);
        }
        return result;
    }
    
    public static double[] addEW(double[] arr, int sum){
        return addEW(arr, (double)sum);
    }
    
    /*
     * Element wise add function. Performs a division operation over every element
     * of a double[][]
     * name: addEW
     * @param arr a double bidimentional array which is going to be modified
     * 
     */
    public static double[] addEW(double[] arr, double sum){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        double[] result =  new double[arr.length];
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (double)(arr[i]+sum);
        }
        return result;
    }
    
    public static float[] addEW(float[] arr, int sum){
        return addEW(arr, (float)sum);
    }
    
    /*
     * Element wise add function. Performs a division operation over every element
     * of a float[][]
     * name: addEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static float[] addEW(float[] arr, float sum){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)(arr[i]+sum);
        }
        return result;
    }
    
    public static float[][] timesScalarEW(float[][] arr, float multiple) {
		int i, j;
		int nRows = arr.length;
		int nCols = arr[0].length;

        float[][] result =  new float[nRows][nCols];
        
        for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				result[i][j] = (float)(arr[i][j] * multiple);
        }

        return result;
    }

    /*
     * Element wise multiplication function. Performs a multiplication operation over every element
     * of a float[][], with a scalar value.
     * name: timesScalarEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static float[] timesScalarEW(float[] arr, float multiple){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)(arr[i]*multiple);
        }
        return result;
    }
    
    /*
     * Element wise matrix multiplication function. Performs a multiplication operation over every element
     * of two float[][]'s
     * name: timesMatEW
     * @param A a float bidimentional array which is going to be modified
     * @param B a float bidimentional array which is going to be multiplied
     * 
     */
    public static float[] timesMatEW(float[] A, float[] B) throws IllegalArgumentException{
        if ((A.length == 0) || (B.length == 0)){
            // °J° System.out.println("array is empty");
            throw new IllegalArgumentException("array is empty");
        }
        if (A.length != B.length){
            // °J° System.out.println("arrays should be the same size");
            throw new IllegalArgumentException("arrays should be the same size");
        }
            
        float[] result =  new float[A.length];
        
        for (int i = 0; i< A.length; i++){
            result[i] = (float)(A[i] * B[i]);
        }
        return result;
    }
    
    /*
     * Element wise matrix addition function. Performs an addition operation over every element
     * of two float[][]'s
     * name: timesMatEW
     * @param A a float bidimentional array which is going to be modified
     * @param B a float bidimentional array which is going to be multiplied
     * 
     */
    public static float[] addMatEW(float[] A, float[] B) throws IllegalArgumentException{
        if ((A.length == 0) || (B.length == 0)){
            // °J° System.out.println("array is empty");
            throw new IllegalArgumentException("array is empty");
        }
        if (A.length != B.length){
            // °J° System.out.println("arrays should be the same size");
            throw new IllegalArgumentException("arrays should be the same size");
        }
            
        float[] result =  new float[A.length];
        
        for (int i = 0; i< A.length; i++){
            result[i] = (float)(A[i] + B[i]);
        }
        return result;
    }
    
    /*
     * Element wise pow function. Performs an exponential operation over every element
     * of a float[][]
     * name: expEW
     * @param arr a float bidimentional array which is going to be modified
     * 
     */
    public static float[] expEW(float[] arr){
        if (arr.length == 0){
            System.out.println("array is empty");
        }
        float[] result =  new float[arr.length];
        
        for (int i = 0; i< arr.length; i++){
            result[i] = (float)(Math.exp(arr[i]));
        }
        return result;
    }

    public static float[][] roundElementsInMatrix(float[][] mx) 
    {
        int n = mx.length;
        int m = mx[0].length;
        int i, j = 0;
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                mx[i][j] = (float) Math.round(mx[i][j]);
        return mx;
    }


    public static boolean isBinaryMatrix(float[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i, j;

        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                if(mx[i][j] != 0.0f && mx[i][j] != 1.0f)
                    return false;

        return true;
    }
    
    public static float[][] invertBinaryMatrix(float[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i, j;

        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                mx[i][j] = mx[i][j] == 0.0f ? 1.0f : 0.0f;
        return mx;
    }

    public static float[][] markLabelDifferences(float[][] left, float[][] right)
    {
        int max = (int) Math.max(maxValueOfMatrix(left), maxValueOfMatrix(right)) + 1;
        int labels[] = new int[max];

        int m = left[0].length;
        int n = left.length;
        int i, j;

        float[][] diff = new float[n][m];

        for(i=0;i<n;i++)
            for(j=0;j<m;j++) {
                if (left[i][j] != 0.0f) {
                    if (labels[(int) left[i][j]] == 0) {
                        labels[(int) left[i][j]] = (int) right[i][j];
                    }else{
                        if (labels[(int) left[i][j]] != (int) right[i][j]) {
                            println(":: [ALERT] Labels differ at point ("+j+","+i+")!!!");
                            diff[i][j] = 1.0f;
                        }
                    }
                }
            }

        if (maxValueOfMatrix(diff) == 0.0f) {
            println(":: [SUCCESS] Matrices do not differ!!!");
        }

        return diff;
    }

    public static float[][] markSquareDifferences(float[][] left, float[][] right, float tolerance)
    {
        int m = left[0].length;
        int n = left.length;
        int i, j;

        int differences_count = 0;

        float[][] diff = new float[n][m];

        for(i=0;i<n;i++)
            for(j=0;j<m;j++) {
                diff[i][j] = (float) Math.pow(left[i][j] - right[i][j], 2);
                if (Math.abs(left[i][j] - right[i][j]) > (double) tolerance) {
					System.out.println(" (" +i+ ", "+j+ ") --> " + 
					String.valueOf(left[i][j])+ " - " + String.valueOf(right[i][j]) +
					"[" + String.valueOf(right[i][j] - left[i][j]) +"]");

                    differences_count++;
				}
            }

        if (differences_count > 0)
            System.out.println(":: [ALERT] Matrices differ by "+differences_count+" points!!!");
        else
            System.out.println(":: [SUCCESS] Matrices do not differ!!!");

        return diff;
    }

    public static float[][] markSquareDifferences(float[][] left, float[][] right)
    {
        int m = left[0].length;
        int n = left.length;
        int i, j;

        int differences_count = 0;

        float[][] diff = new float[n][m];

        for(i=0;i<n;i++)
            for(j=0;j<m;j++) {
                diff[i][j] = (float) Math.pow(left[i][j] - right[i][j], 2);
                if (left[i][j] != right[i][j]) {
					System.out.println(" (" +String.valueOf(i)+ ", " +String.valueOf(j)+ ") --> " + 
					String.valueOf(left[i][j])+ " - " + String.valueOf(right[i][j]) +
					"[" + String.valueOf(right[i][j] - left[i][j]) +"]");

                    differences_count++;
				}
            }

        if (differences_count > 0)
            System.out.println(":: [ALERT] Matrices differ by "+differences_count+" points!!!");
        else
            System.out.println(":: [SUCCESS] Matrices do not differ!!!");

        return diff;
    }
    
    public static void printPresenceInMatrix(float[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i,j;
        System.out.println(":: printPresenceInMatrix =>");
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                if (mx[i][j] == 0.0f)
                    System.out.print("_ ");
                else
                    System.out.print("X ");
            }
            System.out.println();
        }
    }

    public static void printPresenceInMatrix(byte[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i,j;
        System.out.println(":: printPresenceInMatrix =>");
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                if (mx[i][j] == 0)
                    System.out.print("_ ");
                else
                    System.out.print("X ");
            }
            System.out.println();
        }
    }

    public static void printPresenceInBooleanMatrix(boolean[][] bmx)
    {
        int m = bmx[0].length;
        int n = bmx.length;
        int i,j;
        System.out.println(":: printPresenceInBooleanMatrix =>");
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                if (bmx[i][j])
                    System.out.print("O ");
                else
                    System.out.print("_ ");
            }
            System.out.println();
        }
    }

    public static void printMatrix(double[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i,j;
        System.out.println(":: printMatrix =>");
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                System.out.print(mx[i][j]+" ");
            }
            System.out.println();
        }
    }

    public static void printMatrix(float[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i,j;
        System.out.println(":: printMatrix =>");
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                System.out.print(mx[i][j]+" ");
            }
            System.out.println();
        }
    }

    public static void printMatrix(byte[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i,j;
        System.out.println(":: printMatrix =>");
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                System.out.printf("%2d ", mx[i][j]);
            }
            System.out.println();
        }
    }

    public static void printMatrix(int[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i,j;
        System.out.println(":: printMatrix =>");
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                System.out.print(mx[i][j]+" ");
            }
            System.out.println();
        }
    }

	public static float[] applyUint8(float[] input) {
		int i;

		int L = input.length;
		float[] salida = new float[L];

		for (i = 0; i < L; i++) {
			salida[i] = (float)Math.round(input[i]);

			if (salida[i] < 0) salida[i] = 0;
			else if (salida[i] > 255) salida[i] = 255;
		}

		return salida;
	}

    public static float[][] transformMatrixFrom1To2D(float[] pixels, int w, int h)
    {
        float[][] mx = new float[h][w];
        int i,j;
        for(i=0;i<h;i++)
            for(j=0;j<w;j++)
                mx[i][j] = pixels[i*w + j];
        return mx;
    }

    public static float[] transformMatrixFrom2To1D(float[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        float[] pixels = new float[m*n];
        int i,j;
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                pixels[i*m + j] = mx[i][j];
        return pixels;
    }

    public static double[] transformMatrixFrom2To1D(double[][] mx)
    {
        int m = mx[0].length;
        int n = mx.length;
        double[] pixels = new double[m*n];
        int i,j;
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                pixels[i*m + j] = mx[i][j];
        return pixels;
    }

    public static float[][] ensureBinaryMatrixFromBinaryGrayscale(float[][] pixels, int w, int h)
    {
        int i,j;
        for(i=0;i<h;i++)
            for(j=0;j<w;j++)
                if (pixels[i][j] == 255.0f)
                    pixels[i][j] = 1.0f;
                else
                    pixels[i][j] = 0.0f;
        return pixels;
    }

    public static float[][] binarizeMatrix(float[][] mx) {
        return applyRawThreshold(mx, 1.0f);
    }

    public static float[][] applyRawThreshold(float[][] mx, float threshold)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i, j;
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                mx[i][j] = mx[i][j] <= threshold ? 0.0f : 1.0f;
        return mx;
    }

    public static double[][] applyRawThresholdInLower(double[][] mx, double threshold)
    {
        int m = mx[0].length;
        int n = mx.length;
        int i, j = 0;
        for(i=0;i<n;i++)
            for(j=0;j<m;j++)
                if(mx[i][j]<threshold)
                    mx[i][j] = 0.0;
        return mx;
    }
    
    public static int[] byteArr2IntArr(byte[] ba) {
        int[] ia = new int[ba.length];
        for (int i = 0; i < ba.length; i++) {
            ia[i] = ba[i] & 0xff;
        }
        // °J° ImgPlusPrinter.print_img_pixels_I(histogram,256,1);
        return ia;
    }
    
    public static double[] minMat(double[] A, double min){
        double[] minimized = new double[A.length];
        for (int i = 0; i < A.length; i++) {
            minimized[i] = (A[i] < min)? A[i]:  min;
            // °J° if(A[i] >= min) System.out.println(A[i]+" < "+min+" : "+minimized[i]);
        }
        return minimized;
    }
    
    public static double[] maxMat(double[] A, double max){
        double[] maximized = new double[A.length];
        for (int i = 0; i < A.length; i++) {
            maximized[i] = (A[i] > max)? A[i]:  max;
            // °J° if(A[i] <= max) System.out.println(A[i]+" < "+max+" : "+maximized[i]);
        }
        return maximized;
    }
    public static float[] minMat(float[] A, float min){
        float[] minimized = new float[A.length];
        for (int i = 0; i < A.length; i++) {
            minimized[i] = (A[i] < min)? A[i]:  min;
            // °J° if(A[i] >= min) System.out.println(A[i]+" < "+min+" : "+minimized[i]);
        }
        return minimized;
    }
    
    public static float[] maxMat(float[] A, float max){
        float[] maximized = new float[A.length];
        for (int i = 0; i < A.length; i++) {
            maximized[i] = (A[i] > max)? A[i]:  max;
            // °J° if(A[i] <= max) System.out.println(A[i]+" < "+max+" : "+maximized[i]);
        }
        return maximized;
    }
    
    // °J° public static void minMat(int[] A, double min){
        // °J° for (int i = 0; i < A.length; i++) {
            // °J° A[i] = (A[i] < min)? A[i]: min;
        // °J° }
    // °J° }
    
    public static float[][] convertFromDoubleToFloat(double[][] img) {
		int i, j;
		int nRows = img.length;
		int nCols = img[0].length;

        float[][] fMat = new float[nRows][nCols];
        for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				fMat[i][j] = (float)(img[i][j]);
        }
        return fMat;
    }
    
    public static double[][] convertFromFloatToDouble(float[][] img){
		int i, j;
		int nRows = img.length;
		int nCols = img[0].length;

        double[][] iMat = new double[nRows][nCols];
        for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				iMat[i][j] = (double)(img[i][j]);
        }
        return iMat;
    }

    public static float[][] convertFromIntToFloat(int[][] img) {
		int i, j;
		int nRows = img.length;
		int nCols = img[0].length;

        float[][] iMat = new float[nRows][nCols];
        for (i = 0; i < nRows; i++) {
			for (j = 0; j < nCols; j++)
				iMat[i][j] = (float)(img[i][j]);
        }
        return iMat;
    }

    public static float[] convertFromIntToFloat(int[] img){
        float[] fMat = new float[img.length];
        for (int i = 0; i < img.length; i++) {
            fMat[i] = (float)(img[i]);
        }
        return fMat;
    }
    
    public static int[] convertFromFloatToInt(float[] img){
        int[] iMat = new int[img.length];
        for (int i = 0; i < img.length; i++) {
            iMat[i] = Math.round(img[i]);
        }
        return iMat;
    }

    public static float[] im2float(int[] img){
        float[] fMat = new float[img.length];
        for (int i = 0; i < img.length; i++) {
            fMat[i] = (float)(img[i]/ 255f);
        }
        return fMat;
    }
    
    public static double[] im2double(int[] img){
        double[] fMat = new double[img.length];
        for (int i = 0; i < img.length; i++) {
            fMat[i] = (double)(img[i]/ 255.0);
        }
        return fMat;
    }
    
    public static int[] im2int(double[] img){
        int[] iMat = new int[img.length];
        for (int i = 0; i < img.length; i++) {
            iMat[i] = (int)Math.round(img[i]* 255.0);
        }
        return iMat;
    }

    public static int[] im2int(float[] img){
        int[] iMat = new int[img.length];
        for (int i = 0; i < img.length; i++) {
            iMat[i] = Math.round(img[i]* 255f);
        }
        return iMat;
    }

    public static float[][] pixelsToMatrix(LinkedList<Point> mx)
    {
        float[][] px_mx = new float[mx.size()][2];
        Iterator it = mx.iterator();
        Point p = null;
        int i = 0;
        while(it.hasNext()) {
            p = (Point) it.next();
            px_mx[i][0] = p.y; px_mx[i][1] = p.x;
            i++;
        }    
        return px_mx;
    }

    public static float[][] eigenVectors(float [][] mx)
    {
        assert mx != null;
        assert mx.length == mx[0].length;

        float[][] eig_vrs = new float[2][2];

        int n = mx.length;
        int i,j;

        double[][] d_mx = new double[n][n];
        for(i=0;i<n;i++)
            for(j=0;j<n;j++)
                d_mx[i][j] = (double) mx[i][j];
        
        Matrix mat = new Matrix(d_mx);
        EigenvalueDecomposition E = new EigenvalueDecomposition(mat);

        double[] evs = E.getRealEigenvalues();

        int m = evs.length;

        Matrix evrs = E.getV();
        d_mx = evrs.getArray();

        n = d_mx.length;
        m = d_mx[0].length;

        return eig_vrs;
    }

    public static float[] eigenValues(float[][] mx)
    {
        assert mx != null;
        assert mx.length == mx[0].length;

        float[] eig_vs = new float[2];

        // Calculate eigen values from mx.

        int n = mx.length;
        int i,j;

        double[][] d_mx = new double[n][n];
        for(i=0;i<n;i++)
            for(j=0;j<n;j++)
                d_mx[i][j] = (double) mx[i][j];
        
        Matrix mat = new Matrix(d_mx);
        EigenvalueDecomposition E = new EigenvalueDecomposition(mat);

        double[] evs = E.getRealEigenvalues();

        eig_vs[0] = (float) evs[0];
        eig_vs[1] = (float) evs[1];
        
        return eig_vs;
    }

    /**
     * References
     * - {@url http://www.mathworks.com/help/stats/kurtosis.html}
     * - {@url http://stackoverflow.com/questions/15836808/calculating-skewness-and-kurtosis-with-apache-commons}
     * - {@url http://stackoverflow.com/questions/23218300/comparing-matlab-and-apache-statistics-kurtosis/23238861#23238861}
     * - {@url http://commons.apache.org/proper/commons-math/javadocs/api-3.4.1/org/apache/commons/math3/stat/descriptive/DescriptiveStatistics.html}
     */

    public static float getKurtosis(float[] vs, float mean, float std)
    {
        int n = vs.length;
        int i;

        float kurtosis, sub = 0.0f;
        for(i=0;i<n;i++)
            sub += (float) Math.pow(vs[i] - mean, 4);

        sub /=  n;
        kurtosis = sub;

        sub = (float) Math.pow(std, 4);
        kurtosis /= sub; 

        return kurtosis;
    }
    
    public static int[][] colorImage(int[][] img, int colors){
        int width = img[0].length;
        int height = img.length;
        int[][] colr_labels = new int[height][width];
        float fc = 255/colors;
        for ( int r = 0; r < height; r++){
            for (int c = 0; c < width; c++){
                if (img[r][c] != -1){
                    colr_labels[r][c] = (colors < 25  )? (int)(img[r][c]*fc): 255;
                }else{
                    
                }  
            }
        }
        return colr_labels;
    }
    
    public static int[][] colorImageLot(int[][] img, int colors){
        int width = img[0].length;
        int height = img.length;
        int[][] colr_labels = new int[height][width];
        float fc = 255.0f/colors;
        for ( int r = 0; r < height; r++){
            for (int c = 0; c < width; c++){
                if (img[r][c] != -1){
                    colr_labels[r][c] = (int)(img[r][c]*fc);
                }else{
                    
                }  
            }
        }
        return colr_labels;
    }
    
    public static BufferedImage createBImageF(float[][] values) { 
        int width = values[0].length; 
        int height = values.length; 
        int type = BufferedImage.TYPE_BYTE_GRAY; 
 
        BufferedImage im = new BufferedImage(width, height, type); 
        WritableRaster rOut = im.getRaster(); 
 
        double[] rgb = new double[3]; 
        int r, c; 
        for(r = 0; r< height; r++) { 
            for(c = 0; c < width; c++) {
                rgb[0] = values[r][c]; 
                rgb[1] = values[r][c]; 
                rgb[2] = values[r][c]; 
                rOut.setPixel(c, r, rgb); 
            } 
        } 
         
        return im; 
    }

    
    
    
    public static BufferedImage createBImageI(int[][] values) { 
        return createBImageI(values, 1); 
    }
    
    public static BufferedImage createBImageI(int[][] values, int scale) { 
        int width = values[0].length; 
        int height = values.length; 
        int type = BufferedImage.TYPE_BYTE_GRAY; 
 
        BufferedImage im = new BufferedImage(width*scale, height*scale, type); 
        WritableRaster rOut = im.getRaster(); 
 
        double[] rgb = new double[3]; 
        int r, c; 
        for(r = 0; r< height; r++) { 
            for(c = 0; c < width; c++) {
                rgb[0] = values[r][c]; 
                rgb[1] = values[r][c]; 
                rgb[2] = values[r][c]; 
                for (int nn = 0; nn < scale; nn++){
                    for (int mm= 0; mm < scale; mm++){
                        rOut.setPixel(c*scale+nn, r*scale+mm, rgb); 
                    }
                }
            } 
        } 
         
        return im; 
    }
    
    public static BufferedImage createBImageDst(float[][] values) { 
        return createBImageDst(values, 1);
    }
    
    public static BufferedImage createBImageDst(float[][] values, int scale) { 
        int width = values[0].length; 
        int height = values.length; 
        int type = BufferedImage.TYPE_BYTE_GRAY; 
        float min = minValueOfMatrix(values); 
        BufferedImage im = new BufferedImage(width*scale, height*scale, type); 
        WritableRaster rOut = im.getRaster(); 
 
        double[] rgb = new double[3]; 
        double val = 0;
        double xhat;
        int r, c; 
        for(r = 0; r< height; r++) { 
            for(c = 0; c < width; c++) {
                val = values[r][c];
                if (val != Float.NEGATIVE_INFINITY){
                    xhat = -((val - min)*255) / min;
                    rgb[0] = xhat;
                    rgb[1] = xhat;
                    rgb[2] = xhat;
                }else{
                    rgb[0] = 0;
                    rgb[1] = 0;
                    rgb[2] = 0;
                }
                for (int nn = 0; nn < scale; nn++){
                    for (int mm= 0; mm < scale; mm++){
                        rOut.setPixel(c*scale+nn, r*scale+mm, rgb); 
                    }
                }
            } 
        } 
        
        return im; 
    }

    public static BufferedImage combineBImageI(BufferedImage bim, int[][] values, int[][] mask, int scale) { 
        int width = values[0].length; 
        int height = values.length; 
        int type = BufferedImage.TYPE_INT_RGB; 
 
        BufferedImage im = new BufferedImage(width*scale, height*scale, type); 
        // °J° WritableRaster rIn = bim.getRaster(); 
        WritableRaster rOut = im.getRaster(); 
 
        double[] rgb = new double[3]; 
        int r, c; 
        for(r = 0; r< height; r++) { 
            for(c = 0; c < width; c++) {
                for (int nn = 0; nn < scale; nn++){
                    for (int mm= 0; mm < scale; mm++){
                        if(mask[r][c]!=0){
                            int n = values[r][c];
                            // °J° if(n <= 85){
                                // °J° rgb[0] = 0;
                                // °J° rgb[1] = n * 3;
                                // °J° rgb[2] = 255 + n * - 1.5;
                            // °J° }else if (n > 85 && n < 170){
                                // °J° rgb[0] = -128 + n * 1.5;
                                // °J° rgb[1] = 255;
                                // °J° rgb[2] = 255 + n * - 1.5;
                            // °J° }else{
                                // °J° rgb[0] = -128 + n * 1.5;
                                // °J° rgb[1] = 255 + n * -3;
                                // °J° rgb[2] = 0;
                            // °J° }
                            if(n <= 40){
                                rgb[0] = (6.375) * n;
                                rgb[1] = 0;
                                rgb[2] = 0;
                            }else if (n > 40 && n < 210){
                                rgb[0] = 255;
                                rgb[1] = -60 + (1.5) * n;
                                rgb[2] = 0;
                            }else{
                                rgb[0] = 255;
                                rgb[1] = 255;
                                rgb[2] = -1530 + (6.375) * n;
                            }
                            rOut.setPixel(c*scale+nn, r*scale+mm, rgb); 
                        }else{
                            Color col = new Color(bim.getRGB(c,r));
                            rgb[0] = col.getRed();
                            rgb[1] = col.getGreen();
                            rgb[2] = col.getBlue();
                            rOut.setPixel(c*scale+nn, r*scale+mm, rgb); 
                        }
                    }   
                }
            } 
        } 
         
        return im; 
    }
    
    public static BufferedImage readImage(String filename) { 
		BufferedImage inputImage = null;
		try {
			inputImage = ImageIO.read(new File(filename));
		}
		catch(Exception e) {
			System.out.println("!! " +e.getMessage());
		}

		return inputImage;
    }

    public static BufferedImage resize(BufferedImage img, int newW, int newH) { 
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);
    
        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
    
        return dimg;
    }  

    public static int saveImage(String filename,  
            BufferedImage image){ 
        int status = 0;
        File file = new File(filename); 
        try { 
            ImageIO.write(image, "png", file); 
        } catch (Exception e) { 
            System.out.println(e.toString()+" Image '"+filename 
                                +"' saving failed."); 
            status = 1;
        } 
        return status;
    }

    public static void copyFileUsingJava7Files(File source, File dest)
            throws IOException {
        Files.copy(source.toPath(), dest.toPath());
    }

    public static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }
    
    /**
     * Converts a given Image into a BufferedImage
     *
     * @param img The Image to be converted
     * @return The converted BufferedImage
     */
    public static BufferedImage toBufferedImage(Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }
        
        // °J° Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    
        // °J° Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
    
        // °J° Return the buffered image
        return bimage;
    }

    public static float[][] getMatrixFromCSV(String filename)
    {
        return getMatrixFromCSVInCurrentDir("data/imgMatlabFunctions/"+filename);
    }
    public static float[][] getMatrixFromCSVInCurrentDir(String filename)
    {
        float[][] mat = null; 
        try {
            File f = new File(filename);

            CSVReader in = new CSVReader(f);
            in.setColumnDelimiter("\n");
            in.setColumnSeparator(',');

            int w = 0;
            int h = 0;
            while(in.hasNext()) {
                h++;
                Object[] columns = in.next();
                if (w < columns.length)
                    w = columns.length;
            }
            in.close();

            mat = new float[h][w];

            in = new CSVReader(f);
            Object[] columns;
            in.setColumnDelimiter("\n");
            in.setColumnSeparator(',');
            int i=0, j=0;
            String cell;

            while(in.hasNext()) {
                columns = in.next();
                for(j=0;j<w;j++) {
                    cell = (String) columns[j];
                    if (cell.equals("Inf")) {
                        mat[i][j] = Float.POSITIVE_INFINITY;
                    }else if (cell.equals("-Inf")) {
                        mat[i][j] = Float.NEGATIVE_INFINITY;
                    }else{
                        mat[i][j] = Float.parseFloat(cell);
                    }
                }
                i++;
            }
            in.close();
        }
        catch(Exception e) {
            System.out.println("!! " +e.getMessage());
            e.printStackTrace();
        }
        return mat;
    }
    public static void print(String s) { System.out.print(s); }
    public static void println(String s) { System.out.println(s); }
    
    public static void printValuesInBinaryMatrix(float[][] mx) {
        int[] hist = new int[256];
        int n = mx.length;
        int m = mx[0].length;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                hist[(int) (mx[i][j])]++;

        println(":: printing values from matrix...");
        print(":: values: ");
        for (int i = 0; i < 256; i++)
            if(hist[i] != 0)
                print(i+"("+hist[i]+"), ");
    }

    public static float maxValueOfMatrix(float[][] mx) {
        int n = mx.length;
        int m = mx[0].length;
        float max = -10000000.0f;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if(max < mx[i][j])
                    max = mx[i][j];
        return max;
    }

    public static double maxValueOfMatrix(double[][] mx) {
        int n = mx.length;
        int m = mx[0].length;
        double max = -10000000.0;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if(max < mx[i][j])
                    max = mx[i][j];
        return max;
    }
    
    public static int maxValueOfMatrix(int[][] mx) {
        int n = mx.length;
        int m = mx[0].length;
        int max = -10000000;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if(max < mx[i][j])
                    max = mx[i][j];
        return max;
    }

    public static float minValueOfMatrix(float[][] mx) {
        int n = mx.length;
        int m = mx[0].length;
        float min = Float.POSITIVE_INFINITY;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if(min > mx[i][j] && mx[i][j] != Float.NEGATIVE_INFINITY)
                    min = mx[i][j];
        return min;
    }

    public static double minValueOfMatrix(double[][] mx) {
        int n = mx.length;
        int m = mx[0].length;
        double min = Double.POSITIVE_INFINITY;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if(min > mx[i][j])
                    min = mx[i][j];
        return min;
    }
    
    public static int minValueOfMatrix(int[][] mx) {
        int n = mx.length;
        int m = mx[0].length;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                if(min > mx[i][j])
                    min = mx[i][j];
        return min;
    }

    public static float[][] copyMatrix(float[][] mx) {
        int n = mx.length;
        int m = mx[0].length;
        float[][] new_mx = new float[n][m];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                new_mx[i][j] = mx[i][j];
        return new_mx;
    }
    
    public static void printAFewElemsFromStack(Stack<MoorePoint> BT) {
        println(":: In printing values...");
        int size = BT.size();
        int min = Math.min(4, size);
        println(":: Printing first "+min+" elements of stack...");
        Stack<MoorePoint> tmp = new Stack<MoorePoint>();
        MoorePoint mp;
        for (int i = 0; i < min; i++) {
            mp = BT.pop();
            CVUtils.println(mp.toString());
            tmp.push(mp);
        }
        for (int i = 0; i < min; i++) {
            BT.push(tmp.pop());
        }
    }
    public static int[] getHistogram(float[][] mx) {
        // assert ensureGrayscaleMatrix(mx);
        int[] hist = new int[256];
        int n = mx.length;
        int m = mx[0].length;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++) {
                hist[(int) mx[i][j]]++;
            }
        return hist;
    }
    public static int[] getHistogram1D(float[] mx) {
        // assert ensureGrayscaleMatrix(mx);
        int[] hist = new int[256];
        int n = mx.length;
        for (int i = 0; i < n; i++)
            hist[(int) mx[i]]++;
        return hist;
    }
    public static float getEntropy(float[] mx) {
        int[] hist = getHistogram1D(mx);
        float[] p = new float[256];
        float numel = (float) mx.length;

        float entropy = 0.0f;
        for (int i = 0; i < 256; i++) {
            p[i] = ((float) hist[i]) / numel;
            entropy += p[i] != 0 ? (float) (p[i] * (Math.log(p[i]) / Math.log(2))) : 0.0f;
        }
        return -entropy;
    }
    public static float[][] doubleMatrixToFloat(double[][] mx)
    {
        int n = mx.length;
        int m = mx[0].length;
        float[][] new_mx = new float[n][m];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                new_mx[i][j] = (float) mx[i][j];

        return new_mx;
    }
    public static double[][] floatMatrixToDouble(float[][] mx)
    {
        int n = mx.length;
        int m = mx[0].length;
        double[][] new_mx = new double[n][m];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                new_mx[i][j] = (double) mx[i][j];

        return new_mx;
    }
    
    public static void pressKeyContinue(boolean debug){
        try {
            if(debug)System.in.read();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public static void log(String s) { println(":: "+s); }
    public static void debug(String s) { println("|| "+s); }
    public static void error(String s) { println("<> "+s); }
}

